<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('api')->group(function () {

    Route::get('/categories', 'Backend\CategoryController@apiIndex');
    Route::post('/categories/attribute', 'Backend\CategoryController@apiIndexAttribute');
    Route::get('/cities/{provinceId}', 'Auth\RegisterController@getAllCities');
    Route::get('/products/{id}/', 'Frontend\ProductController@apiGetProduct');
    Route::get('/sort-products/{id}/{sort}/{paginate}', 'Frontend\ProductController@apiGetSortedProduct');
    Route::get('/categories-attribute/{id}', 'Frontend\ProductController@apiGetCategoryAttributes');
    Route::get('/filter-products/{id}/{sort}/{paginate}/{attributes}', 'Frontend\ProductController@apiGetFilterProducts');

});

Auth::routes(['verify'=>true]);



Route::get('/verify','VerifyController@getVerify')->name('getVerify');
Route::post('/verify','VerifyController@postVerify')->name('verify');
Route::post('/send-verify','VerifyController@reSendVerify')->name('sendVerify');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::prefix('administrator')->middleware(['auth','isAdmin:مدیر'])->group(function (){
    Route::get('/dashboard','Backend\DashboardController@index');
    Route::get('/news','FrontEnd\AboutController@index')->name('news.index');
    Route::resource('/brands','Backend\BrandController');
    Route::resource('/posts','Backend\PostController');
    Route::resource('/roles','Backend\RoleController');
    Route::resource('/photos','Backend\PhotoController');
    Route::post('/photos/upload','Backend\PhotoController@upload')->name('photos.upload');
    Route::resource('/vendors','Backend\VendorController');
    Route::resource('/shops','Backend\ShopController');
    Route::resource('/categories','Backend\CategoryController');
    Route::resource('/attributes-group','Backend\AttributeGroupController');
    Route::resource('/attributes-value','Backend\AttributeValueController');
    Route::resource('/users','Backend\UserController');
    Route::resource('/profiles','Backend\ProfileController');
    Route::resource('/roles-user','Backend\RoleUserController');
    Route::resource('/coupons','Backend\CouponController');
    Route::resource('/sms-panel','Backend\SmsPanelController');
    Route::resource('/products','Backend\ProductController');
    Route::resource('/banks','Backend\BankController');
    Route::resource('/shops','Backend\ShopController');
    Route::resource('/city','Backend\CityController');
    Route::resource('/province','Backend\ProvinceController');
    Route::resource('/vendors','Backend\VendorController');
    Route::resource('/contacts','Backend\ContactController');
    Route::get('/categories/{id}/settings', 'Backend\CategoryController@indexSetting')->name('categories.indexSetting');
    Route::post('/categories/{id}/settings', 'Backend\CategoryController@saveSetting');
    Route::get('/orders', 'Backend\OrderController@index')->name('order.index');
    Route::get('/orders/lists/{id}', 'Backend\OrderController@getOrderLists')->name('orders.lists');

});

Route::get('/products','FrontEnd\ProductController@index');
Route::get('/products/{slug}','FrontEnd\ProductController@show')->name('product.show');
Route::get('/welcome','FrontEnd\IndexController@index');
Route::get('/','FrontEnd\IndexController@index');
Route::get('/categories/{id}','FrontEnd\CategoryController@show');
Route::post('/search','FrontEnd\SearchController@index');
Route::get('/add-to-cart/{id}', 'FrontEnd\CartController@addToCart')->name('cart.add');
Route::post('/remove-item/{id}', 'FrontEnd\CartController@removeItem')->name('cart.remove');
Route::get('/cart', 'FrontEnd\CartController@getCart')->name('cart.cart');
Auth::routes();

Route::group(['middleware'=>'auth'],function(){
    Route::post('/coupon', 'FrontEnd\CouponController@addCoupon')->name('coupon.add');
    Route::post('/posts', 'FrontEnd\PostController@store')->name('post.store');
    Route::get('/order-verify', 'FrontEnd\OrderController@verify')->name('order.verify');
    Route::get('/payment-verify/{id}', 'FrontEnd\PaymentController@verify')->name('payment.verify');
    Route::get('/profile', 'FrontEnd\UserController@profile')->name('user.profile');
    Route::get('/profile-edit/{id}', 'FrontEnd\UserController@edit')->name('profile.edit');
    Route::put('/profile-update/{id}', 'FrontEnd\UserController@update')->name('profile.updates');
    Route::get('/orders', 'FrontEnd\OrderController@index')->name('profile.orders');
    Route::get('/orders/lists/{id}', 'FrontEnd\OrderController@getOrderLists')->name('profile.orders.lists');
    Route::post('/add-city','FrontEnd\UserController@locations')->name('add.city');
    Route::resource('/addresses','FrontEnd\AddressController');
    Route::post('/photos/upload','Backend\PhotoController@upload')->name('photos.upload');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/mobile/reset', 'Auth\MobileResetController@index')->name('mobile.reset');
Route::post('/send/code', 'Auth\MobileResetController@sendCodePass')->name('send.code');
Route::post('/change/code','Auth\MobileResetController@changePassword')->name('change.code');
Route::get('/mobile/change', 'Auth\MobileResetController@getMobile')->name('mobile.change');

Route::get('/contact-me','FrontEnd\AboutController@contact')->name('contact.me');
Route::post('/contact-save','FrontEnd\AboutController@contactSave')->name('contact.save');
Route::post('/email-save','FrontEnd\AboutController@news')->name('email.save');
Route::get('/about-me','FrontEnd\AboutController@about')->name('about.me');

