let menu_container = document.getElementById("menu-container");
let close_btn = document.getElementById("close-btn");
let columns_container = document.getElementById("cols-container");
let first_column = document.getElementById("first-col");
let second_column = document.getElementById("second-col");
let dim = document.getElementById("dim");

function menuToggle() {
    let menuClassList = menu_container.classList;
    let buttonClassList = close_btn.classList;

    if( menuClassList.contains("d-none") ) {
        menuClassList.remove("d-none");
        menuClassList.add("d-flex");

        buttonClassList.remove("d-none");
        dim.classList.remove("d-none");
    }
    else {
        menuClassList.add("d-none");
        menuClassList.remove("d-flex");

        buttonClassList.add("d-none");
        dim.classList.add("d-none")
    }
}

function getMove() {
    columns_container.scrollBy({
        behavior: "smooth",
        left: "300"
    })
}

function getBackToCategories() {
    columns_container.scrollBy({
        behavior: "smooth",
        left: "-300"
    })
}



// /**
//  * 
//  * @param elementCount The number of elements you want to produce 
//  * @param elmentName The name of element you want to produce
//  * @param parentElementId The id of element you want to append child for it
//  * @param elementOptions The options you want to set for elements
//  */
// function elementManufacture(elementCount, elmentName, parentElementId, elementOptions) {
//     let parentElem = document.getElementById(parentElementId)
//     for(let item = 0; item < elementCount; item++ ) {
//         let childElem = document.createElement(elmentName, elementOptions)
//         parentElem.append(childElem);
//         console.log("product", item)
//     }
// }
// elementManufacture(1, "li", "ellll")