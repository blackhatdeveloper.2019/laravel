const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
mix.sass('resources/sass/backend-style.scss','public/backend/css/backend.css')
    .sass('resources/sass/frontend-style.scss','public/frontend/css/frontend.css')
    .sass('resources/sass/fonts.scss','public/frontend/css/fonts.css')
    .version();

// mix dropzone
// mix.sass('resources/sass/foundation.css','public/css/foundation.css')
// .sass('resources/sass/xzoom.css','public/css/xzoom.css')
// .sass('resources/sass/normalize.css','public/css/normalize.css');
mix.browserSync('http://laravel.local:10/');
// end mix dropzone
