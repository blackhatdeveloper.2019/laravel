<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function photos()
    {
        return $this->belongsToMany(Photo::class,'photo_shop','photo_id','shop_id');
    }
}
