<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table='city';

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }


}
