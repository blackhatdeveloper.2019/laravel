<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
           'name'=>'required|unique:categories|min:4|max:255',
           'meta_desc'=>'required|min:20',
           'meta_title'=>'required|min:6|max:255',
           'meta_keywords'=>'required|min:10|max:255',
        ];
    }

    public function messages()
    {
        return [
          'name.unique'=>'نام دسته قبلا ثبت شده است',
          'name.required'=>'نام دسته را وارد کنید',
          'name.min'=>'نام دسته نباید کمتر از 4 حرف باشد',
          'name.max'=>'نام دسته نباید بیشتر از 255 حرف باشد',
          'meta_desc.required'=>'توضیحات سئو نباید خالی باشد',
          'meta_desc.min'=>'توضیحات سئو نباید کمتر از 20 حرف باشد',
          'meta_title.required'=>'عنوان سئو نباید خالی باشد',
          'meta_title.min'=>'عنوان سئو نباید کمتر از 6 حرف باشد',
          'meta_title.max'=>'عنوان سئو نباید بیشتر از 255 حرف باشد',
          'meta_keywords.required'=>'کلمات کلید سئو نباید خالی باشد',
          'meta_keywords.min'=>'کلمات کلید سئو نباید کمتر از 10 حرف باشد',
          'meta_keywords.max'=>'کلمات کلید سئو نباید بیشتر از 255 حرف باشد',
        ];
    }
}
