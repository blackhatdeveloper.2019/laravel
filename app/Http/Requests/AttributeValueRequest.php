<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttributeValueRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'title'=>'required|min:3|max:255',
            'attributeGroup_id'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'مقادیر ویژگی نباید خالی باشد',
            'title.min'=>'مقادیر ویژگی نباید کمتر از 3 حرف باشد باشد',
            'title.max'=>'مقادیر ویژگی نباید بیشتر از 255 حرف باشد باشد',
            'attributeGroup_id.required'=>'لطفا ویژگی مورد نظر را انتخاب کنید',
        ];
    }
}
