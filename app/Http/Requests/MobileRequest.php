<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MobileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'mobile' => 'required|regex:/(09)[0-9]{9}/|digits:11|numeric',

        ];
    }

    public function messages()
    {
        return [
                'mobile.required' => 'شماره موبایل خود را وارد کنید',
                'mobile.digits' => 'شماره موبایل  باید 11 رقم باشد',
                'mobile.numeric' => 'شماره موبایل  را به صورت صحیح وارد کیند',
                'mobile.regex' => 'شماره موبایل  را به صورت صحیح وارد کیند',

        ];
    }
}
