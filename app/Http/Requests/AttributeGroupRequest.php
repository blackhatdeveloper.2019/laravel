<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttributeGroupRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'=>'required|unique:attributesGroup|min:2|max:255',
        ];
    }

    public function messages()
    {
        return [
        'title.required'=>'عنوان ویژگی نباید خالی باشد',
        'title.min'=>'عنوان ویژگی نباید کمتر از 2 حرف باشد باشد',
        'title.unique'=>'عنوان ویژگی قبلا ثبت شده است',
            'title.max'=>'عنوان ویژگی نباید بیشتر از 255 حرف باشد',
    ];
    }
}
