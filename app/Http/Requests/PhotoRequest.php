<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhotoRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];
    }

    public function messages()
    {
        return [
            'file.required'=>'لطفا عکس مورد نظر را اضافه کنید',
            'file.image'=>'لطفا فقط فرمت های عکس را انتخاب نمایید',
            'file.mimes'=>'فرمت های قابل قبول عکس jpeg,png,jpg,gif,svg می باشند',
            'file.max'=>' حجم عکس نباید بالاتر از 2 مگابایت باشد',
        ];
    }
}
