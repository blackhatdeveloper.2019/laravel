<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'nullable|max:255',
            'strong_point'=>'nullable|max:255',
            'weak_point'=>'nullable|max:255',
            'description'=>'required',
        ];
    }
    public function messages()
    {
        return [
                'title.max'=>'عنوان نظر نباید بیشتر از 255 حرف باشد',
                'strong_point.max'=>'نقاط قوت نباید بیشتر از 255 حرف باشد',
                'weak_point.max'=>'نقاط ضعف نباید بیشتر از 255 حرف باشد',
                'description.required'=>'متن نظر نباید خالی باشد',
        ];
    }
}
