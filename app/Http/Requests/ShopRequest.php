<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShopRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'shop_name'=>'required|max:255',
            'addresses'=>'required|max:255',
            'phone'=>'required|digits:11|numeric',
        ];
    }

    public function messages()
    {
        return [
        'shop_name.required'=>'نام فروشگاه نباید خالی باشد',
        'shop_name.max'=>'نام فروشگاه نباید بیشنر از 255 حرف باشد',
        'addresses.max'=>'آدرس فروشگاه نباید بیشنر از 255 حرف باشد',
        'addresses.required'=>'آدرس فروشگاه نباید خالی باشد',
        'phone.required'=>'تلفن فروشگاه نباید خالی باشد',
        'phone.numeric'=>'تلفن فروشگاه را به صورت صحیح وارد کنید',
        'phone.digits'=>'تلفن فروشگاه باید 11 رقم باشد',
        ];
    }
}
