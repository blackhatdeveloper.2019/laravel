<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRoleRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'=>'required|min:4|unique:roles|max:255'
        ];

    }

    public function messages()
    {
        return [
            'name.required'=>'لطفا نام نقش را وارد کنید',
            'name.min'=>'نام نقش حداقل باید شامل 4 حرف باشد',
            'name.max'=>'نام نقش نباید بیشتر از 255 حرف باشد',
            'name.unique'=>'نام نقش قبلا ثبت شده است',
        ];
    }
}
