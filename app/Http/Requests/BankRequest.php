<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_name'=>'required|unique:banks|min:2|max:255',
            'bank_url'=>'required|max:255',
            'bank_code'=>'required',
            'status'=>'required',
        ];
    }

    public function messages()
    {
        return [
          'bank_name.required'=>'نام درگاه بانک را وارد کنید',
          'bank_name.max'=>'نام درگاه بانک نباید بیشتر از 255 حرف باشد',
          'bank_name.unique'=>'نام درگاه بانک قبلا ثبت شده است',
          'bank_name.min'=>'نام درگاه بانک نباید کمتر از 2 حرف باشد',
          'bank_url.required'=>'آدرس درگاه بانک نباید خالی باشد',
          'bank_code.required'=>'کد درگاه بانک نباید خالی باشد',
          'bank_status.required'=>'وضعیت درگاه بانک نباید خالی باشد',
        ];
    }
}
