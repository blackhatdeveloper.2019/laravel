<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5|max:255',
            'mobile' => 'required|unique:users|regex:/(09)[0-9]{9}/|digits:11|numeric',
            'password' => 'required|min:8|max:255',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'نام را وارد کنید',
            'name.min'=>'نام کاربر نباید کمتر از 5 حرف باشد',
            'name.max'=>'نام کاربر نباید بیشتر از 255 حرف باشد',
            'mobile.required'=>'شماره موبایل خود را وارد کنید',
            'mobile.unique'=>'شماره موبایل شما قبلا ثبت شده است',
            'mobile.digits'=>'شماره موبایل  باید 11 رقم باشد',
            'mobile.numeric'=>'شماره موبایل  را به صورت صحیح وارد کیند',
            'mobile.regex'=>'شماره موبایل  را به صورت صحیح وارد کیند',
            'password.required'=>'کلمه عبور خود را وارد کنید',
            'password.min'=>'کلمه عبور نباید کمتر از 8 حرف باشد',
            'password.max'=>'کلمه عبور نباید بیشتر از 255 حرف باشد',
        ];

    }
}
