<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SmsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url_client'=>'required|max:255',
            'name'=>'required|unique:sms_panel|min:4|max:255',
            'user_name'=>'required|max:255',
            'user_password'=>'required|max:255',
            'panel_number'=>'required',
            'pattern_code'=>'required|max:255',
            'status'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'نام پنل را وارد کنید',
            'name.max'=>'نام پنل نباید بیشتر از 255 حرف باشد',
            'name.unique'=>'نام پنل تکراری می باشد',
            'name.min'=>'نام پنل نباید کمتر از 4 حرف باشد',
            'url_client.required'=>'آدرس سرور را وارد کنید',
            'url_client.max'=>'آدرس سرور نباید بیشتر از 255 حرف باشد',
            'user_name.required'=>'نام کاربری را وارد کنید',
            'user_name.max'=>'نام کاربری نباید بیشتر از 255 حرف باشد',
            'user_password.required'=>'کلمه عبور را وارد کنید',
            'user_password.max'=>'کلمه عبور نباید بیشتر از 255 حرف باشد',
            'panel_number.required'=>'شماره تلفن پنل را وارد کنید',
            'pattern_code.required'=>'کد پترن را وارد کنید',
            'pattern_code.max'=>'کد پترن نباید بیشتر از 255 حرف باشد',
            'status.required'=>'وعضیت را مشخص کنید ',
        ];
    }
}
