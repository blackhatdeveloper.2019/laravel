<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BrandInsertRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'=>'required|min:3|unique:brands|max:255'
        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'عنوان برند نباید خالی باشد',
            'title.min'=>'عنوان برند نباید کمتر از 3 حرف باشد',
            'title.unique'=>'عنوان برند قبلا ثبت شده است',
            'title.max'=>'عنوان برند نباید بیشتر از 255 حرف باشد',
        ];

    }
}
