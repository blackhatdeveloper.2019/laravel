<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VendorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:255',
            'family'=>'required|max:255',
            'national_code'=>'required|unique:vendors|digits:10|numeric',
            'cart_number'=>'required|digits:16|numeric',
            'user_id'=>'required|numeric|unique:vendors'
        ];
    }

    public function messages()
    {
        return [
          'name.required'=>'نام خود را وارد کنید',
          'name.max'=>'نام نباید بیشتر از 255 حرف باشد',
          'family.required'=>'نام خانوادگی خود را وارد کنید',
          'family.max'=>'نام خانوادگی نباید بیشتر از 255 حرف باشد',
          'national_code.required'=>'کد ملی خود را وارد کنید',
          'national_code.digits'=>'کد ملی خود را صحیح وارد کنید',
          'cart_number.required'=>'شماره کارت خود را وارد کنید',
          'cart_number.digits'=>'شماره کارت خود را به صورت صحیح وارد کنید',
          'user_id.required'=>'لطفا کاربر مورد نظر را انتخاب کنید',
          'user_id.numeric'=>'اطلاعات کاربری را صحیح وارد کنید',
          'user_id.unique'=>'کاربر مورد نظر قبلا ثبت نام کرده است',
        ];
    }
}
