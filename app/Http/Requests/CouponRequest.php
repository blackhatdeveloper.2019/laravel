<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CouponRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'title'=>'required|min:4|max:255',
            'code'=>'required|unique:coupons|min:6|max:50',
            'price'=>'required|numeric',
            'status'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'عنوان تخفیف را وارد کنید',
            'code.unique'=>'کد تخفیف تکراری می باشد',
            'title.min'=>'عنوان تخفیف نباید کمتر از 4 حرف باشد',
            'title.max'=>'عنوان تخفیف نباید بیشتر از 255 حرف باشد',
            'code.required'=>'کد تخفیف را وارد کنید',
            'code.min'=>'کد تخیفیف نباید کمتر از 6 حرف باشد',
            'code.max'=>'کد تخیفیف نباید بیشتر از 50 حرف باشد',
            'price.required'=>'قیمت تخفیف را وارد کنید',
            'price.numeric'=>'لطفا قیمت تخفیف را با عدد وارد کنید',
            'status.required'=>'لطفا وضعیت تخفیف را مشخص کنید',
        ];
    }
}
