<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
          'title'=>'required|min:5|max:255',
          'slug'=>'required|min:5|unique:products|max:255',
          'price'=>'required|numeric',
          'qty'=>'required|numeric',
          'discount_price'=>'numeric',
          'meta_desc'=>'required|min:5|max:255',
          'meta_title'=>'required|min:5|max:255',
          'meta_keywords'=>'required|min:5|max:255',
        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'نام محصول را وارد کنید',
            'title.min'=>'نام محصول نباید کمتر از 5 حرف باشد',
            'slug.required'=>'نام مستعار نباید خالی باشد',
            'slug.min'=>'نام مستعار نباید کمتر از 5 حرف باشد',
            'slug.max'=>'نام مستعار نباید بیشتر از 255 حرف باشد',
            'slug.unique'=>'نام مستعار تکراری می باشد',
            'price.required'=>'قیمت محصول را وارد کنید',
            'price.numeric'=>'قیمت محصول را صحیح وارد کنید',
            'qty.required'=>'تعداد محصولات را وارد کنید',
            'qty.numeric'=>'تعداد محصول را صحیح وارد کنید',
            'discount_price.numeric'=>'قیمت تخفیف محصول را صحیح وارد کنید',
            'meta_desc.required'=>'توضیحات متای صفحه را وارد کنید',
            'meta_desc.min'=>'توضیحات متای محصول نباید کمتر از 5 حرف باشد',
            'meta_title.required'=>'عنوان سئو محصول را وارد کنید',
            'meta_title.min'=>'عنوان سئو محصول نباید کمتر از 5 حرف باشد',
            'meta_title.max'=>'عنوان سئو محصول نباید بیشتر از 255 حرف باشد',
            'meta_keywords.required'=>'کلمات کلیدی سئو محصول را وارد کنید',
            'meta_keywords.min'=>'کلمات کلیدی سئو محصول نباید کمتر از 5 حرف باشد',
            'meta_keywords.max'=>'کلمات کلیدی سئو محصول نباید بیشتر از 255 حرف باشد',
        ];
    }
}
