<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:255|string',
            'email'=>'required|email',
            'mobile'=>'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
            'subject'=>'required',
            'order'=>'nullable|max:50',
            'description'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'subject.required'=>'موضوع خود را انتخاب کنید',
            'name.required'=>'نام و نام خانوادگی خود را وارد کنید',
            'name.max'=>'نام و نام خانوادگی نباید بیشتر از 255 حرف باشد',
            'mobile.required' => 'شماره موبایل خود را وارد کنید',
            'mobile.digits' => 'شماره موبایل  باید 11 رقم باشد',
            'mobile.numeric' => 'شماره موبایل  را به صورت صحیح وارد کیند',
            'mobile.regex' => 'شماره موبایل  را به صورت صحیح وارد کیند',
            'email.required'=>'ایمیل خود را وارد کنید',
            'email.email'=>'ایمیل خود را به صورت صحیح وارد کنید',
            'description.required'=>'لطفا پیام خود را وارد کنید',
            'order'=>'نباید شناسه سفارش خودت بیشتر از 50 حرف یا رقم باشد'
        ];
    }
}
