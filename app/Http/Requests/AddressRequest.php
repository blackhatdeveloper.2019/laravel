<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address'=>'required|max:255',
            'company'=>'nullable|max:255',
            'receptor_name'=>'required|max:355',
            'receptor_mobile'=> 'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
            'post_code'=>'required|digits:10',
            'province'=>'required',
            'city'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'company.max'=>'نام شرکت نبادی بیشتر از 255 حرف باشد',
            'receptor_mobile.required' => 'شماره موبایل خود را وارد کنید',
            'receptor_mobile.digits' => 'شماره موبایل  باید 11 رقم باشد',
            'receptor_mobile.numeric' => 'شماره موبایل  را به صورت صحیح وارد کیند',
            'receptor_mobile.regex' => 'شماره موبایل  را به صورت صحیح وارد کیند',
            'city.required'=>'لطفا شهر محل زندگیتان را انتخاب کنید',
            'province.required'=>'لطفا استان خود را انتخاب کنید',
            'address.required'=>'لطفا آدرس خود را انتخاب کنید',
            'address.max'=>'آدرس نباید بیشتر از 355 حرف باشد',
            'receptor_name.required'=>'نام گیرنده را وارد کنید',
            'receptor_name.max'=>'نام گیرنده نباید 255 حرف باشد',
            'post_code.required'=>'کد پستی را وارد کنید',
            'post_code.digits'=>'کد پستی را به صورت صحیح وارد کنید',
        ];
    }
}
