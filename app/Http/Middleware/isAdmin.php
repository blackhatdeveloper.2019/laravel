<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class isAdmin
{

    public function handle($request, Closure $next,$role)
    {
        if (Auth::check()){
            $user= Auth::user();
            if ($user->isAdmin($role) && $user->status==1){
                return $next($request);
            }
            else{
                return redirect()->route('home');
            }
        }
        else{
            return redirect()->route('auth.login');
        }
    }
}
