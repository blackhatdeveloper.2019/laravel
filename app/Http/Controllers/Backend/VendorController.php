<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\VendorRequest;
use App\Photo;
use App\User;
use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class VendorController extends Controller
{

    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $vendors=Vendor::paginate(10);
        return view('admin.vendors.index',['vendors'=>$vendors,'user'=>$user]);
    }

    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $users=User::all();
        return view('admin.vendors.create',['user'=>$user,'users'=>$users]);
    }

    public function store(VendorRequest $request)
    {
        $vendor=new Vendor();
        $vendor->name=$request->input('name');
        $vendor->family=$request->input('family');
        $vendor->national_code=$request->input('national_code');
        $vendor->status=$request->input('status');
        $vendor->cart_number=$request->input('cart_number');
        $vendor->user_id=$request->input('user_id');
        $vendor->save();
        Session::flash('success','فروشنده '.$vendor->family.' با موفقیت اضافه شد.');
        return redirect()->route('vendors.index');
    }


    public function show($id)
    {

    }

    public function edit($id)
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $users=User::all();
        $vendor=Vendor::findOrFail($id);
        return view('admin.vendors.edit',['vendor'=>$vendor,'user'=>$user,'users'=>$users]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'name'=>'required',
                'family'=>'required',
                'national_code'=>'required|digits:10|numeric|unique:vendors,national_code,'.$id,
                'cart_number'=>'required|digits:16|numeric',
                'user_id'=>'required|numeric|unique:vendors,user_id,'.$id,
            ],[
                'name.required'=>'نام خود را وارد کنید',
                'family.required'=>'نام خانوادگی خود را وارد کنید',
                'national_code.required'=>'کد ملی خود را وارد کنید',
                'national_code.digits'=>'کد ملی خود را صحیح وارد کنید',
                'cart_number.required'=>'شماره کارت خود را وارد کنید',
                'cart_number.digits'=>'شماره کارت خود را به صورت صحیح وارد کنید',
                'user_id.required'=>'لطفا کاربر مورد نظر را انتخاب کنید',
                'user_id.numeric'=>'اطلاعات کاربری را صحیح وارد کنید',
                'user_id.unique'=>'کاربر مورد نظر قبلا ثبت نام کرده است',
            ]);
            $vendor=Vendor::findOrFail($id);
            $vendor->name=$request->input('name');
            $vendor->family=$request->input('family');
            $vendor->national_code=$request->input('national_code');
            $vendor->status=$request->input('status');
            $vendor->cart_number=$request->input('cart_number');
            $vendor->user_id=$request->input('user_id');
            $vendor->save();
            Session::flash('success','فروشنده '.$vendor->family.' با موفقیت بروزرسانی شد.');
            return redirect()->route('vendors.index');
    }

    public function destroy($id)
    {
        $vendor=Vendor::findOrFail($id);
        $vendor->delete();

        Session::flash('delete_method','فروشنده '.$vendor->name.' با موفقیت حذف شد');
        return redirect()->route('vendors.index');
    }
}
