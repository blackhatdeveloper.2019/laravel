<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\SmsRequest;
use App\Photo;
use App\SmsPanel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SmsPanelController extends Controller
{

    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $sms_panel=SmsPanel::paginate(10);
        return view('admin.sms-panel.index',['sms_panel'=>$sms_panel,'user'=>$user]);
    }

    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.sms-panel.create',['user'=>$user]);
    }


    public function store(SmsRequest $request)
    {
        $sms=new SmsPanel();
        $sms->url_client=$request->input('url_client');
        $sms->panel_number=$request->input('panel_number');
        $sms->name=$request->input('name');
        $sms->user_name=$request->input('user_name');
        $sms->user_password=$request->input('user_password');
        $sms->pattern_code=$request->input('pattern_code');
        $sms->status=$request->input('status');
        $sms->save();

        Session::flash('success','پنل پیام کوتاه '.$sms->name.' با موفقیت ثبت شد');
        return redirect()->route('sms-panel.index');

    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $sms=SmsPanel::findOrFail($id);
        return view('admin.sms-panel.edit',['user'=>$user,'sms'=>$sms]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,
       [
        'url_client'=>'required',
            'name'=>'required|min:4|unique:sms_panel,name,'.$id,
            'user_name'=>'required',
            'user_password'=>'required',
            'panel_number'=>'required',
            'pattern_code'=>'required',
            'status'=>'required',
           ],[
            'name.required'=>'نام پنل را وارد کنید',
            'name.unique'=>'نام پنل تکراری می باشد',
            'name.min'=>'نام پنل نباید کمتر از 4 حرف باشد',
            'url_client.required'=>'آدرس سرور را وارد کنید',
            'user_name.required'=>'نام کاربری را وارد کنید',
            'user_password.required'=>'کلمه عبور را وارد کنید',
            'panel_number.required'=>'شماره تلفن پنل را وارد کنید',
            'pattern_code.required'=>'کد پترن را وارد کنید',
            'status.required'=>'وعضیت را مشخص کنید ',
        ]);
        $sms=SmsPanel::findOrFail($id);
        $sms->url_client=$request->input('url_client');
        $sms->panel_number=$request->input('panel_number');
        $sms->name=$request->input('name');
        $sms->user_name=$request->input('user_name');
        $sms->user_password=$request->input('user_password');
        $sms->pattern_code=$request->input('pattern_code');
        $sms->status=$request->input('status');
        $sms->save();

        Session::flash('success','پنل پیام کوتاه '.$sms->name.' با موفقیت بروزرسانی شد');
        return redirect()->route('sms-panel.index');

    }

    public function destroy($id)
    {
        $sms=SmsPanel::findOrFail($id);
        $sms->delete();

        Session::flash('delete_method','پنل پیام کوتاه '.$sms->name.' با موفقیت حذف شد');
        return redirect()->route('sms-panel.index');
    }
}
