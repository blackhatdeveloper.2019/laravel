<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Photo;
use App\Province;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $province=Province::paginate(10);

        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.province.index',['province'=>$province,'user'=>$user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.province.create',['user'=>$user]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request ,[
            'name'=>'required|min:2|max:255|string|unique:province'
            ],[
                 'name.required'=>'نام استان را وارد کنید',
                 'name.min'=>'نام استان نباید کمتر از 2 حرف باشد',
                 'name.max'=>'نام استان نباید بیشتر از 255 حرف باشد',
                 'name.unique'=>'نام استان قبلا ثبت شده است',
            ]);
        $province=new Province();
        $province->name=$request->input('name');
        $province->save();
        Session::flash('success','استان '.$province->name.' با موفقیت ثبت شد.');
        return redirect()->route('province.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $province=Province::findOrFail($id);
        return view('admin.province.edit',['province'=>$province,'user'=>$user]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request ,[
            'name'=>'required|min:2|max:255|string|unique:province,name,'.$id
        ],[
            'name.required'=>'نام استان را وارد کنید',
            'name.min'=>'نام استان نباید کمتر از 2 حرف باشد',
            'name.max'=>'نام استان نباید بیشتر از 255 حرف باشد',
            'name.unique'=>'نام استان قبلا ثبت شده است',
        ]);
        $province=Province::findOrFail($id);
        $province->name=$request->input('name');
        $province->save();
        Session::flash('success','استان '.$province->name.' با موفقیت بروزرسانی شد.');
        return redirect()->route('province.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $province=Province::findOrFail($id);
        $province->delete();
        Session::flash('delete_method','استان '.$province->name.' با موفقیت حذف شد.');
        return redirect()->route('province.index');
    }
}
