<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Photo;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{

    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();

        $posts=Post::with('product','users')->paginate(10);

        return view('admin.posts.index',['user'=>$user,'posts'=>$posts]);
    }


    public function create()
    {

    }


    public function store(Request $request)
    {

    }


    public function show($id)
    {

    }


    public function edit($id)
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();

        $post=Post::with('product','users')->findOrFail($id);

        return view('admin.posts.edit',['user'=>$user,'post'=>$post]);

    }

    public function update(PostRequest $request, $id)
    {
        $post=Post::findOrFail($id);
        $post->title=$request->input('title');
        $post->strong_point=$request->input('strong_point');
        $post->weak_point=$request->input('weak_point');
        $post->description=$request->input('description');
        $post->status=$request->input('status');
        $post->save();
        Session::flash('success','نظر شما با موفقیت ثبت شد و بعد از تایید مدیران نمایش داده خواهد شد');
        return redirect()->route('posts.index');
    }


    public function destroy($id)
    {

    }
}
