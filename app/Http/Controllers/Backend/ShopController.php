<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ShopRequest;
use App\Photo;
use App\Shop;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ShopController extends Controller
{

    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $shops=Shop::with('photos','vendor')->paginate(10);
        return view('admin.shop.index',['shops'=>$shops,'user'=>$user]);
    }

    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.shop.create',['user'=>$user]);
    }

    public function store(ShopRequest $request)
    {

        $shop=new Shop();
        $shop->shop_name=$request->input('shop_name');
        $shop->phone=$request->input('phone');
        $shop->address=$request->input('addresses');
        $shop->vendor_id=Auth::id();
        $photos = explode(',', $request->input('photo_id')[0]);
        $shop->save();


        $shop->photos()->attach($photos);

        Session::flash('success','فروشگاه '.$shop->shop_name.' با موفقیت ثبت شد.');
        return redirect()->route('shops.index');
    }

    public function show($id)
    {

    }


    public function edit($id)
    {
        $shop=Shop::with('photos','vendor')->findOrFail($id);

        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return  view('admin.shop.edit',['shop'=>$shop,'user'=>$user]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'shop_name'=>'required|max:255',
            'addresses'=>'required|max:255',
            'phone'=>'required|digits:11|numeric',
        ],[
            'shop_name.required'=>'نام فروشگاه نباید خالی باشد',
            'shop_name.max'=>'نام فروشگاه نباید بیشنر از 255 حرف باشد',
            'addresses.max'=>'آدرس فروشگاه نباید بیشنر از 255 حرف باشد',
            'addresses.required'=>'آدرس فروشگاه نباید خالی باشد',
            'phone.required'=>'تلفن فروشگاه نباید خالی باشد',
            'phone.numeric'=>'تلفن فروشگاه را به صورت صحیح وارد کنید',
            'phone.digits'=>'تلفن فروشگاه باید 11 رقم باشد',
        ]);

        $shop = Shop::findOrFail($id);
        $shop->shop_name=$request->input('shop_name');
        $shop->phone=$request->input('phone');
        $shop->address=$request->input('addresses');
        $shop->vendor_id=Auth::id();
        $shop->save();

        $photos = explode(',', $request->input('photo_id')[0]);
        $shop->photos()->sync($photos);
        Session::flash('success','فروشگاه '.$shop->shop_name.' با موفقیت بروز رسانی شد.');
        return redirect()->route('shops.index');
    }

    public function destroy($id)
    {
        $shop=Shop::findOrFail($id);
        $shop->delete();

        Session::flash('delete_method','فروشگاه '.$shop->shop_name.' با موفقیت حذف شد');
        return redirect()->route('shops.index');
    }
}
