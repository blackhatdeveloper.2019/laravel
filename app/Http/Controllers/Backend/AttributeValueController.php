<?php

namespace App\Http\Controllers\Backend;

use App\AttributeGroup;
use App\AttributeValue;
use App\Http\Controllers\Controller;
use App\Http\Requests\AttributeValueRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AttributeValueController extends Controller
{
    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $attributesValue = AttributeValue::with('attributeGroup')->paginate(10);
        return view('admin.attributes-value.index', ['attributesValue'=> $attributesValue,'user'=>$user]);
    }

    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $attributesGroup = AttributeGroup::all();
        return view('admin.attributes-value.create', ['attributesGroup'=> $attributesGroup,'user'=>$user]);
    }

    public function store(AttributeValueRequest $request)
    {
        $newValue = new AttributeValue();
        $newValue->title = $request->input('title');
        $newValue->attributeGroup_id = $request->input('attributeGroup_id');
        $newValue->save();
        Session::flash('success', 'مقدار ویژگی '.$newValue->title.' با موفقیت اضافه شد.');

        return redirect('administrator/attributes-value');

    }


    public function show($id)
    {

    }


    public function edit($id)
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $attributeValue = AttributeValue::with('attributeGroup')->whereId($id)->first();
        $attributesGroup = AttributeGroup::all();
        return view('admin.attributes-value.edit', ['attributeValue'=>$attributeValue, 'attributesGroup'=>$attributesGroup,'user'=>$user]);
    }

    public function update(Request $request, $id)
    {

        $this->validate($request ,[
            'title'=>'required|min:2',
            'attributeGroup_id'=>'required',
        ], [
            'title.required'=>'مقادیر ویژگی نباید خالی باشد',
            'title.min'=>'مقادیر ویژگی نباید کمتر از 2 حرف باشد باشد',
            'attributeGroup_id.required'=>'لطفا ویژگی مورد نظر را انتخاب کنید',
        ]);

        $updatedValue = AttributeValue::findOrFail($id);
        $updatedValue->title = $request->input('title');
        $updatedValue->attributeGroup_id = $request->input('attributeGroup_id');
        $updatedValue->save();

        Session::flash('success', 'مقدار ویژگی '.$updatedValue->title.' با موفقیت بروزرسانی شد');

        return redirect('administrator/attributes-value');
    }

    public function destroy($id)
    {
        $attributeValue = AttributeValue::findOrFail($id);
        $attributeValue->delete();

        Session::flash('delete_method', 'مقدار '.$attributeValue->title.' حذف شد');

        return redirect('administrator/attributes-value');
    }
}
