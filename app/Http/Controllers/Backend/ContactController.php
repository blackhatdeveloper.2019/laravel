<?php

namespace App\Http\Controllers\Backend;

use App\Contact;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ContactController extends Controller
{
    public function index()
    {
        $contacts=Contact::paginate(10);
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.contacts.index',['user'=>$user,'contacts'=>$contacts]);
    }

    public function show($id)
    {
        $contact=Contact::findOrFail($id);
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.contacts.show',['user'=>$user,'contact'=>$contact]);

    }

    public function edit($id)
    {
        $contact=Contact::findOrFail($id);
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.contacts.edit',['user'=>$user,'contact'=>$contact]);

    }

    public function update(ContactRequest $request,$id)
    {
        $contact=Contact::findOrFail($id);
        $contact->name=$request->input('name');
        $contact->email=$request->input('email');
        $contact->mobile=$request->input('mobile');
        $contact->subject=$request->input('subject');
        $contact->order_code=$request->input('order');
        $contact->description=$request->input('description');
        $contact->save();
        Session::flash('success','اطلاعات شما با موفقیت ارسال شد.');
        return redirect()->route('contacts.index');
    }

    public function destroy($id)
    {
        $contact=Contact::findOrFail($id);
        $contact->delete();
        Session::flash('delete_method','اطلاعات مورد نظر با موفقیت حذف شد.');
        return redirect()->route('contacts.index');

    }
}
