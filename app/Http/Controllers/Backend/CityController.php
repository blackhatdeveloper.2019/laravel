<?php

namespace App\Http\Controllers\Backend;

use App\City;
use App\Http\Controllers\Controller;
use App\Photo;
use App\Province;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CityController extends Controller
{

    public function index()
    {
        $cities=City::with('province')->paginate(10);

        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.city.index',['cities'=>$cities,'user'=>$user]);

    }

    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();


        $items=Province::all();

        return view('admin.city.create',['items'=>$items,'user'=>$user]);

    }


    public function store(Request $request)
    {
        $this->validate($request ,[
            'name'=>'required|min:2|max:255|string',
            'province'=>'required',
        ],[
            'name.required'=>'نام شهرستان را وارد کنید',
            'name.min'=>'نام شهرستان نباید کمتر از 2 حرف باشد',
            'name.max'=>'نام شهرستان نباید بیشتر از 255 حرف باشد',
            'province.required'=>'لطفا استان را انتخاب کنید',
        ]);
        $city=new City();
        $city->name=$request->input('name');
        $city->province_id=$request->input('province');
        $city->save();
        Session::flash('success','شهرستان '.$city->name.' با موفقیت ثبت شد.');
        return redirect()->route('city.index');
    }


    public function show($id)
    {

    }

    public function edit($id)
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $items=Province::all();
        $city=City::findOrFail($id);
        return view('admin.city.edit',['items'=>$items,'city'=>$city,'user'=>$user]);

    }

    public function update(Request $request, $id)
    {
        $this->validate($request ,[
            'name'=>'required|min:2|max:255|string',
            'province'=>'required',
        ],[
            'name.required'=>'نام شهرستان را وارد کنید',
            'name.min'=>'نام شهرستان نباید کمتر از 2 حرف باشد',
            'name.max'=>'نام شهرستان نباید بیشتر از 255 حرف باشد',
            'province.required'=>'لطفا استان را انتخاب کنید',
        ]);
        $city=City::findOrFail($id);
        $city->name=$request->input('name');
        $city->province_id=$request->input('province');
        $city->save();
        Session::flash('success','شهرستان '.$city->name.' با موفقیت بروزرسانی شد.');
        return redirect()->route('city.index');
    }

    public function destroy($id)
    {
        $city=City::findOrFail($id);
        $city->delete();
        Session::flash('delete_method','شهرستان '.$city->name.' با موفقیت حذف شد.');
        return redirect()->route('city.index');
    }
}
