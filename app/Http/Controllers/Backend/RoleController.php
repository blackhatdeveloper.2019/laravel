<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateRoleRequest;
use App\Http\Requests\EditRoleRequest;
use App\Photo;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RoleController extends Controller
{

    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();

        $roles=Role::paginate(10);
        return view('admin.roles.index',compact(['roles','user']));
    }

    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.roles.create',['user'=>$user]);
    }

    public function store(CreateRoleRequest $request)
    {
//        $this->validate($request,[
//           'name'=>'required|min:4|alpha|unique:roles'
//        ],[
//            'name.required'=>'لطفا نام نقش را وارد کنید',
//            'name.min'=>'نام نقش حداقل باید شامل 4 حرف باشد',
//            'name.unique'=>'نام نقش قبلا ثبت شده است',
//            'name.alpha'=>'لطفا حروف را وارد کنید و از وارد کردن اعداد خوداری کنید'
//        ]);


        $role= new Role();
        $role->name=$request->input('name');
        $role->save();
        Session::flash('success','نقش '.$role->name.' با موفقیت ثبت شد ');
        return redirect()->route('roles.index');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $role=Role::findOrFail($id);
        return view('admin.roles.edit',compact(['role','user']));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
           'name'=>'required|min:4|unique:roles,name,'.$id
        ],[
            'name.required'=>'لطفا نام نقش را وارد کنید',
            'name.min'=>'نام نقش حداقل باید شامل 4 حرف باشد',
            'name.unique'=>'نام نقش قبلا ثبت شده است',
            'name.alpha'=>'لطفا حروف را وارد کنید و از وارد کردن اعداد خوداری کنید'
        ]);

        $role=Role::findOrFail($id);
        if($role->name=='کاربر عادی' || $role->name=='مدیر'){
            Session::flash('delete_method','نقش '.$role->name.' پیشفرض انتخاب شده است نمی توان آن را تغییر داد.');
            return redirect()->route('roles.index');
        }
        else{
            $role->name=$request->input('name');
            $role->save();
            Session::flash('success','نقش '.$role->name.' با موفقیت بروز رسانی شد.');
            return redirect()->route('roles.index');
        }

    }

    public function destroy($id)
    {
        $role=Role::findOrFail($id);
        if($role->name=='کاربر عادی' || $role->name=='مدیر'){
            Session::flash('delete_method',' نقش '.$role->name.' نمی تواند حذف شود ');
            return redirect()->route('roles.index');
        }else{
            $role->delete();
            Session::flash('delete_method',' نقش '.$role->name.' با موفقیت حذف شد ');
            return redirect()->route('roles.index');
        }

    }
}
