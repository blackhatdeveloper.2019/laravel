<?php

namespace App\Http\Controllers\Backend;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Http\Requests\BrandInsertRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BrandController extends Controller
{
    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $brands=Brand::with('photo')->paginate(10);
        return view('admin.brands.index',['brands'=>$brands,'user'=>$user]);
    }

    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.brands.create',['user'=>$user]);
    }

    public function store(BrandInsertRequest $request)
    {
        $brand=new Brand();
        $brand->title=$request->input('title');
        $brand->description=$request->input('description');
        $brand->photo_id=$request->input('photo_id');
        $brand->save();

        Session::flash('success',' برند '.$brand->title.' با موفقیت اضافه شد.');
        return redirect()->route('brands.index');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $brand=Brand::findOrFail($id);
        return view('admin.brands.edit',['brand'=>$brand,'user'=>$user]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required|min:3|unique:brands,title,'.$id
        ],[
            'title.required'=>'عنوان برند نباید خالی باشد',
            'title.min'=>'عنوان برند نباید کمتر از 3 حرف باشد',
            'title.unique'=>'عنوان برند قبلا ثبت شده است',
        ]);
        $brand=Brand::findOrFail($id);
        $brand->title=$request->input('title');
        $brand->description=$request->input('description');
        $brand->photo_id=$request->input('photo_id');

        $brand->save();

        Session::flash('success',' برند '.$brand->title.' با موفقیت بروز رسانی شد.');
        return redirect()->route('brands.index');

    }

    public function destroy($id)
    {
        $brand=Brand::findOrFail($id);
        $brand->delete();

        Session::flash('delete_method','برند '.$brand->title.' با موفقیت حذف شد');
        return redirect()->route('brands.index');
    }
}
