<?php

namespace App\Http\Controllers\Backend;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Photo;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{


    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $products =Product::with('photos')->paginate(10);
        return view('admin.products.index', compact(['products','user']));
    }


    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $brands = Brand::all();
        return view('admin.products.create', compact(['brands','user']));
    }

    public function store(ProductRequest $request)
    {
        $newProduct = new Product();
        $newProduct->title = $request->title;
        $newProduct->sku = $this->generateSKU();
        $newProduct->slug = $this->makeSlug($request->slug);
        $newProduct->status = $request->status;
        $newProduct->price = $request->price;
        $newProduct->qty = $request->qty;
        $newProduct->discount_price = $request->discount_price;
        $newProduct->description = $request->description;
        $newProduct->brand_id = $request->brand;
        $newProduct->meta_desc = $request->meta_desc;
        $newProduct->meta_title = $request->meta_title;
        $newProduct->meta_keywords = $request->meta_keywords;
        $newProduct->user_id = 1;

        $newProduct->save();

        $attributes = explode(',', $request->input('attributes')[0]);
        $photos = explode(',', $request->input('photo_id')[0]);

        $newProduct->categories()->sync($request->categories);
        $newProduct->attributeValues()->sync($attributes);

        $newProduct->photos()->attach($photos);

        Session::flash('success', 'محصول  '.$newProduct->title.' با موفقیت اضافه شد.');
        return redirect('/administrator/products');
    }

    public function show($id)
    {

    }

    public function generateSKU()
    {
        $number = mt_rand(1000, 99999);
        if($this->checkSKU($number)){
            return $this->generateSKU();
        }
        return (string)$number;
    }
    public function checkSKU($number)
    {
        return Product::where('sku', $number)->exists();
    }
    function makeSlug($string)
    {
        //$string = strtolower($string);
        //$string = str_replace(['؟', '?'], '', $string);
        return preg_replace('/\s+/u', '-', trim($string));
    }
    public function edit($id)
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $brands = Brand::all();
        $product = Product::with(['attributeValues', 'brand', 'categories', 'photos'])->whereId($id)->first();
        return view('admin.products.edit', compact(['brands', ['product'],'user']));
    }

    public function update(Request $request, $id)
    {
            $this->validate($request, [
                'title' => 'required|min:5',
                'slug'=>'required|min:5|unique:products,slug,'.$id,
                'price'=>'required|numeric',
                'qty'=>'required|numeric',
                'discount_price'=>'numeric',
                'meta_desc'=>'required|min:5',
                'meta_title'=>'required|min:5',
                'meta_keywords'=>'required|min:5',
            ], [
                'title.required'=>'نام محصول را وارد کنید',
                'title.min'=>'نام محصول نباید کمتر از 5 حرف باشد',
                'slug.required'=>'نام مستعار نباید خالی باشد',
                'slug.min'=>'نام مستعار نباید کمتر از 5 حرف باشد',
                'slug.unique'=>'نام مستعار تکراری می باشد',
                'price.required'=>'قیمت محصول را وارد کنید',
                'price.numeric'=>'قیمت محصول را وارد کنید',
                'qty.required'=>'تعداد محصولات را وارد کنید',
                'qty.numeric'=>'تعداد محصول را صحیح وارد کنید',
                'discount_price.numeric'=>'قیمت تخفیف محصول را صحیح وارد کنید',
                'meta_desc.required'=>'توضیحات متای صفحه را وارد کنید',
                'meta_desc.min'=>'توضیحات متای محصول نباید کمتر از 5 حرف باشد',
                'meta_title.required'=>'عنوان سئو محصول را وارد کنید',
                'meta_title.min'=>'عنوان سئو محصول نباید کمتر از 5 حرف باشد',
                'meta_keywords.required'=>'کلمات کلیدی سئو محصول را وارد کنید',
                'meta_keywords.min'=>'کلمات کلیدی سئو محصول نباید کمتر از 5 حرف باشد',
            ]);

        $product = Product::findOrFail($id);
        $product->title = $request->title;
        $product->sku = $this->generateSKU();
        $product->slug = $this->makeSlug($request->slug);
        $product->status = $request->status;
        $product->price = $request->price;
        $product->qty = $request->qty;
        $product->discount_price = $request->discount_price;
        $product->description = $request->description;
        $product->brand_id = $request->brand;
        $product->meta_desc = $request->meta_desc;
        $product->meta_title = $request->meta_title;
        $product->meta_keywords = $request->meta_keywords;
        $product->user_id = 1;

        $product->save();

        $attributes = explode(',', $request->input('attributes')[0]);
        $photos = explode(',', $request->input('photo_id')[0]);

        $product->categories()->sync($request->categories);
        $product->attributeValues()->sync($attributes);
        $product->photos()->sync($photos);

        Session::flash('success', 'محصول '.$product->title.' با موفقیت ویرایش شد.');
        return redirect('/administrator/products');

    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        Session::flash('delete_method', 'محصول '.$product->title.' با موفقیت حذف شد.');
        return redirect('/administrator/products');
    }
}
