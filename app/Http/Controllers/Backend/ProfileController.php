<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{

    public function index()
    {
        $users=User::findOrFail(Auth::user()->id);

        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.profiles.index',['users'=>$users,'user'=>$user]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {

    }


    public function edit($id)
    {
        $users=User::findOrFail(Auth::user()->id);
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.profiles.edit',['users'=>$users,'user'=>$user]);

    }

    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'name' => 'required|min:5',
            'mobile' => 'required|regex:/(09)[0-9]{9}/|digits:11|numeric|unique:users,mobile,'.$id,
            'password' => 'nullable|min:8',
        ],[
            'name.required'=>'نام را وارد کنید',
            'name.min'=>'نام کاربر نباید کمتر از 5 حرف باشد',
            'mobile.required'=>'شماره موبایل خود را وارد کنید',
            'mobile.unique'=>'شماره موبایل شما قبلا ثبت شده است',
            'mobile.digits'=>'شماره موبایل  باید 11 رقم باشد',
            'mobile.numeric'=>'شماره موبایل  را به صورت صحیح وارد کیند',
            'mobile.regex'=>'شماره موبایل  را به صورت صحیح وارد کیند',
            'password.min'=>'کلمه عبور نباید کمتر از 8 حرف باشد',
        ]);

        $user=User::findOrFail($id);
        if (!empty($request->input('password')))
        {
            $user->password=Hash::make($request->input('password'));
        }

        $user->name=$request->input('name');
        $user->mobile=$request->input('mobile');
        $user->photo_id=$request->input('photo_id');

        $user->save();
        Session::flash('success','پروفایل '.$user->name.' با موفقیت بروز رسانی شد');
        return redirect('/administrator/profiles');

    }

    public function destroy($id)
    {

    }
}
