<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Photo;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RoleUserController extends Controller
{

    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();

        $users_role=User::with('roles')->paginate(10);
        return view('admin.roles-user.index',compact(['user','users_role']));
    }

    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $roles=Role::all();
        $users=User::all();
       return view('admin.roles-user.create',['roles'=>$roles,'users'=>$users,'user'=>$user]);
    }


    public function store(Request $request)
    {
        $roles=$request->input('role_id');
        $user=new User();
        $user->roles()->sync($roles);
        Session::flash('success','نقش های کاربر '.$user->name.' با موفقیت افزوده شد');
        return redirect('/administrator/roles-user');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();

        $roles=Role::all();
        $users=User::with('roles')->findOrFail($id);
        return view('admin.roles-user.edit',['user'=>$user,'roles'=>$roles,'users'=>$users]);

    }

    public function update(Request $request, $id)
    {
        $user=User::findOrFail($id);
        $user->status=$request->input('status');
        $user->save();
        $user->roles()->sync($request->input('role_id'));
        Session::flash('success','نقش های کاربر '.$user->name.' با موفقیت افزوده شد');
        return redirect('/administrator/roles-user');
    }


    public function destroy($id)
    {

    }
}
