<?php

namespace App\Http\Controllers\Backend;

use App\Coupon;
use App\Http\Controllers\Controller;
use App\Http\Requests\CouponRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CouponController extends Controller
{

    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();

        $coupons = Coupon::paginate(10);
        return view('admin.coupons.index', compact(['coupons','user']));
    }


    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.coupons.create',['user'=>$user]);
    }

    public function store(CouponRequest $request)
    {
        $coupon = new Coupon();
        $coupon->title = $request->title;
        $coupon->code = $request->code;
        $coupon->price = $request->price;
        $coupon->status = $request->status;

        $coupon->save();

        Session::flash('success', 'کد تخفیف با موفقیت ایجاد شد.');
        return redirect('/administrator/coupons');
    }


    public function show($id)
    {

    }

    public function edit($id)
    {
        $coupon = Coupon::findOrFail($id);
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.coupons.edit', compact(['coupon','user']));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'title'=>'required|min:4',
                'code'=>'required|min:6|unique:coupons,code,'.$id,
                'price'=>'required|numeric',
                'status'=>'required',
            ],[
                'title.required'=>'عنوان تخفیف را وارد کنید',
                'code.unique'=>'کد تخفیف تکراری می باشد',
                'title.min'=>'عنوان تخفیف نباید کمتر از 4 حرف باشد',
                'code.required'=>'کد تخفیف را وارد کنید',
                'code.min'=>'کد تخیفیف نباید کمتر از 6 حرف باشد',
                'price.required'=>'قیمت تخفیف را وارد کنید',
                'price.numeric'=>'لطفا قیمت تخفیف را با عدد وارد کنید',
                'status.required'=>'لطفا وضعیت تخفیف را مشخص کنید',
            ]
            );
        $coupon = Coupon::findOrFail($id);
        $coupon->title = $request->title;
        $coupon->code = $request->code;
        $coupon->price = $request->price;
        $coupon->status = $request->status;

        $coupon->save();

        Session::flash('success', 'کد تخفیف با موفقیت ویرایش شد.');
        return redirect('/administrator/coupons');
    }

    public function destroy($id)
    {
        $coupon = Coupon::findOrFail($id);
        $coupon->delete();

        Session::flash('delete_method', 'کد تخفیف با موفقیت حذف شد.');
        return redirect('/administrator/coupons');
    }
}
