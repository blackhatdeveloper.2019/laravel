<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{

    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();

        $users_pro=User::paginate(10);
        return view('admin.users.index',['users_pro'=>$users_pro,'user'=>$user]);
    }

    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.users.create',['user'=>$user]);
    }

    public function store(UserRequest $request)
    {
        $user=new User();
        $user->name=$request->input('name');
        $user->mobile=$request->input('mobile');
        $user->active=$request->input('status');
        if (!empty($request->input('password')))
        {
            $user->password=Hash::make($request->input('password'));
        }

        $user->save();
        Session::flash('success',' با موفقیت اضافه شد'.$user->name.'کاربر ');
        return redirect()->route('users.index');
    }


    public function show($id)
    {

    }


    public function edit($id)
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();

        $users=User::findOrFail($id);
        return view('admin.users.edit',['user'=>$user,'users'=>$users]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|min:5',
            'mobile' => 'required|regex:/(09)[0-9]{9}/|digits:11|numeric|unique:users,mobile,'.$id,
            'password' => 'nullable|min:8',
        ],[
        'name.required'=>'نام را وارد کنید',
        'name.min'=>'نام کاربر نباید کمتر از 5 حرف باشد',
        'mobile.required'=>'شماره موبایل خود را وارد کنید',
        'mobile.unique'=>'شماره موبایل شما قبلا ثبت شده است',
        'mobile.digits'=>'شماره موبایل  باید 11 رقم باشد',
        'mobile.numeric'=>'شماره موبایل  را به صورت صحیح وارد کیند',
        'mobile.regex'=>'شماره موبایل  را به صورت صحیح وارد کیند',
        'password.required'=>'کلمه عبور خود را وارد کنید',
        'password.min'=>'کلمه عبور نباید کمتر از 8 حرف باشد',
        ]);

        $user=User::findOrFail($id);
        $user->name=$request->input('name');
        $user->mobile=$request->input('mobile');
        $user->active=$request->input('status');
        $user->password= Hash::make($request->input('password'));
        $user->save();
        Session::flash('success',' با موفقیت اضافه شد'.$user->name.'کاربر ');
        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        $user=User::findOrFail($id);
        $user->delete();

        Session::flash('delete_method','کاربر '.$user->name.' با موفقیت حذف شد.');
        return redirect()->route('users.index');
    }

}
