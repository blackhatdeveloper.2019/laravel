<?php

namespace App\Http\Controllers\Backend;

use App\AttributeGroup;
use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();

        $categories = Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->paginate(10);
        return view('admin.categories.index', compact(['categories','user']));
    }


    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $categories = Category::with('childrenRecursive')
        ->where('parent_id', null)
        ->get();

        return view('admin.categories.create', compact(['categories','user']));
    }


    public function store(CategoryRequest $request)
    {

        $category = new Category();
        $category->name  = $request->input('name');
        $category->parent_id  = $request->input('category_parent');
        $category->meta_title = $request->input('meta_title');
        $category->meta_desc = $request->input('meta_desc');
        $category->meta_keywords = $request->input('meta_keywords');
        $category->photo_id = $request->input('photo_id');
        $category->save();

        Session::flash('success', 'دسته بندی '.$category->name.' با موفقیت اضافه شد');
        return redirect()->route('categories.index');
    }

    public function show($id)
    {

    }


    public function edit($id)
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $categories = Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->get();
        $category = Category::findOrFail($id);
        return view('admin.categories.edit', ['categories'=> $categories, 'category'=>$category,'user'=>$user]);

    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required|min:4|unique:categories,name,'.$id ,
            'meta_desc'=>'required|min:20',
            'meta_title'=>'required|min:6',
            'meta_keywords'=>'required|min:10',
        ],[

            'name.unique'=>'نام دسته قبلا ثبت شده است',
            'name.required'=>'نام دسته را وارد کنید',
            'name.min'=>'نام دسته نباید کمتر از 4 حرف باشد',
            'meta_desc.required'=>'توضیحات سئو نباید خالی باشد',
            'meta_desc.min'=>'توضیحات سئو نباید کمتر از 20 حرف باشد',
            'meta_title.required'=>'عنوان سئو نباید خالی باشد',
            'meta_title.min'=>'عنوان سئو نباید کمتر از 6 حرف باشد',
            'meta_keywords.required'=>'کلمات کلید سئو نباید خالی باشد',
            'meta_keywords.min'=>'کلمات کلید سئو نباید کمتر از 10 حرف باشد',
        ]);
        $category = Category::findOrFail($id);
        $category->name  = $request->input('name');
        $category->parent_id  = $request->input('category_parent');
        $category->meta_title = $request->input('meta_title');
        $category->meta_desc = $request->input('meta_desc');
        $category->meta_keywords = $request->input('meta_keywords');
        $category->photo_id = $request->input('photo_id');
        $category->save();

        Session::flash('success', 'دسته بندی '.$category->name.' با موفقیت بروز رسانی شد.');
        return redirect()->route('categories.index');
    }

    public function destroy($id)
    {
        $category = Category::with('childrenRecursive')->where('id', $id)->first();
        if(count($category->childrenRecursive)>0){
            Session::flash('delete_method', 'دسته بندی '.$category->name.' دارای زیر دسته است. بنابراین حذف آن امکان پذیر نمی باشد.');
            return redirect()->route('categories.index');
        }
        $category->delete();
        Session::flash('delete_method', 'دسته بندی '.$category->name.' با موفقیت حذف شد.');
        return redirect()->route('categories.index');
    }
    public function indexSetting($id)
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $category = Category::findOrFail($id);
        $attributeGroups = AttributeGroup::all();
        return view('admin.categories.index-setting', compact(['category', 'attributeGroups','user']));
    }

    public function saveSetting(Request $request, $id)
    {

        $category = Category::findOrFail($id);
        $category->attributeGroups()->sync($request->attributeGroups);
        $category->save();
        Session::flash('success', 'دسته بندی '.$category->name.' با موفقیت به ویژگیها اضافه شد.');
        return redirect()->to('/administrator/categories');
    }

    public function apiIndex()
    {
        $categories = Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->get();

        $response =[
            'categories' => $categories
        ];
        return response()->json($response, 200);
    }
    public function apiIndexAttribute(Request $request)
    {
        $categories = $request->all();
        $attributeGroup = AttributeGroup::with('attributesValue', 'categories')
            ->whereHas('categories', function($q) use ($categories){
                $q->whereIn('categories.id', $categories);
            })->get();
        $response =[
            'attributes' => $attributeGroup
        ];
        return response()->json($response, 200);
    }
}
