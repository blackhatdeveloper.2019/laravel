<?php

namespace App\Http\Controllers\Backend;

use App\Bank;
use App\Http\Controllers\Controller;
use App\Http\Requests\BankRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BankController extends Controller
{

    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $banks=Bank::with('photo')->paginate(10);
        return view('admin.banks.index',['banks'=>$banks,'user'=>$user]);

    }


    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.banks.create',['user'=>$user]);
    }

    public function store(BankRequest $request)
    {
        $bank=new Bank();
        $bank->bank_name=$request->input('bank_name');
        $bank->bank_url=$request->input('bank_url');
        $bank->bank_code=$request->input('bank_code');
        $bank->status=$request->input('status');
        $bank->photo_id=$request->input('photo_id');
        $bank->save();
        Session::flash('success','درگاه بانک '.$bank->bank_name.' با موفقیت اضافه شد');
        return redirect()->route('banks.index');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $bank=Bank::with('photo')->findOrFail($id);
        return view('admin.banks.edit',['bank'=>$bank,'user'=>$user]);

    }

    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'bank_name'=>'required|min:2|unique:banks,bank_name,'.$id,
            'bank_url'=>'required',
            'bank_code'=>'required',
            'status'=>'required',
            ]
            ,[
                'bank_name.required'=>'نام درگاه بانک را وارد کنید',
                'bank_name.unique'=>'نام درگاه بانک قبلا ثبت شده است',
                'bank_name.min'=>'نام درگاه بانک نباید کمتر از 2 حرف باشد',
                'bank_url.required'=>'آدرس درگاه بانک نباید خالی باشد',
                'bank_code.required'=>'کد درگاه بانک نباید خالی باشد',
                'bank_status.required'=>'وضعیت درگاه بانک نباید خالی باشد',
            ]);
        $bank=Bank::findOrFail($id);
        $bank->bank_name=$request->input('bank_name');
        $bank->bank_url=$request->input('bank_url');
        $bank->bank_code=$request->input('bank_code');
        $bank->status=$request->input('status');
        $bank->photo_id=$request->input('photo_id');
        $bank->save();
        Session::flash('success','درگاه بانک '.$bank->bank_name.' با موفقیت بروزرسانی شد');
        return redirect()->route('banks.index');
    }

    public function destroy($id)
    {
        $bank=Bank::findOrFail($id);
        $bank->delete();
        Session::flash('delete_method','درگاه بانک '.$bank->bank_name.' با موفقیت حذف شد');
        return redirect()->route('banks.index');
    }
}
