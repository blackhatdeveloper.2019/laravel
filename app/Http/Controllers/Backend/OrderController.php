<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Order;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();

        $orders = Order::paginate(20);
        return view('admin.orders.index', compact(['orders','user']));
    }
    public function getOrderLists($id){

        $order = Order::with('user.addresses.province', 'user.addresses.city', 'products.photos')->whereId($id)->first();
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.orders.lists', compact(['order','user']));
    }
}
