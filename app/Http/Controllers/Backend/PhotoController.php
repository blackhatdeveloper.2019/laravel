<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\PhotoRequest;
use App\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PhotoController extends Controller
{

    public function index()
    {

    }


    public function create()
    {
    }


    public function upload(PhotoRequest $request)
    {
        $uploadedFile = $request->file('file');
        $filename =time().$uploadedFile->getClientOriginalName();
        $original_name=$uploadedFile->getClientOriginalName();
        Storage::disk('public_uploads')->putFileAs(
            '/photos', $uploadedFile, $filename
        );
        $photo =new Photo();
        $photo->original_name=$original_name;
        $photo->path =$filename;
        $photo->user_id=Auth::user()->id;
        $photo->save();
        return response()->json(['photo_id'=>$photo->id]);
    }


    public function store(Request $request)
    {

    }


    public function show($id)
    {

    }


    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {

    }
}
