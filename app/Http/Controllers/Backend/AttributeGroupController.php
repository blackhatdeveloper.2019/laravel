<?php

namespace App\Http\Controllers\Backend;

use App\AttributeGroup;
use App\Http\Controllers\Controller;
use App\Http\Requests\AttributeGroupRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AttributeGroupController extends Controller
{
    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();

        $attributesGroup = AttributeGroup::paginate(10);
        return view('admin.attributes.index', ['attributesGroup'=> $attributesGroup,'user'=>$user]);
    }

    public function create()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.attributes.create',['user'=>$user]);
    }

    public function store(AttributeGroupRequest $request)
    {
        $attributeGroup = new AttributeGroup();
        $attributeGroup->title = $request->input('title');
        $attributeGroup->type = $request->input('type');
        $attributeGroup->description = $request->input('description');
        $attributeGroup->save();

        Session::flash('success', 'ویژگی '.$attributeGroup->title.' با موفقیت اضافه شد.');

        return redirect('administrator/attributes-group');
    }


    public function show($id)
    {

    }

    public function edit($id)
    {
        $attributeGroup = AttributeGroup::findOrFail($id);
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        return view('admin.attributes.edit', ['attributeGroup'=>$attributeGroup,'user'=>$user]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required|min:2|unique:attributesGroup,title,'.$id,
            ],[
            'title.required'=>'عنوان ویژگی نباید خالی باشد',
            'title.min'=>'عنوان ویژگی نباید کمتر از 2 حرف باشد باشد',
            'title.unique'=>'عنوان ویژگی قبلا ثبت شده است',
        ]);
        $attributeGroup = AttributeGroup::findOrFail($id);
        $attributeGroup->title = $request->input('title');
        $attributeGroup->type = $request->input('type');
        $attributeGroup->description = $request->input('description');
        $attributeGroup->save();

        Session::flash('success', 'ویژگی '.$attributeGroup->title.' با موفقیت بروزرسانی شد');

        return redirect('administrator/attributes-group');
    }


    public function destroy($id)
    {
        $attributeGroup = AttributeGroup::findOrFail($id);
        $attributeGroup->delete();

        Session::flash('delete_method', 'ویژگی '.$attributeGroup->title.' حذف شد');

        return redirect('administrator/attributes-group');
    }
}
