<?php

namespace App\Http\Controllers\FrontEnd;

use App\Category;
use App\City;
use App\Contact;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\News;
use App\Photo;
use App\Province;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AboutController extends Controller
{
    public function contact()
    {
        $province=Province::all();
        $cities=City::all();
        $categories=Category::with('childrenRecursive')->where('parent_id', null)->get();
        $lists=Category::where('parent_id',null)->get();
        if (Auth::check()){
            $user=User::where('id',Auth::user()->id)->first();
            $photo=Photo::where('id',$user->photo_id)->first();
            return view('frontend.contact.call',['photo'=>$photo,'user'=>$user,'province'=>$province,'cities'=>$cities,'lists'=>$lists,'categories'=>$categories]);
        }
        else{
            return view('frontend.contact.call',['province'=>$province,'cities'=>$cities,'lists'=>$lists,'categories'=>$categories]);

        }
    }

    public function about()
    {
        $province=Province::all();
        $cities=City::all();
        $categories=Category::with('childrenRecursive')->where('parent_id', null)->get();
        $lists=Category::where('parent_id',null)->get();
        if (Auth::check()){
            $user=User::where('id',Auth::user()->id)->first();
            $photo=Photo::where('id',$user->photo_id)->first();
            return view('frontend.contact.about',['user'=>$user,'photo'=>$photo,'province'=>$province,'cities'=>$cities,'lists'=>$lists,'categories'=>$categories]);

        }
        else{

            return view('frontend.contact.about',['province'=>$province,'cities'=>$cities,'lists'=>$lists,'categories'=>$categories]);

        }
   }
    public function contactSave(ContactRequest $request)
    {
       $contact=new Contact();
       $contact->name=$request->input('name');
       $contact->email=$request->input('email');
       $contact->mobile=$request->input('mobile');
       $contact->subject=$request->input('subject');
       $contact->order_code=$request->input('order');
       $contact->description=$request->input('description');
       $contact->save();
       Session::flash('success','اطلاعات شما با موفقیت ارسال شد.');
       return back();
    }

    public function news(Request $request)
    {

        $this->validate($request,[
           'email'=>'required|email|unique:news',
            ],[
            'email.required'=>'ایمیل خود را جهت عضویت در خبر نامه وارد کنید',
            'email.email'=>'لطفا ایمیل خود را به صورت صحیح وارد کنید',
            'email.unique'=>'ایمیل مورد نظر قبلا ثبت شده است'
        ]);

        $new=new News();
        $new->email=$request->input('email');
        $new->save();
        Session::flash('success','ایمیل شما جهت عضویت در خبرنامه ثبت شد.');
        return back();

    }

    public function index()
    {
        $photo_profile=User::findOrFail(Auth::user()->id);
        $user=Photo::where('id',$photo_profile->photo_id)->first();
        $news=News::paginate(10);
        return view('admin.news.index',['user'=>$user,'news'=>$news]);
    }

}
