<?php

namespace App\Http\Controllers\FrontEnd;

use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Photo;
use App\Post;
use App\Product;
use App\Province;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index()
    {
        $province=Province::all();
        $cities=City::all();
        $products=Product::with('photos','attributeValues.attributeGroup','brand')->inRandomOrder()->take(12)->where('status',1)->get();

        $categories=Category::with('childrenRecursive')
            ->where('parent_id', null)->get();
        $lists=Category::where('parent_id',null)->get();
        if(Auth::check()){
            $user=User::where('id',Auth::user()->id)->first();
            $photo=Photo::where('id',$user->photo_id)->first();
            return view('frontend.products.index',['photo'=>$photo,'user'=>$user,'categories'=>$categories,'products'=>$products,'lists'=>$lists,'cities'=>$cities,'province'=>$province]);
        }
        else{
            return view('frontend.products.index',['categories'=>$categories,'products'=>$products,'lists'=>$lists,'cities'=>$cities,'province'=>$province]);
        }
    }
    public function show($slug)
    {
        $province=Province::all();
        $cities=City::all();
        $product=Product::with('photos','attributeValues.attributeGroup','brand','categories')->where('status',1)->whereSlug($slug)->first();
        $posts=Post::with('users','product')->where('product_id',$product->id)->where('status',1)->get();
        //dd($posts);
        $categories=Category::with('childrenRecursive')
            ->where('parent_id', null)->get();
        $lists=Category::where('parent_id',null)->get();
        $products=Product::with('photos','attributeValues.attributeGroup','brand')->inRandomOrder()->take(12)->where('status',1)->get();
        if(Auth::check()){
            $user=User::where('id',Auth::user()->id)->first();
            $photo=Photo::where('id',$user->photo_id)->first();
            return view('frontend.products.show',['posts'=>$posts,'products'=>$products,'categories'=>$categories,'photo'=>$photo,'user'=>$user,'product'=>$product,'lists'=>$lists,'cities'=>$cities,'province'=>$province]);
        }
        else{
            return view('frontend.products.show',['posts'=>$posts,'products'=>$products,'categories'=>$categories,'product'=>$product,'lists'=>$lists,'cities'=>$cities,'province'=>$province]);
        }
    }
}
