<?php

namespace App\Http\Controllers\FrontEnd;

use App\Address;
use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Photo;
use App\Province;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function profile()
    {
        //dd(Auth::user()->id);
        $province=Province::all();
        $cities=City::all();
        $categories=Category::with('childrenRecursive')->where('parent_id', null)->get();
        $lists=Category::where('parent_id',null)->get();
        $user=User::where('id',Auth::user()->id)->first();
        //dd($user->photo_id);
        $photo=Photo::where('id',$user->photo_id)->first();
        return view('frontend.profile.index', compact(['user','photo','categories','lists','cities','province']));
    }

    public function edit($id)
    {
        $province=Province::all();
        $cities=City::all();
        $categories=Category::with('childrenRecursive')->where('parent_id', null)->get();
        $lists=Category::where('parent_id',null)->get();
        $user=User::where('id',Auth::user()->id)->first();
        $photo=Photo::where('id',$user->photo_id)->first();
        return view('frontend.profile.edit',compact(['user','photo','categories','lists','cities','province']));
    }

    public function update(Request $request, $id)
    {
        //dd($request->all());
        $this->validate($request,[
            'name' => 'required|min:3|max:255',
            'mobile' => 'required|regex:/(09)[0-9]{9}/|digits:11|numeric|unique:users,mobile,'.$id,
            'password' => 'nullable|min:8|max:255',
            ],[
            'name.required'=>'نام را وارد کنید',
            'name.min'=>'نام کاربر نباید کمتر از 3 حرف باشد',
            'name.max'=>'نام کاربر نباید بیشتر از 255 حرف باشد',
            'mobile.required'=>'شماره موبایل خود را وارد کنید',
            'mobile.unique'=>'شماره موبایل شما قبلا ثبت شده است',
            'mobile.digits'=>'شماره موبایل  باید 11 رقم باشد',
            'mobile.numeric'=>'شماره موبایل  را به صورت صحیح وارد کیند',
            'mobile.regex'=>'شماره موبایل  را به صورت صحیح وارد کیند',
            'password.min'=>'کلمه عبور نباید کمتر از 8 حرف باشد',
            'password.max'=>'کلمه عبور نباید بیشتر از 255 حرف باشد',
        ]);
        $user=User::where('id',$id)->first();
        $user->name=$request->input('name');
        $user->mobile=$request->input('mobile');
        $user->photo_id=$request->input('photo_id');
        if (!empty($request->input('password'))){
            $user->password=Hash::make($request->input('password'));
        }
        $user->save();

        Session::flash('success','بروزرسانی انجام شد');
        return redirect('/profile');
    }

    public function locations(Request $request)
    {
        $this->validate($request,[
            'city'=>'required'
            ],[
                'city.required'=>'نام شهرستان نباید خالی باشد.'
        ]);
        $user=User::findOrFail(Auth::user()->id);
        $user->city_id=$request->input('city_id');
        $user->update();
        Session::flash('success','شهرستان شما با موفقیت ثبت شد');
        return back();
    }

}
