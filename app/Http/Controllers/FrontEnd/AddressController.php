<?php

namespace App\Http\Controllers\FrontEnd;

use App\Address;
use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddressRequest;
use App\Photo;
use App\Province;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AddressController extends Controller
{

    public function index()
    {
        $province=Province::all();
        $cities=City::all();
        $categories=Category::with('childrenRecursive')->
        where('parent_id', null)->get();
        $lists=Category::where('parent_id',null)->get();
        $user=User::where('id',Auth::user()->id)->first();
        $photo=Photo::where('id',$user->photo_id)->first();
        $addresses = Address::with('province','city','user')->
        where('user_id',Auth::user()->id)->get();
         //dd($addresses);
        return view('frontend.addresses.index',
            ['user'=>$user,'categories'=>$categories,
                'lists'=>$lists,'cities'=>$cities,'province'=>$province,
                'addresses'=>$addresses,'photo'=>$photo]);
    }

    public function create()
    {
        $province=Province::all();
        $cities=City::all();
        $categories=Category::with('childrenRecursive')->where('parent_id', null)->get();
        $lists=Category::where('parent_id',null)->get();
        $user = User::where('id',Auth::user()->id)->first();
        $photo=Photo::where('id',$user->photo_id)->first();
        return view('frontend.addresses.create',compact(['user','photo','cities','province','categories','lists']));

    }
    public function show()
    {

    }

    public function edit()
    {

    }

    public function store(AddressRequest $request)
    {
        $address=new Address();
        $address->address=$request->input('address');
        $address->company=$request->input('company');
        $address->receptor_name=$request->input('receptor_name');
        $address->receptor_phone=$request->input('receptor_mobile');
        $address->post_code=$request->input('post_code');
        $address->city_id=$request->input('city');
        $address->province_id=$request->input('province');
        $address->user_id=Auth::user()->id;
        $address->save();
        Session::flash('success','آدرس شما با موفقیت ثبت شد.');
        return redirect()->route('addresses.index');
    }

    public function update()
    {

    }

    public function destroy($id)
    {
        $address=Address::findOrFail($id);
        $address->delete();

        Session::flash('delete_method','آدرس مورد نظر با مفقیت حذف شد');
        return redirect()->route('addresses.index');
    }
}
