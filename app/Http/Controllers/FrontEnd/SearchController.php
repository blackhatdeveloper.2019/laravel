<?php

namespace App\Http\Controllers\FrontEnd;

use App\Brand;
use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Photo;
use App\Product;
use App\Province;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $province=Province::all();
        $cities=City::all();
        $categories=Category::with('childrenRecursive')
            ->where('parent_id', null)->get();
        $lists=Category::where('parent_id',null)->get();

        $products=Product::with('photos','categories','brand')->where('title','like',"%{$request->input('search')}%")->where('status',1)->get();
        if (Auth::check()){
            $user=User::where('id',Auth::user()->id)->first();
            $photo=Photo::where('id',$user->photo_id)->first();
            return view('frontend.search.index',['products'=>$products,'user'=>$user,'photo'=>$photo,'categories'=>$categories,'lists'=>$lists,'cities'=>$cities,'province'=>$province]);
        }
        else{
            return view('frontend.search.index',['products'=>$products,'categories'=>$categories,'lists'=>$lists,'cities'=>$cities,'province'=>$province]);
        }
    }
}
