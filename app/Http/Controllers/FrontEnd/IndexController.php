<?php

namespace App\Http\Controllers\FrontEnd;

use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Photo;
use App\Product;
use App\Province;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index()
    {
        $cities=City::all();
        $province=Province::all();
        $categories=Category::with('childrenRecursive')
        ->where('parent_id', null)->get();
        $lists=Category::where('parent_id',null)->get();
//        return view('welcome',['categories'=>$categories]);
        $products=Product::with('photos','attributeValues.attributeGroup','brand')->inRandomOrder()->take(20)->where('status',1)->get();
        if (Auth::check()){
            $user=User::where('id',Auth::user()->id)->first();
            $photo=Photo::where('id',$user->photo_id)->first();
            return view('welcome',['products'=>$products,'lists'=>$lists,'photo'=>$photo,'user'=>$user,'categories'=>$categories ,'province'=>$province,'cities'=>$cities]);
        }
        else{
            return view('welcome',['products'=>$products,'lists'=>$lists,'categories'=>$categories ,'province'=>$province,'cities'=>$cities]);
        }

    }
}
