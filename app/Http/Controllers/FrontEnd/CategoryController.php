<?php

namespace App\Http\Controllers\FrontEnd;

use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Photo;
use App\Product;
use App\Province;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function show($id)
    {
        $province=Province::all();
        $cities=City::all();
        $categories=Category::with('childrenRecursive')
        ->where('parent_id', null)->get();
        $lists=Category::where('parent_id',null)->get();
         $list=Category::findOrFail($id);
        $products=Product::with('photos','attributeValues.attributeGroup','brand','categories')->inRandomOrder()->take(40)->where('status',1)->get();
         if (Auth::check()){
             $user=User::where('id',Auth::user()->id)->first();
             $photo=Photo::where('id',$user->photo_id)->first();
             return view('frontend.categories.show',['products'=>$products,'categories'=>$categories,'list'=>$list ,'lists'=>$lists,'user'=>$user,'photo'=>$photo,'province'=>$province,'cities'=>$cities]);

         }
         else{
             return view('frontend.categories.show',['products'=>$products,'categories'=>$categories,'list'=>$list ,'lists'=>$lists,'province'=>$province,'cities'=>$cities]);
         }

      //  $lists=Category::where('parent_id',null)->get();
     //   return view('frontend.categories.show',['categories'=>$categories,'lists'=>$lists]);
    }
}
