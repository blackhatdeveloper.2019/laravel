<?php

namespace App\Http\Controllers\FrontEnd;

use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Order;
use App\Payment;
use App\Photo;
use App\Province;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class  OrderController extends Controller
{
    public function verify(){
        $cart = Session::has('cart') ? Session::get('cart') : null;
        if(!$cart){
            Session::flash('delete_method', 'سبد خرید شما خالی است');
            return redirect('/');
        }

        $productsId= [];

        foreach ($cart->items as $product){
            $productsId[$product['item']->id] = ['qty' => $product['qty']];
        }

        $order = new Order();
        $order->amount = $cart->totalPrice;
        $order->user_id = Auth::user()->id;
        $order->status = 0;
        $order->save();

        $order->products()->sync($productsId);

        $payment = new Payment($order->amount, $order->id);
        $result = $payment->doPayment();

        if ($result->Status == 100) {
            return redirect()->to('https://sandbox.zarinpal.com/pg/StartPay/'.$result->Authority);
        } else {
            echo'ERR: '.$result->Status;
        }

    }
    public function index()
    {
        $province=Province::all();
        $cities=City::all();
        $orders = Order::where('user_id',Auth::user()->id)->paginate(10);
        $categories=Category::with('childrenRecursive')->where('parent_id', null)->get();
        $lists=Category::where('parent_id',null)->get();
        $user=User::where('id',Auth::user()->id)->first();
        $photo=Photo::where('id',$user->photo_id)->first();
        return view('frontend.profile.orders', compact(['user','photo','orders','province','cities','lists','categories']));
    }
    public function getOrderLists($id)
    {
        $province=Province::all();
        $cities=City::all();
        $order = Order::with('user', 'products.photos')->whereId($id)->first();
        $categories=Category::with('childrenRecursive')->where('parent_id', null)->get();
        $lists=Category::where('parent_id',null)->get();
        $user=User::where('id',Auth::user()->id)->first();
        $photo=Photo::where('id',$user->photo_id)->first();
        return view('frontend.profile.lists', compact(['user','photo','order','province','cities','lists','categories']));
    }
}
