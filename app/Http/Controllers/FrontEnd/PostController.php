<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{
    public function store(PostRequest $request)
    {
        $post=new Post();
        $post->title=$request->input('title');
        $post->strong_point=$request->input('strong_point');
        $post->weak_point=$request->input('weak_point');
        $post->description=$request->input('description');
        $post->product_id=$request->input('product_id');
        $post->user_id=Auth::user()->id;
        $post->status=0;
        $post->save();
        $post->users()->sync(Auth::user()->id);
        Session::flash('success','نظر شما با موفقیت ثبت شد و بعد از تایید مدیران نمایش داده خواهد شد');
        return back();
    }
}
