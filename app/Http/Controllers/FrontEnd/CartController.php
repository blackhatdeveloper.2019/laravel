<?php

namespace App\Http\Controllers\FrontEnd;

use App\Cart;
use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Photo;
use App\Product;
use App\Province;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
//    public function index()
//    {
//        $categories=Category::with('childrenRecursive')->where('parent_id', null)->get();
//        $lists=Category::where('parent_id',null)->get();
//
//        return view('frontend.carts.index',['categories'=>$categories,'lists'=>$lists]);
//    }

    public function addToCart(Request $request, $id)
    {
        $product = Product::with('photos')->whereId($id)->first();
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);
        $request->session()->put('cart', $cart);
        //dd($request->session()->get('cart'));
        return back();
    }

    public function removeItem(Request $request, $id){
        $product = Product::findOrFail($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->remove($product, $product->id);
        $request->session()->put('cart', $cart);
        return back();
    }

    public function getCart()
    {
        $province=Province::all();
        $cities=City::all();
        $categories=Category::with('childrenRecursive')->where('parent_id', null)->get();
        $lists=Category::where('parent_id',null)->get();

        $cart = Session::has('cart') ? Session::get('cart') : null;
        if (Auth::check()){
            $user=User::where('id',Auth::user()->id)->first();
            $photo=Photo::where('id',$user->photo_id)->first();
            //dd($user->photo_id);
            return view('frontend.carts.index', compact(['photo','user','cart','categories','lists','cities','province']));
        }
        else{
            return view('frontend.carts.index', compact(['cart','categories','lists','cities','province']));
        }
    }
}
