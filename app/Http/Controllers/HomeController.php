<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Photo;
use App\Product;
use App\Province;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $province=Province::all();
        $cities=City::all();
        $categories=Category::with('childrenRecursive')
            ->where('parent_id', null)->get();
        $products=Product::with('photos')->inRandomOrder()->take(12)->where('status',1)->get();
        $lists=Category::where('parent_id',null)->get();
        if(Auth::check()){
            $user=User::findOrFail(Auth::user()->id);
            $photo=Photo::where('id',$user->photo_id)->first();
            return view('home',['lists'=>$lists,'categories'=>$categories,'products'=>$products,'province'=>$province,'cities'=>$cities,'photo'=>$photo,'user'=>$user]);
        }
        else{
            return view('home',['lists'=>$lists,'categories'=>$categories,'products'=>$products,'province'=>$province,'cities'=>$cities]);
        }

    }
}
