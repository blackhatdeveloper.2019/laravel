<?php

namespace App\Http\Controllers;

use App\SendCode;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class VerifyController extends Controller
{
    public function getVerify()
    {
        return view('auth.verify');
    }

    public function postVerify(Request $request)
    {
        $this->validate($request ,[
            'code'=>'required|digits:5|numeric'
        ], [
            'code.required'=>'کد مورد نظر را وارد کنید',
            'code.digits'=>'کد مورد نظر شما باید 5 رقم باشد',
            'code.numeric'=>'لطفا کد تایید را درست درج نمایید',
        ]);

            $user=User::where('mobile',$request->input('mobile'))->first();
            if ($user){
                if ($user->code==$request->input('code'))
                {
                    $user->active=1;
                    $user->code=null;
                    $user->save();
                    Session::flash('success','شماره موبایل شما با موفقیت فعال شد');
                    return redirect()->route('login');
                }
                else{
                    Session::flash('delete_method','کد فعال سازی شما اشتباه می باشد.');
                    return back();
                }
            }
            else{
                Session::flash('delete_method','شماره موبایل شما اشتباه می باشد لطفا دوباره سعی کنید');
                return redirect('register');
            }

    }

    public function reSendVerify(Request $request)
    {

                $mobile=$request->input('mobile');
                $user=User::where('mobile',$mobile)->first();
                $names=$user->name;
                if ($user){
                    $send=new SendCode();
                    $user->code=$send->sendCodes($mobile,$names);
                    $user->save();
                }
                Session::flash('success','کد تایید دیگری برای شما ارسال شد');
                return redirect('/verify?mobile='.$request->mobile);

    }


}
