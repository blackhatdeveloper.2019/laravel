<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\SendCode;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use SoapClient;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected function redirectTo()
    {
        //dd(Auth::user()->id);

        $user=User::with('roles')->findOrFail(Auth::user()->id);
        foreach ($user->roles as $role) {
            if ($role->name=='مدیر' && $user->status==1){
                Session::flash('success',' '.$user->name.' به پنل مدیریت آلاکالا خوش آمدید.');
                return '/administrator/dashboard';
            }
            else  if ($role->name=='مدیر' && $user->status==0){
                Session::flash('delete_method','وضعیت شما غیرفعال می باشد لطفا از طریق پشتیبانی جهت فعال سازی وضعیت کاربری خود اقدام بفرمایید');
                return '/home';
            }
        }
        foreach ($user->roles as $role_user){
         if ($role_user->name=='کاربر عادی') {
                return '/home';
            }
        }

    }

    //protected $redirectTo = '/home';


    public function __construct()
    {
        $this->middleware(['guest','throttle: 60 ,4'])->except('logout');
    }


    public function login(Request $request)
    {
        $this->validateLogin($request);
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutResponse($request);
        }

        if ($this->guard()->validate($this->credentials($request))) {
            $user = $this->guard()->getLastAttempted();
            if ($user->active && $this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            } else {
                $this->incrementLoginAttempts($request);
                $send=new SendCode();
                $user->code = $send->sendCodes($request['mobile'], $request['name']);
                if ($user->save()) {
                    return redirect('/verify?mobile=' . $user->mobile);
                }
            }
        }
        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }
}
