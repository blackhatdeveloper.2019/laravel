<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\MobileRequest;
use App\SendCode;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class MobileResetController extends Controller
{
    public function index()
    {
      return  view('auth.mobile.reset');
    }

    public function sendCodePass(MobileRequest $request)
    {
        $user = User::where('mobile', $request->input('mobile'))->get()->first();

        if ($user) {
            $send = new SendCode();
            $user->code = $send->sendCodes($user->mobile, $user->name);
            $user->save();
            Session::flash('success','لطفا کد تایید همراه با کلمه عبور خود را وارد کنید');

            return redirect()->route('mobile.change',['mobile'=>$user->mobile]);
        }
        else{

            Session::flash('delete_method','شماره موبایل مورد نظر یافت نشد لطفا دقت فرمایید');
            return back();
        }
    }

    public function getMobile(Request $request)
    {
           return view('auth.mobile.change',['mobile'=>$request->input('mobile')]);
    }

    public function changePassword(Request $request)
    {
            //dd($request->all());
        $this->validate($request,[
            'password' => 'required|min:8|confirmed',
            'code'=>'required|digits:5|numeric'
        ],[
            'password.required'=>'کلمه عبور خود را وارد کنید',
            'password.min'=>'کلمه عبور نباید کمتر از 8 حرف باشد',
            'password.confirmed'=>'کلمه عبور مطابقت ندارد',
            'code.required' => 'کد تایید خود را وارد کنید',
            'code.digits' => 'کد تایید باید 5 رقم باشد',
            'code.numeric' => 'کد تایید را به صورت صحیح وارد کیند',

        ]);

        $user=User::where('mobile',$request->input('mobile'))->get()->first();
            if (($user->code==$request->input('code'))&& $user){
                $user->code=null;
                $user->password=Hash::make($request->input('password'));
                $user->active=1;
                $user->update();

                Session::flash('success','کلمه عبور با موفقیت بازنشانی شد.');
                return redirect()->route('login');
            }
            else{
                Session::flash('delete_method','کد تایید شما اشتباه می باشد لطفا دوباره سعی کنید.');
                return redirect()->back();
            }

    }
}
