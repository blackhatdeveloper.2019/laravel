<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

use App\SendCode;

class RegisterController extends Controller
{

    use RegistersUsers;


    protected $redirectTo='/verify';


    public function __construct()
    {
        $this->middleware('guest');
    }



    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|min:5',
            'mobile' => 'required|unique:users|regex:/(09)[0-9]{9}/|digits:11|numeric',
            'password' => 'required|min:8',
        ],[
            'name.required'=>'نام را وارد کنید',
            'name.min'=>'نام کاربر نباید کمتر از 5 حرف باشد',
            'mobile.required'=>'شماره موبایل خود را وارد کنید',
            'mobile.unique'=>'شماره موبایل شما قبلا ثبت شده است',
            'mobile.digits'=>'شماره موبایل  باید 11 رقم باشد',
            'mobile.numeric'=>'شماره موبایل  را به صورت صحیح وارد کیند',
            'mobile.regex'=>'شماره موبایل  را به صورت صحیح وارد کیند',
            'password.required'=>'کلمه عبور خود را وارد کنید',
            'password.min'=>'کلمه عبور نباید کمتر از 8 حرف باشد',
            ]);
    }


    protected function create(array $data)
    {
         $user=User::create([
            'name' => $data['name'],
            'mobile' => $data['mobile'],
            'active'=>0,
            'password' => Hash::make($data['password']),
        ]);
         if ($user){
             $send=new SendCode();
             $user->code=$send->sendCodes($data['mobile'],$data['name']);
             $user->save();
         }
        $role=Role::where('name','کاربر عادی')->first();
        $roles=$role->id;
        $user->roles()->attach($roles);
        Session::flash('success',' کاربر گرامی  کد فعال سازی برای موبایل شما اس ام اس شد لطفا کد تایید را در کادر زیر وارد کنید');
        return $user;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user=$this->create($request->all())));
        return $this->registered($request,$user) ?: redirect('/verify?mobile='.$request->mobile);
    }


}
