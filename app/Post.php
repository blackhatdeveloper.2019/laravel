<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
