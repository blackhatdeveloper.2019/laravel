<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table='province';

    public function cities()
    {
        return $this->belongsToMany(City::class);
    }
    public function address()
    {
        return $this->belongsTo(Address::class);
    }
}
