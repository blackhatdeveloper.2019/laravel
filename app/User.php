<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $fillable = [
        'name', 'mobile', 'password','code','active'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
//    protected $casts = [
//        'phone_verified_at' => 'datetime',
//    ];
//    public function getRolesAttribute($name)
//    {
//            return $this->roles('name',$name)->get();
//    }

    public function photo()
    {
        return $this->hasOne(Photo::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class,'role_user','user_id','role_id');
    }

    public function isAdmin($newRole)
    {
        foreach ($this->roles as $role){
            if ($role->name == $newRole){
                return true;
            }
        }
        return false;
    }

    public function coupons()
    {
        return $this->belongsToMany(Coupon::class);
    }
    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
