<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use SoapClient;

class SendCode extends Model
{
    public function sendCodes($mobile,$name)
    {
        $randCode=rand(10000,99999);
        $client = new SoapClient("http://sms.modirpayamak.com/class/sms/wsdlservice/server.php?wsdl");
        $user = "000000";
        $pass = "000000";
        $fromNum = "+983000505";
        $toNum = array($mobile);
        $pattern_code = "ou6frijrca";
        $input_data = array(
            "name" => $name,
            "verification-code" => $randCode
        );
        $client ->sendPatternSms($fromNum, $toNum, $user, $pass, $pattern_code, $input_data);
        if ($client){
            return $randCode;
        }
        else
        {
            Session::flash('delete_method','مشکل در ارسال کد لطفا بعد از مدتی دوباره تلاش کنید');
            return back();
        }
    }
}
