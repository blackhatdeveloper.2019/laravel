<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $uploads='/uploads/photos/';

    public function getPathAttribute($photo)
    {
        return $this->uploads.$photo;
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function brand()
    {
        return $this->hasOne(Brand::class);
    }
    public function bank()
    {
        return $this->hasOne(Bank::class);
    }
    public function shops()
    {
        return $this->belongsToMany(Shop::class,'photo_shop','shop_id','photo_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
