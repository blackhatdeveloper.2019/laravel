<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    public function shops()
    {
        return $this->hasMany(Shop::class);
    }
}
