@extends('layouts.app')

@section('title')
    ورود به آلا کالا
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-sm-8 col-lg-5">
                <div class="text-center my-2">
                    <a href="{{url('/')}}" class="text-decoration-none"><img src="{{url('images/Png-logo.png')}}" width="200" height="80" class="img-fluid" alt=""></a>
                </div>
                @include('admin.partials.form-errors')
                @include('admin.partials.index-success')
                <div class="card my-2">
                    <div class="text-center pt-2">
                        <span class="h3 font-weight-bold">ورود به آلاکالا</span>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}" dir="ltr">
                            @csrf

                            <label for="mobile" class="font-weight-bold pr-1">موبایل</label>
                            <div class="input-group input-group-lg">
                                <input id="mobile" type="tel" class="form-control form-control-lg" dir="rtl" name="mobile" placeholder="شماره موبایل خود را وارد کنید..."  required autocomplete="tel">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-yellow"><i class="fa fa-mobile-alt text-dark fa-lg font-size-22"></i></span>
                                </div>
                            </div>
                            <span class="mr-auto pl-0"><a href="{{route('mobile.reset')}}" class="linked float-left">کلمه عبور خود را فراموش کرده اید؟</a></span>
                            <label for="password" class="font-weight-bold pr-1">کلمه عبور</label>
                            <div class="input-group mb-4">
                                <input id="password" type="password" dir="rtl" class="form-control form-control-lg" name="password" placeholder="کلمه عبور خود را وارد کنید..." required autocomplete="new-password">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-yellow">
                                        <i class="fa fa-lock fa-lg font-size-22 text-dark"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn bg-yellow border-dark btn-block btn-lg custom-control-inline justify-content-center p-2 m-auto">
                                    <span>ورود </span>
                                    <span class="pl-2"><i class="fa fa-sign-in-alt h2"></i></span>
                                </button>
                            </div>

                        </form>
                    </div>

                    </div>

                    <div class="d-block mt-4">
                        <div class="row">
                            <div class="col custom-control-inline m-auto">
                                <hr class="w-40">
                                <span class="text-center font-weight-bold">ثبت نام</span>
                                <hr class="w-40">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <a href="{{route('register')}}" class="btn a-button border-dark d-block font-weight-bold">ثبت نام در آلاکالا</a>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="row">
            <div class="col-md-9 m-auto text-center">
                <hr class="shadow border-bottom shadow-sm">
                <ul class="list-unstyled custom-control-inline font-weight-bold">
                    <li class="nav-item"><a href="#" class="nav-link">شرایط استفاده</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">حریم خصوصی</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">راهنمایی</a></li>
                </ul>
                <br>
                <span class="font-en">Copyright ©2020 All rights reserved | This Web is made with by Hassan Hassanzadeh</span>
            </div>
        </div>
    </div>
@endsection
