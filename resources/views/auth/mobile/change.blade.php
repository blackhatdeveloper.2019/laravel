@extends('layouts.app')

@section('title')
    بازنشانی رمز عبور
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-sm-8 col-lg-5">
                <div class="text-center my-2">
                    <a href="{{url('/')}}" class="text-decoration-none"><img src="{{url('images/Png-logo.png')}}" width="200" height="80" class="img-fluid" alt=""></a>
                </div>
                @include('admin.partials.index-success')
                @include('admin.partials.form-errors')
                <div class="card mt-4">
                    <div class="text-center h4 pt-3">{{ __('بازنشانی کلمه عبور') }}</div>
                    <div class="card-body">
                        <form class="d-inline" method="POST" action="{{route('change.code')}}" dir="ltr">
                            @csrf

                            <input type="hidden" name="mobile" value="{{$mobile}}">
                            <label for="password" class="font-weight-bold"><span class="text-danger font-size-25 mt-0 pt-0">*</span>کلمه عبور</label>
                            <div class="input-group mb-4">
                                <input type="password" name="password" dir="rtl" class="form-control form-control-lg" id="password" placeholder="کلمه عبور را وارد کنید">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-yellow">
                                        <i class="fa fa-lock fa-lg font-size-22"></i>
                                    </span>
                                </div>
                            </div>
                            <label for="confirm" class="font-weight-bold"><span class="text-danger font-size-25 mt-0 pt-0">*</span>تایید کلمه عبور</label>
                            <div class="input-group mb-4">
                                <input type="password" name="password_confirmation" dir="rtl" class="form-control form-control-lg" id="confirm" placeholder="کلمه عبور را دباره وارد کنید">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-yellow">
                                        <i class="fa fa-unlock fa-lg font-size-22"></i>
                                    </span>
                                </div>
                            </div>
                            <label for="code" class="font-weight-bold"><span class="text-danger font-size-25 mt-0 pt-0">*</span>کد تایید</label>
                            <div class="input-group mb-4">
                                <input type="number" name="code" dir="rtl" class="form-control form-control-lg" id="code" placeholder="لطفا کد تایید را وارد کنید">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-yellow">
                                        <i class="fa fa-unlock fa-lg font-size-22"></i>
                                    </span>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-lg bg-yellow border-dark btn-block">{{ __('تایید شماره موبایل') }}<span class="pl-3"><i class="fa fa-mobile-alt"></i></span></button>
                            <br>
                        </form>
                        <br>
                    </div>

                </div>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-md-9 m-auto text-center">
                <hr class="shadow border-bottom shadow-sm">
                <ul class="list-unstyled custom-control-inline font-weight-bold">
                    <li class="nav-item"><a href="#" class="nav-link">شرایط استفاده</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">حریم خصوصی</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">راهنمایی</a></li>
                </ul>
                <br>
                <span class="font-en">Copyright ©2020 All rights reserved | This Web is made with by Hassan Hassanzadeh</span>
            </div>
        </div>
    </div>


@endsection

