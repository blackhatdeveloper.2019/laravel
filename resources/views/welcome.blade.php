@extends('layouts.app')

@section('title')
    به سایت | آلاکالا خوش آمدید
@endsection
@section('style')
    <style>
    button#next {
    right: 15px;
    padding: 30px 0;
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
    }

    button#prev {
    left: 15px;
    padding: 30px 0;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
    }
        .text-darkturquoise{
            color:darkturquoise;
        }
    </style>
@endsection
@include('layouts.navigation')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-12 m-2">
            @include('admin.partials.index-success')
            @include('admin.partials.form-errors')
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-12">
                <div id="carouselIndicators" class="carousel slide text-center" data-ride="carousel">
                    <ol class="carousel-indicators list-unstyled">
                        @foreach($categories as $key=>$category)
                            @if($key==0)
                        <li data-target="#carouselIndicators" data-slide-to="{{$key}}" class="active"></li>
                            @else
                        <li data-target="#carouselIndicators" data-slide-to="{{$key}}"></li>
                            @endif
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach($categories as $keys=>$category)
                            @if($keys==0)
                        <div class="carousel-item carousel-items active ">
                            <img src="{{$category->photo->path}}" width="97%;" class="rounded " height="330" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <a href="{{url('/categories/'.$category->id)}}" class="nav-link"><h5 class="text-light">{{$category->name}}</h5></a>
                                <p></p>
                            </div>
                        </div>
                            @elseif($keys>0)
                        <div class="carousel-item carousel-items">
                            <img src="{{$category->photo->path}}" width="97%;" class="rounded " height="330" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <a href="{{url('/categories/'.$category->id)}}" class="nav-link"><h5 class="text-light">{{$category->name}}</h5></a>
                                <p></p>
                            </div>
                        </div>
                            @endif
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon " aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </div>
        </div>

        @include('layouts.section-product')

        <div class="row m-2">
            <div class="col-md-12 col-sm-12 col-12 ">

                <div class="row">
                @foreach($products as $key_d=>$product)
                    @if($key_d<4)
                    <div class="col-md-3 col-12">
                        <div class="card card-pk">
                                <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                            <div class="card-body">
                                <h5 class="card-title">{{$product->title}}</h5>
                                @if($product->qty!=0)
                                <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                @elseif($product->qty==0)
                                <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
                </div>

                <div class="row">
                    @foreach($products as $key_d=>$product)
                        @if($key_d>=4 && $key_d < 8)
                            <div class="col-md-3 col-12">
                                <div class="card card-pk">
                                    <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$product->title}}</h5>
                                        @if($product->qty!=0)
                                            <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                            <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                            <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                        @elseif($product->qty==0)
                                            <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="row">
                    @foreach($products as $key_d=>$product)
                        @if($key_d>=8 && $key_d < 12)
                            <div class="col-md-3 col-12">
                                <div class="card card-pk">
                                    <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$product->title}}</h5>
                                        @if($product->qty!=0)
                                            <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                            <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                            <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                        @elseif($product->qty==0)
                                            <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="row">
                    @foreach($products as $key_d=>$product)
                        @if($key_d>=12 && $key_d < 16)
                            <div class="col-md-3 col-12">
                                <div class="card card-pk">
                                    <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$product->title}}</h5>
                                        @if($product->qty!=0)
                                            <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                            <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                            <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                        @elseif($product->qty==0)
                                            <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="row">
                    @foreach($products as $key_d=>$product)
                        @if($key_d>=16 && $key_d < 20)
                            <div class="col-md-3 col-12">
                                <div class="card card-pk">
                                    <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$product->title}}</h5>
                                        @if($product->qty!=0)
                                            <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                            <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                            <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                        @elseif($product->qty==0)
                                            <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

            </div>
        </div>

    </div>

    @include('layouts.footer')
@endsection
@section('script')
    <script>
        $('.carousel').carousel({
            interval: 7000
        })
    </script>
@endsection
