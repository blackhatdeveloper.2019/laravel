@extends('layouts.app')

@section('title')
    خطای 500
@endsection

@section('content')
    <br><br><br><br>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger">
                    <div class="h2 text-center">
                        <span>مشکل در خدمات رسانی سرور لطفا بعد تلاش فرمایید</span>
                    </div>
                </div>
                <div class="font-size-280 text-center mt-0 pt-0">
                    <span>5 <span class="text-danger">00</span></span>
                </div>
            </div>
        </div>
    </div>


@endsection
