@extends('layouts.app')

@section('title')
    خطای 429
@endsection

@section('content')
    <br><br><br><br>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-warning">
                        <div class="h2 text-center">
                            <span>دفعات زیادی برای ورود به سایت تلاش کرده اید لطفا بعد مدتی دوباره سعی کنید</span>
                        </div>
                    </div>
                    <div class="d-block">
                        <a href="{{url('/')}}" class="btn btn-warning btn-lg d-block text-white"> بازگشت به صفحه اصلی</a>
                    </div>
                    <div class="font-size-280 text-center mt-0 pt-0">
                        <span>9</span>
                        <span class="text-warning">2</span>
                        <span>4</span>
                    </div>
                </div>
            </div>
        </div>


@endsection
