<header class="d-flex flex-column position-relative nav-bg bg-secondary" >
    <div id="dim" class="d-none"></div>
    <nav class="navbar navbar-toggler navbar-dark" dir="rtl">
        <div class="row vw-100">
            <div class="col-md-3 col-6 text-right">
                <!--  -->

                <button class="navbar-toggler" type="button" onclick="menuToggle()">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <!-- Menu container -->
                <div class="menu-container d-none flex-column align-items-stretch position-absolute p-0 m-0"  id="menu-container">

                    <div class="d-flex flex-row justify-content-end align-items-center py-2 px-4" id="sticky-row">
                        @if(!empty($user->photo_id) && (Auth::check()))
                        <img src="{{$photo->path}}" class="rounded-circle" alt="" />
                        @else
                         <img src="{{url('images/profile_icon.png')}}" alt="" />
                        @endif
                        <span class="m-auto">
                            @if(Auth::check())
                                {{Auth::user()->name}}
                                @else
                                <span><a href="{{url('/login')}}" class="text-muted">ورود به سایت</a></span>
                            @endif
                        </span>

                    </div>

                    <div class="d-flex flex-row-reverse" id="cols-container">
                        <!-- First column -->
                        <div class="d-flex flex-column" id="first-col">
                            <p class="category-title">دسته بندی ها</p>
                            <ul class="d-flex flex-column justify-content-start p-0" id="menu-list">
                               @foreach($lists as $list)
                                <li class="d-flex flex-row justify-content-between align-items-center category-item" onclick="getMove()">
                                    <span><a href="{{url('/categories',$list->id)}}" class="linked text-dark font-size-16">{{$list->name}}</a></span>
                                  <!--  <span><i class="fa fa-angle-left"></i></span> -->
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>

                <!-- Close button --- Appears when the menu is open -->
                <button class="position-absolute d-none" id="close-btn" onclick="menuToggle()">
                    <img src="{{url('images/close.png')}}" class="close-icon" alt="close_icon">
                </button>
                <a href="{{url('/')}}" class="navbar-brand text-decoration-none"><img src="{{url('images/Png-logo-whit.png')}}" height="34"  class="image-grayscale" alt=""></a>
            </div>
            <div id="cart-user" class="col-md-3 col-6 text-left">
                <a class="link text-decoration-none ml-4" href="{{url('/cart')}}">
                    <span class="p-0 text-warning font-size-25 cart-shop">
                       @if(Session::has('cart'))
                            {{Session::get('cart')->totalQty}}
                        @else
                            0
                        @endif
                    </span>
                    <img src="{{url('images/cart.png')}}" alt="سبد خرید">
                </a>
                <a class="dropdown ml-4" href="#" class="nav-link" data-toggle="dropdown">
                        @if(!empty($user->photo_id) && (Auth::check()) )
                            <img src="{{$photo->path}}" class="rounded-circle" width="35" alt="">
                        @else
                            <img src="{{url('images/login.png')}}" alt="">
                        @endif
                            <div class="dropdown-menu text-right" role="menu">
                                <h6 class=" text-center text-muted">ناحیه کاربری</h6>
                                <hr>
                                @if(Auth::check())
                                    <a href="{{url('/profile')}}" class="dropdown-item text-secondary"><span><i class="fas fa-user-alt pl-2"></i></span>پروفایل</a>
                                    <a href="{{route('logout')}}" class="dropdown-item text-secondary"><span><i class="fas fa-sign-out-alt pl-2"></i></span> خروج</a>
                                @else
                                    <a href="{{url('/register')}}" class="dropdown-item text-secondary"><span><i class="fas fa-user-plus pl-2"></i></span>ثبت نام</a>
                                    <a href="{{url('/login')}}" class="dropdown-item text-secondary"><span><i class="fas fa-sign-in-alt pl-2"></i></span> ورود</a>
                                @endif

                            </div>
                </a>
            </div>
            <div class="col-md-6 col-12 text-center">
                <form action="{{url('/search')}}" method="POST" dir="ltr" class="my-auto">
                    @csrf
                    <div class="input-group bg-white rounded">
                        <input type="text" class="form-control" name="search" id="search" dir="rtl" placeholder="جستجو کنید">
                        <span class="input-group-btn bg-yellow rounded-right">
                                <button class="btn btn-default" type="submit">
                                    <img src="{{url('images/search.png')}}" class="img-fluid" alt="">
                                </button>
                            </span>
                    </div>
                </form>
            </div>
            <div id="user-cart" class="col-md-3 col-6 text-left">
                <a class="link text-decoration-none ml-4" href="{{url('/cart')}}">
                    <span class="p-0 text-warning font-size-25 cart-shop">
                        @if(Session::has('cart'))
                            {{Session::get('cart')->totalQty}}
                        @else
                        0
                        @endif
                    </span>
                    <img src="{{url('images/cart.png')}}" alt="سبد خرید">
                </a>
                <a class="dropdown ml-4" href="#" class="nav-link" data-toggle="dropdown">
                    @if(isset($user->photo_id))
                        <img src="{{$photo->path}}" class="img-fluid rounded-circle" width="35" height="35" alt="">
                    @else
                        <img src="{{url('images/login.png')}}" alt="ورود کاربر">
                    @endif
                    <div class="dropdown-menu text-right">
                        <h6 class=" text-center text-muted">ناحیه کاربری</h6>
                        <hr>
                        @if(Auth::check())
                            <a href="{{url('/profile')}}" class="dropdown-item text-secondary"><span><i class="fas fa-user-alt pl-2"></i></span>پروفایل</a>
                            <a href="{{route('logout')}}" class="dropdown-item text-secondary"><span><i class="fas fa-sign-out-alt pl-2"></i></span> خروج</a>
                            @else
                            <a href="{{url('/register')}}" class="dropdown-item text-secondary"><span><i class="fas fa-user-plus pl-2"></i></span>ثبت نام</a>
                            <a href="{{url('/login')}}" class="dropdown-item text-secondary"><span><i class="fas fa-sign-in-alt pl-2"></i></span> ورود</a>
                        @endif

                    </div>
                </a>
            </div>
        </div>
        <div class="row vw-100" id="img-menu">
            <div class="col-md-8 col-3 col-xl-8 col-sm-4 text-right">
                <ul class="custom-control-inline text-menu text-right list-unstyled pr-0 mr-0">
                    @foreach($lists as $key=>$listed)
                        @if($key<10)
                        <li class="nav-item font-size-16"><a href="{{url('/categories/'.$listed->id)}}" class="nav-link">{{$listed->name}}</a></li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <div class="col-md-4 col-xl-4 col-sm-8 col-9 text-left">
                <a class="text-decoration-none" data-toggle="modal" href="#locations">

                    @if(isset($user->city_id)&& Auth::check())
                        <span class="text-white  font-size-14 ml-1">{{$user->city->name}}</span>
                        @else
                    <span class="text-white  font-size-14 ml-1">لطفا شهر و استان  خود را انتخاب کنید</span>
                    @endif
                    <img src="{{url('images/location.png')}}" class="img-fluid pl-4" alt="">
                </a>
            </div>
        </div>
    </nav>

</header>

<div class="modal fade" id="locations" tabindex="-1" dir="ltr" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">انتخاب شهرستان</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('admin.partials.form-errors')
                <form action="{{url('/add-city')}}" method="POST" dir="rtl">
                    @csrf

                    <label for="province">انتخاب استان</label>
                    <div class="form-group">
                        <select class="form-control" name="province" id="province">
                            <option value="">استان محل سکونت خود را انتخاب کنید</option>
                            @foreach($province as $state)
                            <option value="{{$state->id}}">{{$state->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <label for="city">نام شهر </label>
                    <div class="form-group">
                        <select class="form-control" name="city" id="city">
                            <option value="">استان محل سکونت خود را انتخاب کنید</option>
                            @foreach($cities as $city)
                                <option value="{{$city->id}}">{{$city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn bg-yellow border-dark">ثبت اطلاعات</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">انصراف</button>
                </form>
            </div>
        </div>
    </div>
</div>
