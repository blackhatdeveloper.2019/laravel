<!doctype html>
<html lang="fa">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta')
    <link rel="icon" href="{{url('/images/logoico.ico')}}">
    <title>@yield('title')</title>
    <style>
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(images/loader-64x/Preloader_2.gif) center no-repeat #fff;
        }
    </style>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/loader.js')}}"></script>
    <script>
        //paste this code under head tag or in a seperate js file.
        // Wait for window load
        $(window).load(function() {
            // Animate loader off screen
            $(".se-pre-con").fadeOut("slow");
        });
    </script>
    <!-- Styles -->
    <link href="{{ url('css/app.css') }}" rel="stylesheet">
    <link href="{{ url('css/fonts.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ url('frontend/css/fonts.css') }}" rel="stylesheet">
    <link href="{{ url('frontend/css/frontend.css') }}" rel="stylesheet">
    @yield('style')
    @yield('script-top')

</head>

<body id="content">
    <!-- Paste this code after body tag -->
    <div class="se-pre-con"></div>
    <!-- Ends -->
    @yield('content')

    <!-- src popper js -->
    <script src="{{url('backend/js/popper.min.js')}}"></script>

    <!-- Custom JS file -->
    <script src="{{url('frontend/js/carousel_scripts.js')}}"></script>
    <script src="{{url('frontend/js/scripts_menu.js')}}"></script>
    @yield('script')
    <script type="text/javascript">
    window.vasleSettings = {
        app_id: "umJRXMMq",
        fullname: "Your Customer Name", // نام کاربر داخل پایگاه داده شما که می بایست رشته باشد | اجباری
        user_id: null, // شناسه کاربر داخل پایگاه داده شما که می بایست عددی باشد | اجباری
        email: null, // ایمیل کاربر شما که میبایست به صورت رشته باشد | اجباری
        created_at: null, // تاریخ ثبت نام کاربر که به صورت عددی و unix timestamp میباشد, | اجباری
        mobile: null, // شماره مبایل کاربر که به صورت رشته میباشد | اختیاری
        custom_info: { // این ها یکسری نمونه داده هستند که برای هر کسب و کار می تواند متفاوت باشد
            gender: "male", //optional
            age: 28, //optional
            last_order: 1518098619, //optional
            is_vip: false, //optional
            // و هر داده ای که برای شما مهم هست
        }
    }
    </script><script type="text/javascript">
    !function(){function t(){var t=n.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://www.retain.ir/app/umJRXMMq/widget/?"+Math.random().toString(34).slice(2);var e=n.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=window,a=e.Vasle;if("function"==typeof a)a("reattach_activator"),a("update",e.vasleSettings);else{var n=document,c=function(){c.c(arguments)};c.q=[],c.c=function(t){c.q.push(t)},e.Vasle=c,e.attachEvent?e.attachEvent("onload",t):e.addEventListener("load",t,!1)}}();
    </script>
</body>
</html>
