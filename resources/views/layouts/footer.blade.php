<div class="container-fluid bg-dark">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <a href="#content" class="font-size-18 text-light text-decoration-none">
                <div class="text-center p-2">
                    <i class="fa fa-chevron-circle-up font-size-18"></i>
                    <span>برگشت به بالا</span>
                </div>
            </a>
        </div>
    </div>
</div>
<footer class="footer bg-secondary">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-12">
                <div class="text-center my-3">
                    <a href="#" class="text-decoration-none text-light">
                        <img src="{{url('images/expres.png')}}" class="" width="35%;" height="70&;" alt="">
                        <h6>تحویل اکسپرس</h6>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-12">
                <div class="text-center my-3">
                    <a href="#" class="text-decoration-none text-light">
                        <img src="{{url('images/Support.png')}}" class="" width="35%;" height="70&;" alt="">
                        <h6>پشتیبانی 24 ساعته</h6>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-12">
                <div class="text-center my-3">
                    <a href="#" class="text-decoration-none text-light">
                        <img src="{{url('images/FastDel.png')}}" class="" width="35%;" height="70&;" alt="">
                        <h6>تحویل سریع</h6>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-12">
                <div class="text-center my-3">
                    <a href="#" class="text-decoration-none text-light">
                        <img src="{{url('images/garantee.png')}}" class="" width="35%;" height="70&;" alt="">
                        <h6>ضمانت اصل بودن</h6>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <hr class="w-100 bg-light">
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-12">
                        <ul class="list-unstyled text-light">
                            <li class="nav-item"><span class="font-size-18">راهنمای خرید</span></li>
                            <li class="nav-item"><a class="nav-link text-light" href="#">شیوه های پرداخت</a></li>
                            <li class="nav-item"><a class="nav-link text-light" href="#">رویه ارسال سفارش</a></li>
                            <li class="nav-item"><a class="nav-link text-light" href="#">نحوه ثبت سفارش</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12">
                        <ul class="list-unstyled text-light">
                            <li class="nav-item"><span class="font-size-18">خدمات مشتریان</span></li>
                            <li class="nav-item"><a class="nav-link text-light" href="#">پاسخ به پرسش های متداول</a></li>
                            <li class="nav-item"><a class="nav-link text-light" href="#">شرایط استفاده</a></li>
                            <li class="nav-item"><a class="nav-link text-light" href="#">حریم خصوصی</a></li>
                            <li class="nav-item"><a class="nav-link text-light" href="#">گزارش باگ</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12">
                        <ul class="list-unstyled text-light">
                            <li class="nav-item"><span class="font-size-18">آلاکالا</span></li>
                            <li class="nav-item"><a class="nav-link text-light" href="#">فروش در آلاکالا</a></li>
                            <li class="nav-item"><a class="nav-link text-light" href="#">فرصت های شغلی</a></li>
                            <li class="nav-item"><a class="nav-link text-light" href="{{url('/contact-me')}}">تماس با آلاکالا</a></li>
                            <li class="nav-item"><a class="nav-link text-light" href="{{url('/about-me')}}">درباره آلاکالا</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-12">
                <label for="email" class="nav-item"><span class="text-light">عضویت در خبر نام آلاکالا :</span></label>
                <form action="{{route('email.save')}}" method="POST" dir="ltr" class="d-block">
                    @csrf
                    <div class="input-group ml-3">
                        <input type="text" name="email" id="email" dir="rtl" class="form-control form-control-lg d-block" placeholder="آدرس ایمیل خود را وارد کنید...">
                        <div class="input-group-append">
                            <button class="input-group-text bg-yellow text-dark" type="submit"><span><i class="fa fa-envelope fa-lg"></i></span></button>
                        </div>
                    </div>
                </form>
                <h6><span class="text-light">آلاکالا را در شبکه های اجتماعی دنبال کنید:</span></h6>
                <div class="text-right py-2 font-size-30">
                    <span class="p-2"><a href="#"><i class="fab fa-instagram text-white"></i></a></span>
                    <span class="p-2"><a href="https://t.me/ZagrosExchange" target="_blank"><i class="fab fa-telegram text-white"></i></a></span>
                </div>
            </div>
        </div>
        <div class="row">
            <hr class="w-100 bg-light">
        </div>
        <div class="row px-5 py-3 txt-justify ">
            <div class="col-md-5 text-light">
                <span class="font-size-16 text-light"> هفت روز هفته ، ۲۴ ساعت شبانه‌روز پاسخگوی شما هستیم</span>
                <br>
                <span class="font-size-16 pt-2 text-light"> آدرس ایمیل : info@alakala.com</span>
            </div>
            <div class="col-md-5">
                <span class="font-size-16 text-light"> تلفن تماس و فکس: 44241700 - 044 ، 44241701 - 044 </span>
            </div>
            <div class="col-md-2">

            </div>
        </div>
    </div>
    <div class="row">
        <hr class="w-100 bg-light">
    </div>
    <div class="row px-5 py-3">
        <div class="col-md-8 text-light text-justify">
            <h5 class="font-size-18">آلاکالا فروش مستقیم، بررسی و خرید آنلاین</h5>
            <p class="text-content-justify"><span class=" font-size-14 font-height">ما به خوبی می دانیم اگر بتوانیم به چند مسئله مهم که عبارتند از سرعت ؛ کیفیت و اصالت ؛ مناسبترین قیمت و تضمین تعویض کالا را پاسخی در شأن شما بدهیم ؛ به سود خودمان خواهد بود. آلاکالا با سرعت و کیفیت مناسب و تکیه بر قیمت مناسب این فرصت را فراهم کرده است که شما با حفظ کیفیت خواسته هایتان از خریدتان خوشنود باشید. سایت آلاکالا با بانک های ملت و ملی برای پرداخت آنلاین طرف قرار داد است.</span></p>
        </div>
        <div class="col-md-4">
        </div>
    </div>
    <div class="row">
        <hr class="w-100 bg-light">
    </div>
    <div class="row">
        <div class="col-md-12 col-12">
            <h6 class="text-center text-light">
                تمام حقوق این وب سایت متعلق به فروشگاه آلاکالا می باشد.
            </h6>
            <h6 class="text-center font-en text-light ">
                Copyright ©2020 All rights reserved | This Web is made by <a class="text-light" href="https://www.github.com/hassanhassanzadeh/" target="_blank" >Hassan Hassanzadeh</a>
            </h6>
        </div>
    </div>
</footer>
