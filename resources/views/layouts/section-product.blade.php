
    @if(! isset($products))
        <div id="root" class="d-flex flex-column m-3 rounded">
    @foreach($products as $keys=>$product)
    @foreach($product->categories as $key=>$category)
        @if($key==0 && $keys==0)
    <h5 id="carousel-header" class="d-inline-block py-3 pl-4 align-self-start position-relative"> {{$category->name}} </h5>
        @endif
    @endforeach
    @endforeach

    <button id="next" class="controller_btn m-3" onclick="next()"> <span><i class="fa fa-angle-right font-size-30"></i></span> </button>
    <button id="prev" class="controller_btn m-3" onclick="prev()"> <span><i class="fa fa-angle-left font-size-30"></i></span></button>
    <div id="carousel-container" class="d-flex flex-column align-items-start flex-nowrap py-3">

        <ul class="d-flex flex-row flex-nowrap p-0">
            @foreach($products as $product)
                <a href="{{url('/products',$product->slug)}}" class="text-decoration-none text-dark">
            <li class="d-flex flex-column align-items-end carousel-item">
                @foreach($product->photos as $key=>$photo)
                    @if($key==0)
                        <img src="{{$photo->path}}" class="product-ala"  alt="product_img" />
                    @endif
                @endforeach
                <figcaption>{{$product->title}}</figcaption>
                <section class="d-flex flex-row justify-content-between align-items-end">
                    <span class="mx-2"><del>{{$product->presentPrice()}}</del></span><small>تومان</small>
                    <h6 class="mx-4">{{$product->discountPresent()}}</h6>
                </section>
            </li>
                </a>
                @endforeach
        </ul>
    </div>

</div>
    @endif
