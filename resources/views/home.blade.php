@extends('layouts.app')

@section('title')
صفحه اصلی | آلاکالا
@endsection

@include('layouts.navigation')

@section('content')

    @include('admin.partials.index-success')
    @include('admin.partials.form-errors')

    @include('layouts.section-product')

    <div class="container-fluid">
        <div class="row m-2">
            <div class="col-md-12 col-sm-12 col-12 ">

                <div class="row">
                    @foreach($products as $key_d=>$product)
                        @if($key_d<4)
                            <div class="col-md-3 col-12">
                                <div class="card card-pk">
                                    <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$product->title}}</h5>
                                        @if($product->qty!=0)
                                            <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                            <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                            <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                        @elseif($product->qty==0)
                                            <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="row">
                    @foreach($products as $key_d=>$product)
                        @if($key_d>=4 && $key_d < 8)
                            <div class="col-md-3 col-12">
                                <div class="card card-pk">
                                    <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$product->title}}</h5>
                                        @if($product->qty!=0)
                                            <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                            <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                            <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                        @elseif($product->qty==0)
                                            <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="row">
                    @foreach($products as $key_d=>$product)
                        @if($key_d>=8 && $key_d < 12)
                            <div class="col-md-3 col-12">
                                <div class="card card-pk">
                                    <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$product->title}}</h5>
                                        @if($product->qty!=0)
                                            <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                            <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                            <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                        @elseif($product->qty==0)
                                            <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="row">
                    @foreach($products as $key_d=>$product)
                        @if($key_d>=12 && $key_d < 16)
                            <div class="col-md-3 col-12">
                                <div class="card card-pk">
                                    <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$product->title}}</h5>
                                        @if($product->qty!=0)
                                            <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                            <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                            <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                        @elseif($product->qty==0)
                                            <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="row">
                    @foreach($products as $key_d=>$product)
                        @if($key_d>=16 && $key_d < 20)
                            <div class="col-md-3 col-12">
                                <div class="card card-pk">
                                    <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$product->title}}</h5>
                                        @if($product->qty!=0)
                                            <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                            <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                            <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                        @elseif($product->qty==0)
                                            <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

            </div>
        </div>
    </div>
    @include('layouts.footer')

@endsection


