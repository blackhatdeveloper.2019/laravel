
@extends('admin.layouts.master')

@section('title')
    اعضای خبرنامه
@endsection

@section('content')
    <div class="container-fluid">

        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/news')}}">اعضای خبرنامه</a></span>
            </div>
        </div>
        <br>

        <div class="h4 text-center mb-4">
            <span class="text-center">اعضای خبر نامه</span>
        </div>

        @include('admin.partials.index-success')
        <div class="row">
            <table class="table table-active table-striped table-hover shadow">
                <thead class="thead-light">
                <tr>
                    <th>شناسه</th>
                    <th>ایمیل</th>
                    <th>وضعیت</th>
                    <th>تاریخ عضویت</th>
                </tr>
                </thead>
                <tbody>
                @foreach($news as $new)
                    <tr>
                        <td>{{$new->id}}</td>
                        <td >{{$new->email}}</td>
                        @if($new->status == 1)
                            <td><span class="badge badge-success">فعال</span></td>
                        @else
                            <td><span class="badge badge-danger">غیرفعال</span></td>
                        @endif
                        <td>{{\Hekmatinasser\Verta\Verta::instance($new->created_at)}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <!-- /.table-responsive -->
        <div class="m-auto">
            <span class="align-items-center">{{$news->links()}}</span>
        </div>
    </div>
@endsection
