@extends('admin.layouts.master')

@section('title')
    صفحه مدیرت سایت
@endsection

@section('content')
    <div class="alert alert-secondary d-block p-1 m-0 shadow">
        <div class="pr-1">
            <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span>
        </div>
    </div>
    <br>
    @include('admin.partials.index-success')
    <div class="card-deck">
        <div class="card text-white bg-primary mb-3 border-0 shadow">
            <div class="card-body">
                <i class="fas fa-chart-bar h5 float-left"></i>
                <h5 class="card-title">53000</h5>
                <p class="card-text">تعداد بازدید روزانه</p>
                <canvas id="chart1" class="w-100" height="100"></canvas>
            </div>
        </div>
        <div class="card text-white bg-info mb-3 border-0 shadow">
            <div class="card-body">
                <i class="fas fa-users h5 float-left"></i>
                <h5 class="card-title">25000</h5>
                <p class="card-text">تعداد کاربران جدید</p>
                <canvas id="chart2" class="w-100" height="100"></canvas>
            </div>
        </div>
        <div class="card text-white bg-danger mb-3 border-0 shadow">
            <div class="card-body">
                <i class="fas fa-chart-pie h5 float-left"></i>
                <h5 class="card-title">15000</h5>
                <p class="card-text">تعداد فروش روزانه</p>
                <canvas id="chart3" class="w-100" height="100"></canvas>
            </div>
        </div>
        <div class="card text-white bg-success mb-3 border-0 shadow">
            <div class="card-body">
                <i class="fas fa-chart-line h5 float-left"></i>
                <h5 class="card-title">924</h5>
                <p class="card-text">تعداد محصولات جدید</p>
                <canvas id="chart4" class="w-100" height="100"></canvas>
            </div>
        </div>

    </div>

    <!-------------Card deck--------------->

    <div class="card text-dark bg-white my-3 border-0 shadow">
        <div class="card-body">
            <div class="d-flex mb-4 justify-content-between align-items-center">
                <h5 class="card-title">آمار بازدید سایت</h5>
                <div class="btn-group btn-group-toggle dir-ltr" data-toggle="buttons">
                    <label class="btn btn-outline-secondary">
                        <input type="radio" name="options" id="option1" autocomplete="off">
                        <span>سال</span>
                    </label>
                    <label class="btn btn-outline-secondary">
                        <input type="radio" name="options" id="option2" autocomplete="off">
                        <span>ماه</span>
                    </label>
                    <label class="btn btn-outline-secondary active">
                        <input type="radio" name="options" id="option3" autocomplete="off" checked>
                        <span>روز</span>
                    </label>
                </div>

                <button type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="گرفتن خروجی">
                    <i class="fas fa-file-export"></i>
                </button>
            </div>
            <canvas id="visit" class="w-100" height="300"></canvas>
        </div>
        <div class="card-footer">
            <div class="row text-center">
                <div class="col-12 col-sm-6 col-md my-2 my-md-0">
                    <strong class="text-muted">بازدید کل</strong>
                    <p class="my-3">53000 کاربر (40%)</p>
                    <div class="progress" style="height: 10px;">
                        <div class="progress-bar bg-info" style="width: 40%;"></div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md my-2 my-md-0">
                    <strong class="text-muted">بازدید های یکتا</strong>
                    <p class="my-3">49000 کاربر (35%)</p>
                    <div class="progress" style="height: 10px;">
                        <div class="progress-bar bg-warning" style="width: 35%;"></div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md my-2 my-md-0">
                    <strong class="text-muted">بازدید صفحات سایت</strong>
                    <p class="my-3">85000 کاربر (80%)</p>
                    <div class="progress" style="height: 10px;">
                        <div class="progress-bar bg-primary" style="width: 80%;"></div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md my-2 my-md-0">
                    <strong class="text-muted">نرخ خروج</strong>
                    <p class="my-3">45%</p>
                    <div class="progress" style="height: 10px;">
                        <div class="progress-bar bg-danger" style="width: 45%;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!----------------Visits--------------->

    <div class="card text-dark bg-white my-4 border-0 shadow">
        <div class="card-body">
            <h5 class="card-title">گزارشات فروش</h5>

            <div class="table-responsive">
                <table class="table table-bordered ">
                    <thead class="thead-light">
                    <tr>
                        <th>نام محصول</th>
                        <th>ایمیل کاربر</th>
                        <th>درگاه پرداخت</th>
                        <th>وضعیت</th>
                        <th>زمان تراکنش</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>آموزش بوت استرپ 4 | جادوی رابط کاربری</td>
                        <td>k@gmail.com</td>
                        <td>بانک ملت</td>
                        <td><span class="btn btn-success d-block shadow">تکمیل شده</span></td>
                        <td>
                            10 دقیقه پیش
                        </td>
                    </tr>
                    <tr>
                        <td>آموزش مقدماتی تا پیشرفته جاوا اسکریپت</td>
                        <td>y@gmail.com</td>
                        <td>زرین پال</td>
                        <td><span class="btn btn-danger d-block shadow">لغو شده</span></td>
                        <td>
                            20 دقیقه پیش
                        </td>
                    </tr>
                    <tr>
                        <td>آموزش مقدماتی تا پیشرفته جی کوئری</td>
                        <td>s@gmail.com</td>
                        <td>بانک ملت</td>
                        <td><span class="btn btn-info d-block shadow">در انتظار پرداخت</span></td>
                        <td>
                            1 ساعت پیش
                        </td>
                    </tr>
                    <tr>
                        <td>آموزش کامل CSS و CSS3</td>
                        <td>r@gmail.com</td>
                        <td>زرین پال</td>
                        <td><span class="btn btn-success d-block shadow">تکمیل شده</span></td>
                        <td>
                            2 ساعت پیش
                        </td>
                    </tr>
                    <tr>
                        <td>آموزش طراحی سایت بدون کدنویسی</td>
                        <td>t@gmail.com</td>
                        <td>بانک ملت</td>
                        <td><span class="btn btn-info d-block shadow">در انتظار پرداخت</span></td>
                        <td>
                            3 ساعت پیش
                        </td>
                    </tr>
                    <tr>
                        <td>آموزش راه اندازی فروشگاه اینترنتی</td>
                        <td>p@gmail.com</td>
                        <td>زرین پال</td>
                        <td><span class="btn btn-danger d-block shadow">لغو شده</span></td>
                        <td>
                            4 ساعت پیش
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
