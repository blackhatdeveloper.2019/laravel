@extends('admin.layouts.master')

@section('title')
    کاربران
@endsection

@section('content')


<div class="container-fluid">
    <div class="alert alert-secondary d-block p-1 m-0 shadow">
        <div class="pr-1">
            <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{url('/administrator/users')}}">کاربران</a></span>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="h4 text-center align-items-center">
                <span class="text-center">کاربران</span>
                <span class="float-left">
                    <a href="{{route('users.create')}}" class="btn btn-lg btn-info text-white">
                        <i class="fas fa-plus fa-lg pl-2"></i>
                        <span>جدید</span>
                    </a>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 m-auto pt-2">

        @include('admin.partials.index-success')
        <table class="table table-hover table-bordered table-striped shadow">
            <thead class="thead-light">
            <tr>
                <th>نام </th>
                <th>موبایل </th>
                <th>وضعیت </th>
                <th>تاریخ ایجاد</th>
                <th>عملیات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users_pro as $users)
                <tr>
                    @if($users->id!=Auth::user()->id)
                    <td>{{$users->name}}</td>
                    <td>{{$users->mobile}}</td>
                    <td>
                        @if(empty($users->active))
                            <span class="badge badge-danger">غیر فعال</span>
                            @else
                            <span class="badge badge-success"> فعال</span>
                        @endif

                    </td>
                    <td>{{\Hekmatinasser\Verta\Verta::instance($users->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}</td>
                    <td>

                        <a href="{{route('users.edit',$users->id)}}" class="btn btn-warning text-white d-inline ml-2">
                            <span>ویرایش</span>
                        </a>
                        <form action="{{route('users.destroy',$users->id)}}" method="POST" class="d-inline">
                            @csrf
                            {{method_field('DELETE')}}
                            <button type="submit" class="btn btn-danger">
                                <span>حذف</span>
                            </button>
                        </form>

                    </td>
                        @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    </div>
    <div class="row">
        <div class="m-auto">
                <span class="align-items-center">
                    {{$users_pro->links()}}
                </span>
        </div>
    </div>
</div>
@endsection
