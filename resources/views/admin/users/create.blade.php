@extends('admin.layouts.master')

@section('title')
   ایجاد کاربر جدید
@endsection

@section('content')

    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/users')}}">کاربران</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('users.create')}}"> ایجاد کاربر جدید</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center"> ایجاد کاربر </span>
        </div>
        <div class="col-8 m-auto ">

            @include('admin.partials.form-errors')


            <form action="{{route('users.store')}}" method="POST" autocomplete="off">
                @csrf

                <div class="form-group d-block">
                    <label for="name">نام  <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" class="form-control form-control-lg" id="name" name="name" placeholder="نام کاربر را وارد کنید..." autocomplete="name">
                </div>

                <div class="form-group d-block">
                    <label for="mobile">موبایل  <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" class="form-control form-control-lg" id="mobile" name="mobile" placeholder="شماره موبایل کاربر را وارد کنید..."  autocomplete="email">
                </div>

                <div class="form-group d-block">
                    <label for="password">کلمه عبور  <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="password" class="form-control form-control-lg" id="password" name="password"  placeholder="کلمه کاربر را وارد کنید..." autocomplete="password">
                </div>

                <div class="form-group">
                    <label for="status"></label>
                    <select name="status" id="status" class="form-control">
                        <option  value="0" selected>غیر فعال</option>
                        <option  value="1">فعال</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-info btn-block">ذخیره </button>
            </form>
        </div>
    </div>
@endsection
