@extends('admin.layouts.master')

@section('title')
    ویرایش کاربر
@endsection

@section('content')

    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/users')}}">کاربران</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('users.edit',$users->name)}}"> ویرایش کاربر</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('users.edit',$users->name)}}">{{$users->name}}</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center"> ویرایش کاربر {{$users->name}} </span>
        </div>
        <div class="col-8 m-auto ">

            @include('admin.partials.form-errors')


            <form action="{{route('users.update',$users->id)}}" method="POST">
                @csrf
                {{method_field('PATCH')}}

                <div class="form-group d-block">
                    <label for="name">نام  <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" value="{{$users->name}}" class="form-control form-control-lg" id="name" name="name" placeholder="نام کاربر را وارد کنید...">
                </div>

                <div class="form-group d-block">
                    <label for="mobile">شماره موبایل  <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" value="{{$users->mobile}}" class="form-control form-control-lg" id="mobile" name="mobile" placeholder="شماره موبایل کاربر را وارد کنید...">
                </div>
                <div class="form-group d-block">
                    <label for="password">کلمه عبور</label>
                    <input type="password" id="password" name="password"  class="form-control form-control-lg" placeholder="جهت تغییر کلمه عبور جدید کلمه عبور را وارد کنید..." autocomplete="new-password">
                </div>
                <div class="form-group">
                    <label for="status"></label>
                    <select name="status" id="status" class="form-control">
                        <option @if(empty($users->active)) selected @endif value="0">غیر فعال</option>
                        <option @if(! empty($users->active)) selected @endif value="1">فعال</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-info btn-block">بروزرسانی </button>
            </form>
        </div>
    </div>
@endsection
