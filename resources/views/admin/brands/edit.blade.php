@extends('admin.layouts.master')

@section('title')
    ویرایش برند
@endsection

@section('style')

    <link rel="stylesheet" href="{{asset('backend/css/dropzone.min.css')}}">

@endsection

@section('content')

    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">محصولات</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/brands')}}">برند ها</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('brands.edit',$brand->id)}}">ویرایش برند</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('brands.edit',$brand->id)}}">{{$brand->title}}</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center">ویرایش برند {{$brand->title}} </span><br><br>
            <img class="img image-grayscale" src="{{$brand->photo->path}}" width="140" height="140" alt="">
        </div>
        <div class="col-9 m-auto ">

            @include('admin.partials.form-errors')


            <form action="{{route('brands.update',$brand->id)}}" method="POST">
                @csrf
                {{method_field('PATCH')}}
                <div class="form-group d-block">
                    <label for="title">عنوان برند <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" value="{{$brand->title}}" class="form-control form-control-lg" id="title" name="title" placeholder="عنوان برند را وارد کنید...">
                </div>

                <div class="form-group d-block">
                    <label for="textareaDescription">درباره برند </label>
                    <textarea class="form-control form-control-lg" id="textareaDescription" name="description" placeholder="توضیحاتی درباره برند وارد کنید...">{{$brand->description}}</textarea>
                </div>
                <div class="form-group">
                    <input type="hidden" name="photo_id" id="photo_id" value="{{$brand->photo_id}}">
                    <div id="photo" class="dropzone">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">بروز رسانی</button>
            </form>
        </div>
    </div>
@endsection

@section('script')

    <script type="text/javascript" src="{{asset('backend/plugins/ckeditor/ckeditor.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/dropzone.min.js')}}"></script>

    <script>
        Dropzone.autoDiscover=false;
        var drop = new Dropzone('#photo', {
            addRemoveLinks: true,
            maxFiles: 1,
            url: "{{ route('photos.upload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                document.getElementById('photo_id').value = response.photo_id
            }
        });
        CKEDITOR.replace('textareaDescription',{
            customConfig:'config.js',
            toolbar:'sample',
            language:'fa',

        })

    </script>
@endsection
