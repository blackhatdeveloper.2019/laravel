@extends('admin.layouts.master')

@section('title')
    برند ها
@endsection

@section('content')


    <div class="container-fluid">

        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">محصولات</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/brands')}}">برند ها</a></span>
            </div>
        </div>
        <br>

        <div class="h4 text-center mb-4">
            <span class="text-center">برند ها</span>
            <span class="float-left">
                <a href="{{route('brands.create')}}" class="btn btn-lg btn-info text-white">
                    <i class="fas fa-plus fa-lg pl-2"></i>
                    <span>جدید</span>
                </a>
            </span>
        </div>

        @include('admin.partials.index-success')
        <div class="row">
               <table class="table table-active table-striped table-hover shadow">
                <thead>
                <tr>
                    <th>نام برند</th>
                    <th>درباره برند</th>
                    <th>عکس برند</th>
                    <th>تاریخ ایجاد</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($brands as $brand)
                    <tr>
                        <td>{{$brand->title}}</td>
                        <td>{{substr($brand->description,0,100)}}</td>
                        <td>

                            <img src="{{$brand->photo->path}}" width="50" height="50" alt="">

                        </td>
                        <td>{{\Hekmatinasser\Verta\Verta::instance($brand->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}</td>
                        <td>

                            <a href="{{route('brands.edit',$brand->id)}}" class="btn btn-warning text-white d-inline ml-2">
                                <span>ویرایش</span>
                            </a>
                            <form action="{{route('brands.destroy',$brand->id)}}" method="POST" class="d-inline">
                                @csrf
                                {{method_field('DELETE')}}
                                <button type="submit" class="btn btn-danger">
                                    <span>حذف</span>
                                </button>
                            </form>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
           </div>
               <div class="m-auto">
                <span class="align-items-center">{{$brands->links()}}</span>
            </div>
        </div>


@endsection

