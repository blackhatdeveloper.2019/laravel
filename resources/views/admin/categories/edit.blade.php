@extends('admin.layouts.master')

@section('title')
    ویرایش دسته
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('backend/css/dropzone.min.css')}}">
@endsection

@section('content')

    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">محصولات</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/categories')}}">دسته بندی ها</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('categories.edit',$category->id)}}">ویرایش دسته بندی</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('categories.edit',$category->id)}}">{{$category->name}}</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center">ویرایش دسته {{$category->name}} </span>
        </div>
        <div class="col-8 m-auto ">

            @include('admin.partials.form-errors')


            <form action="{{route('categories.update',$category->id)}}" method="POST">
                @csrf
                {{method_field('PATCH')}}
                @if($category->photo_id)
                <div class="text-center">
                    <img src="{{$category->photo->path}}" width="120" height="150" alt="">
                </div>
                @endif
                <div class="form-group d-block">
                    <label for="name"> نام دسته <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" value="{{$category->name}}" class="form-control form-control-lg" id="name" name="name" placeholder="نام دسته را وارد کنید...">
                </div>
                <div class="form-group">
                    <label for="category_parent">دسته والد</label>
                    <select name="category_parent" id="" class="form-control">
                        <option value="">بدون والد</option>
                        @foreach($categories as $category_data)
                            <option value="{{$category_data->id}}" @if($category->parent_id == $category_data->id) selected @endif>{{$category_data->name}}</option>
                            @if(count($category_data->childrenRecursive) > 0)
                                @include('admin.partials.category', ['categories'=>$category_data->childrenRecursive, 'level'=>1, 'selected_category'=>$category])
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="photo_id">عکس دسته بندی</label>
                    <input type="hidden" name="photo_id" id="photo_id" value="{{$category->photo_id}}">
                    <div id="photo" class="dropzone">
                    </div>
                </div>

                <div class="form-group d-block">
                    <label for="meta_title"> عنوان متا <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" value="{{$category->meta_title}}" class="form-control form-control-lg" id="meta_title" name="meta_title" placeholder="عنوان متای دسته را وارد کنید...">
                </div>
                <div class="form-group d-block">
                    <label for="meta_desc"> توضیحات متا  <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <textarea  class="form-control form-control-lg" id="meta_desc" name="meta_desc" placeholder="توضیحات متای صفحه دسته را وارد کنید...">{{$category->meta_desc}}</textarea>
                </div>
                <div class="form-group d-block">
                    <label for="meta_keywords"> کلمات کلیدی <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" value="{{$category->meta_keywords}}" class="form-control form-control-lg" id="meta_keywords" name="meta_keywords" placeholder="کلمات کلیدی دسته را وارد کنید...">
                </div>

                <button type="submit" class="btn btn-info btn-block">ذخیره اطلاعات</button>
            </form>
        </div>
    </div>
@endsection

@section('script')

    <script type="text/javascript" src="{{asset('backend/js/dropzone.min.js')}}"></script>

    <script>
        Dropzone.autoDiscover=false;
        var drop = new Dropzone('#photo', {
            addRemoveLinks: true,
            maxFiles: 1,
            url: "{{ route('photos.upload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                document.getElementById('photo_id').value = response.photo_id
            }
        });

    </script>
@endsection
