@extends('admin.layouts.master')

@section('title')
 تعیین ویژگی دسته بندی ها
@endsection

@section('content')
    <section class="content">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">محصولات</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/categories')}}">دسته بندی ها</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('categories.edit',$category->id)}}">ویرایش دسته بندی</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('categories.indexSetting', $category->id)}}">تنظیمات دسته بندی</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('categories.indexSetting', $category->id)}}">{{$category->name}}</a></span>
            </div>
        </div>
        <br>
            <div class="border-bottom pb-3">
                <h3 class="text-center">تعیین ویژگی دسته بندی {{$category->name}}</h3>
            </div>
                <div class="row">
                    <div class="col-md-8 m-auto pt-3 d-block">
                        @include('admin.partials.form-errors')

                        <form method="post" action="/administrator/categories/{{$category->id}}/settings">
                            @csrf
                            <div class="form-group">
                                <label for="attributeGroups">ویژگی های دسته بندی {{$category->name}}</label>
                                <select name="attributeGroups[]" id="" class="form-control" multiple>
                                    @foreach($attributeGroups as $attributeGroup)
                                        <option value="{{$attributeGroup->id}}" @if(in_array($attributeGroup->id, $category->attributeGroups->pluck('id')->toArray())) selected @endif>{{$attributeGroup->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-info btn-block" value="ذخیره">
                            </div>
                        </form>
                    </div>
                </div>
    </section>

@endsection
