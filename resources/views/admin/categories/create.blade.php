@extends('admin.layouts.master')

@section('title')
   ایجاد دسته
@endsection

@section('style')

    <link rel="stylesheet" href="{{asset('backend/css/dropzone.min.css')}}">

@endsection

@section('content')

    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">محصولات</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/categories')}}">دسته بندی ها</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('categories.create')}}">ایجاد دسته بندی جدید جدید</a></span>
            </div>
        </div>
        <br>


        <div class="h4 text-center mb-4">
            <span class="text-center">ایجاد دسته بندی جدید </span>
        </div>
        <div class="col-8 m-auto ">

           @include('admin.partials.form-errors')


            <form action="{{route('categories.store')}}" method="POST">
                @csrf
                <div class="form-group d-block">
                    <label for="name"> نام دسته <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" class="form-control form-control-lg" id="name" name="name" placeholder="نام دسته را وارد کنید...">
                </div>

                <div class="form-group">
                    <label for="category_parent">دسته والد</label>
                    <select name="category_parent" id="category_parent" class="form-control">
                        <option value="">بدون والد</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @if(count($category->childrenRecursive) > 0)
                                @include('admin.partials.category', ['categories'=>$category->childrenRecursive, 'level'=>1])
                            @endif
                        @endforeach
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="photo_id">عکس دسته بندی</label>
                    <input type="hidden" name="photo_id" id="photo_id" value="">
                    <div id="photo" class="dropzone"></div>
                </div>

                <div class="form-group d-block">
                    <label for="meta_title"> عنوان سئو <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" class="form-control form-control-lg" id="meta_title" name="meta_title" placeholder="عنوان متای دسته را وارد کنید...">
                </div>
                <div class="form-group d-block">
                    <label for="meta_description"> توضیحات سئو  <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <textarea  class="form-control form-control-lg" id="meta_desc" name="meta_desc" placeholder="توضیحات متای صفحه دسته را وارد کنید..."></textarea>
                </div>
                <div class="form-group d-block">
                    <label for="meta_keywords"> کلمات کلیدی سئو <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" class="form-control form-control-lg" id="meta_keywords" name="meta_keywords" placeholder="کلمات کلیدی دسته را وارد کنید...">
                </div>

                <button type="submit" class="btn btn-info btn-block">ذخیره اطلاعات</button>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('backend/js/dropzone.min.js')}}"></script>

    <script>
        Dropzone.autoDiscover=false;
        var photosGallery = []
        var drop = new Dropzone('#photo',{
            addRemoveLinks:true,
            maxFiles :1,
            url:"{{route('photos.upload')}}",
            sending:function(file,xhr,formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success:function(file,response){
                document.getElementById('photo_id').value = response.photo_id
                //photosGallery.push(response.photo_id)
            }
        });
        // brandGallery =function(){
        //    document.getElementById('photo_id').value=photosGallery
        //}


    </script>
@endsection
