@extends('admin.layouts.master')

@section('title')
    دسته بندی ها
@endsection

@section('content')


<div class="container-fluid">
    <div class="alert alert-secondary d-block p-1 m-0 shadow">
        <div class="pr-1">
            <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">محصولات</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{url('/administrator/categories')}}">دسته بندی ها</a></span>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="h4 text-center align-items-center">
                <span class="text-center">دسته بندی ها</span>
                <span class="float-left">
                    <a href="{{route('categories.create')}}" class="btn btn-lg btn-info text-white">
                        <i class="fas fa-plus fa-lg pl-2"></i>
                        <span>جدید</span>
                    </a>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 m-auto pt-3">

            @include('admin.partials.index-success')


                @if(Session::has('error_category'))
                    <div class="alert alert-danger">
                        <div>{{session('error_category')}}</div>
                    </div>
                @endif

                    <table class="table table-active table-striped table-hover shadow">
                        <thead class="thead-light">
                        <tr>
                            <th>شناسه</th>
                            <th>عنوان</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{$category->id}}</td>
                                <td>{{$category->name}}</td>
                                <td class="custom-control-inline">
                                        <a class="btn btn-warning text-white mx-1" href="{{route('categories.edit', $category->id)}}">ویرایش</a>
                                        <form method="post" action="/administrator/categories/{{$category->id}}">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-danger mx-1">حذف</button>
                                        </form>
                                        <a class="btn btn-primary mx-1" href="{{route('categories.indexSetting', $category->id)}}">تنظیمات</a>
                                </td>
                            </tr>

                            @if(count($category->childrenRecursive) > 0)
                                @include('admin.partials.category_list', ['categories'=>$category->childrenRecursive, 'level'=>1])
                            @endif
                        @endforeach
                        </tbody>
                    </table>
            </div>
    <div class="row">
        <div class="m-auto">
                <span class="align-items-center">
                    {{$categories->links()}}
                </span>
        </div>
    </div>
</div>
@endsection
