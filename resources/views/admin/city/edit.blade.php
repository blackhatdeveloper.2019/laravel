@extends('admin.layouts.master')

@section('title')
    ویرایش شهرستان
@endsection

@section('content')
    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/coupons')}}">ناحیه ها</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/city')}}">شهرستان ها</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('city.edit',$city->name)}}">ویرایش شهرستان {{$city->name}}</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center">ویرایش شهر {{$city->name}} </span>
        </div>
                <div class="row">
                    <div class="col-md-8 m-auto">
                        @include('admin.partials.form-errors')
                        <form id="myForm" method="post" action="{{route('city.update',$city->id)}}">
                            @csrf
                            {{method_field('PATCH')}}
                            <div class="form-group">
                                <label for="province"> نام استان <span class="text-danger">*</span></label>
                                <select name="province" class="form-control" id="province">
                                    @foreach($items as $state)
                                        <option value="{{$state->id}}" @if($state->id==$city->province_id) selected @endif >{{$state->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="name">نام شهر<span class="text-danger">*</span></label>
                                <input type="text" id="name" name="name" value="{{$city->name}}" class="form-control" placeholder="نام شهر را وارد کنید...">
                            </div>

                            <button type="submit" class="btn btn-success btn-block">ذخیره</button>
                        </form>
                    </div>
                </div>

            </div>

@endsection
