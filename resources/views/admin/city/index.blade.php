@extends('admin.layouts.master')

@section('title')
   شهرستان ها
@endsection

@section('content')

    <div class="container-fluid">

        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/coupons')}}">ناحیه ها</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/city')}}">شهرستان ها</a></span>
            </div>
        </div>
        <br>

        <div class="h4 text-center mb-4">
            <span class="text-center">ایجاد شهر جدید</span>
            <span class="float-left">
                <a href="{{route('city.create')}}" class="btn btn-lg btn-info text-white">
                    <i class="fas fa-plus fa-lg pl-2"></i>
                    <span>جدید</span>
                </a>
            </span>
        </div>

        @include('admin.partials.index-success')
        <div class="row">
                    <table class="table table-active table-striped table-hover shadow text-center">
                        <thead class="thead-light">
                        <tr>
                            <th>شناسه</th>
                            <th>نام استان</th>
                            <th>نام شهر</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cities as $city)
                            <tr>
                                <td>{{$city->id}}</td>
                                <td>{{$city->province->name}}</td>
                                <td>{{$city->name}}</td>
                                <td class="custom-control-inline">
                                    <a class="btn btn-warning text-white ml-2" href="{{route('city.edit', $city->id)}}">ویرایش</a>
                                    <div class="display-inline-block">
                                        <form method="post" action="/administrator/city/{{$city->id}}">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-danger">حذف</button>
                                        </form>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            <div class="m-auto">
                <span class="align-items-center">{{$cities->links()}}</span>
            </div>
    </div>



@endsection
