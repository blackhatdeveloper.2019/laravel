
@extends('admin.layouts.master')

@section('title')
    نظرات کاربران
@endsection

@section('content')
    <div class="container-fluid">

        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/news')}}">نظر کاربران</a></span>
            </div>
        </div>
        <br>

        <div class="h4 text-center mb-4">
            <span class="text-center">نظر کاربران</span>
        </div>

        @include('admin.partials.index-success')
        <div class="row">
            <table class="table table-active table-striped table-hover shadow">
                <thead class="thead-light">
                <tr>
                    <th>نام کاربر</th>
                    <th>شماره تماس</th>
                    <th>نام محصول</th>
                    <th>متن نظر</th>
                    <th>وضعیت</th>
                    <th>تاریخ </th>
                    <th>عملیات </th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>
                            @foreach($post->users as $users)
                            {{$users->name}}
                        </td>
                        <td >
                            {{$users->mobile}}
                        </td>
                            @endforeach
                        <td >{{$post->product->title}}</td>
                        <td >{!! $post->description !!}</td>
                        @if($post->status == 1)
                            <td><span class="badge badge-success">منتشر شده</span></td>
                        @else
                            <td><span class="badge badge-danger">منتشر نشده</span></td>
                        @endif
                        <td>{{\Hekmatinasser\Verta\Verta::instance($post->created_at)}}</td>
                        <td>
                            <a href="{{route('posts.edit',$post->id)}}" class="btn btn-warning">ویرایش</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <!-- /.table-responsive -->
        <div class="m-auto">
            <span class="align-items-center">{{$posts->links()}}</span>
        </div>
    </div>
@endsection
