@extends('admin.layouts.master')

@section('title')
    ویرایش پست ها
@endsection

@section('content')

    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/posts')}}">نظر کاربران</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/posts/'.$post->id.'/edit')}}">ویرایش نظرات</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('posts.edit',$post->id)}}">{{$post->title}}</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center">ویرایش نظر {{$post->title}} </span><br><br>
        </div>
        <div class="col-12 m-auto col-md-9">

            @include('admin.partials.form-errors')


            <form action="{{route('posts.update',$post->id)}}" method="POST">
                @csrf
                {{method_field('PATCH')}}

                <div class="form-group d-block">
                    <label for="title">عنوان نظر </label>
                    <input type="text" value="{{$post->title}}" class="form-control form-control-lg" id="title" name="title" placeholder="عنوان نظر را وارد کنید...">
                </div>
                <div class="form-group d-block">
                    <label for="strong_point">نقاط قوت </label>
                    <input type="text" value="{{$post->strong_point}}" class="form-control form-control-lg" id="strong_point" name="strong_point" placeholder="نقاط قوت را وارد کنید...">
                </div>
                <div class="form-group d-block">
                    <label for="weak_point">نقاط ضعف </label>
                    <input type="text" value="{{$post->weak_point}}" class="form-control form-control-lg" id="weak_point" name="weak_point" placeholder="نقاط ضعف را وارد کنید...">
                </div>
                <div class="form-group d-block">
                    <label for="description"> متن نظر <span class="text-danger">*</span></label>
                    <textarea class="form-control form-control-lg" id="description" name="description" placeholder="متن نظر وارد کنید...">{{$post->description}}</textarea>
                </div>
                <div class="form-group d-block">
                    <label for="status">وضعیت</label>
                    <select name="status" id="status" class="form-control form-control-lg">
                        <option value="1" @if($post->status == 1) selected @endif>منتشر شده</option>
                        <option value="0" @if($post->status == 0) selected @endif>منتشر نشد</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-info btn-block">بروز رسانی</button>
            </form>
        </div>
    </div>
@endsection

