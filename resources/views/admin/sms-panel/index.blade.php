@extends('admin.layouts.master')

@section('title')
   پنل پیام کوتاه
@endsection

@section('content')


<div class="container-fluid">
    <div class="alert alert-secondary d-block p-1 m-0 shadow">
        <div class="pr-1">
            <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{url('/administrator/sms-panel')}}">پنل پیام کوتاه</a></span>
        </div>
    </div>
    <div class="row my-4">
        <div class="col-md-12">
            <div class="h4 text-center align-items-center">
                <span class="text-center">پنل ها</span>
                <span class="float-left">
                    <a href="{{route('sms-panel.create')}}" class="btn btn-lg btn-info text-white">
                        <i class="fas fa-plus fa-lg pl-2"></i>
                        <span>جدید</span>
                    </a>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 m-auto pt-2">

        @include('admin.partials.index-success')
        <table class="table table-hover table-bordered table-striped">
            <thead class="thead-light">
            <tr>
                <th>نام پنل</th>
                <th>آدرس سرویس دهنده</th>
                <th>نام کاربری </th>
                <th>کلمه عبور </th>
                <th>شماره سرویس دهنده</th>
                <th>کد الگوی سرویس دهنده</th>
                <th>وضعیت</th>
                <th>تاریخ ایجاد</th>
                <th>عملیات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sms_panel as $sms)
                <tr>
                    <td>{{$sms->name}}</td>
                    <td>{{$sms->url_client}}</td>
                    <td>{{$sms->user_name}}</td>
                    <td>{{$sms->user_password}}</td>
                    <td>{{$sms->panel_number}}</td>
                    <td>{{$sms->pattern_code}}</td>
                    <td>
                        @if($sms->status)
                            <div class="badge badge-success rounded">فعال</div>
                            @else
                            <div class="badge badge-danger rounded">غیر فعال</div>
                        @endif
                    </td>
                    <td>{{\Hekmatinasser\Verta\Verta::instance($sms->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}</td>
                    <td>

                        <a href="{{route('sms-panel.edit',$sms->id)}}" class="btn btn-warning text-white d-inline ml-2">
                            <span>ویرایش</span>
                        </a>
                        <form action="{{route('sms-panel.destroy',$sms->id)}}" method="POST" class="d-inline">
                            @csrf
                            {{method_field('DELETE')}}
                            <button type="submit" class="btn btn-danger">
                                <span>حذف</span>
                            </button>
                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    </div>
    <div class="row">
        <div class="m-auto">
                <span class="align-items-center">
                    {{$sms_panel->links()}}
                </span>
        </div>
    </div>
</div>
@endsection
