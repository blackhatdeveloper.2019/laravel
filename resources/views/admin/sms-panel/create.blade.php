@extends('admin.layouts.master')

@section('title')
    ایجاد  پنل اس پیام کوتاه
@endsection

@section('content')

    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/sms-panel')}}">پنل پیام کوتاه</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('sms-panel.create')}}">ایجاد پنل پیام کوتاه</a></span>
            </div>
        </div>
        <div class="h4 text-center m-4">
            <span class="text-center"> ایجاد اطلاعات پنل پیام کوتاه </span>
        </div>
        <div class="col-8 m-auto ">

            @include('admin.partials.form-errors')


            <form action="{{route('sms-panel.store')}}" method="POST">
                @csrf
                <div class="form-group d-block">
                    <label for="name">نام  پنل  <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text"  class="form-control form-control-lg" id="name" name="name" placeholder="لطفا نام پنل خود را وارد کنید...">
                </div>

                <div class="form-group d-block">
                    <label for="url_client">آدرس سرور <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text"  class="form-control form-control-lg" id="url_client" name="url_client" placeholder="لطفا آدرس کلاینت سرور را وارد کنید...">
                </div>

                <div class="form-group d-block">
                    <label for="user_name">نام کاربری پنل  <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text"  class="form-control form-control-lg" id="user_name" name="user_name" placeholder="لطفا نام کاربری پنل خود را وارد کنید...">
                </div>

                <div class="form-group d-block">
                    <label for="user_password">کلمه عبور پنل <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="password"  class="form-control form-control-lg" id="user_password" name="user_password" placeholder="لطفا کلمه عبور پنل خود را وارد کنید...">
                </div>

                <div class="form-group d-block">
                    <label for="panel_number">شماره تلفن پنل <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text"  class="form-control form-control-lg" id="panel_number" name="panel_number" placeholder="شماره تلفن پنل خود را وارد کنید...">
                </div>

                <div class="form-group d-block">
                    <label for="pattern_code">شماره الگوی پنل <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text"  class="form-control form-control-lg" id="pattern_code" name="pattern_code" placeholder="شماره الگوی پنل خود را وارد کنید...">
                </div>


                <div class="form-group">
                    <label for="status">وضعیت</label>
                    <select name="status" id="status" class="form-control">
                        <option value="0" selected>غیر فعال</option>
                        <option value="1" > فعال</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary btn-block">ثبت </button>
            </form>
        </div>
    </div>
@endsection
