@extends('admin.layouts.master')

@section('title')
   ارتباط با ما
@endsection

@section('content')

    <div class="container-fluid">

        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/contacts')}}">ارتباط با ما</a></span>
            </div>
        </div>
        <br>
        <div class="text-center font-size-22">
            <h3>ارتباط با ما</h3>
        </div>
        @include('admin.partials.index-success')
        <div class="row">
                    <table class="table table-active table-striped table-hover shadow text-center">
                        <thead class="thead-light">
                        <tr>
                            <th>شناسه</th>
                            <th>نام نام خانوادگی</th>
                            <th>ایمیل</th>
                            <th>شماره تماس</th>
                            <th>موضوع</th>
                            <th>شناسه سفارش</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contacts as $contact)
                            <tr>
                                <td><a href="{{route('contacts.show',$contact->id)}}">{{$contact->id}}</a></td>
                                <td>{{$contact->name}}</td>
                                <td>{{$contact->email}}</td>
                                <td>{{$contact->mobile}}</td>
                                <td>{{$contact->subject}}</td>
                                <td>{{$contact->order_code}}</td>
                                <td class="custom-control-inline">
                                    <a class="btn btn-warning text-white ml-2" href="{{route('contacts.edit', $contact->id)}}">ویرایش</a>
                                    <div class="display-inline-block">
                                        <form method="post" action="/administrator/contacts/{{$contact->id}}">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-danger">حذف</button>
                                        </form>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            <div class="m-auto">
                <span class="align-items-center">{{$contacts->links()}}</span>
            </div>
    </div>



@endsection
