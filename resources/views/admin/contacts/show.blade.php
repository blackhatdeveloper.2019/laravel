@extends('admin.layouts.master')

@section('title')
    ارتباط با ما
@endsection

@section('content')

    <div class="container-fluid">

        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/contacts')}}">ارتباط با ما</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/contacts',$contact->id)}}">{{$contact->name}}</a></span>
            </div>
        </div>
        <br>
        <div class="text-center font-size-22 py-2 my-2">
            <h3>ارتباط با ما</h3>
        </div>

        <div class="row">
            <table class="table table-active table-striped table-hover shadow">
                <thead class="thead-light">
                <tr>
                    <th>عنوان</th>
                    <th>متن</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>شناسه</th>
                    <th>{{$contact->id}}</th>
                </tr>
                <tr>
                    <th>نام و نام خانوادگی</th>
                    <th>{{$contact->name}}</th>
                </tr>
                <tr>
                    <th>ایمیل</th>
                    <th>{{$contact->email}}</th>
                </tr>
                <tr>
                    <th>شماره تماس</th>
                    <th>{{$contact->mobile}}</th>
                </tr>
                <tr>
                    <th>موضوع</th>
                    <th>{{$contact->subject}}</th>
                </tr>
                <tr>
                    <td>شناسه سفارش</td>
                    <td>{{$contact->order_code}}</td>
                </tr>
                <tr>
                    <td>تاریخ ایجاد</td>
                    <td>{{\Hekmatinasser\Verta\Verta::instance($contact->created_at)}}</td>
                </tr>
                <tr>
                    <td>متن پیام</td>
                    <td>{{$contact->description}}</td>
                </tr>
                <tr>
                    <td>عملیات</td>
                    <td class="custom-control-inline">
                            <a class="btn btn-warning text-white ml-2" href="{{route('contacts.edit', $contact->id)}}">ویرایش</a>
                            <div class="display-inline-block">
                                <form method="post" action="/administrator/contacts/{{$contact->id}}">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                </form>
                            </div>

                        </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>



@endsection

