@extends('admin.layouts.master')

@section('title')
    ارتباط با ما
@endsection

@section('content')

    <div class="container-fluid">

        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/contacts')}}">ارتباط با ما</a></span>
            </div>
        </div>
        <br>
        <div class="text-center font-size-22">
            <h3>ارتباط با ما</h3>
        </div>
        @include('admin.partials.index-success')
        <div class="row">
            <div class="col-12 col-md-8 m-auto">
                <form action="{{route('contacts.update',$contact->id)}}" method="POST">
                    @csrf
                    {{method_field('PATCH')}}
                    <label for="name" class="col-form-label">نام و نام خانوادگی <span class="text-danger">*</span></label>
                    <div class="form-group">
                        <input type="text" value="{{$contact->name}}" name="name" class="form-control form-control-lg" id="name" placeholder="نام و نام خانوادگی خود را وارد کنید">
                    </div>
                    <label for="email" class="col-form-label">ایمیل <span class="text-danger">*</span></label>
                    <div class="form-group">
                        <input type="email" value="{{$contact->email}}" name="email" class="form-control form-control-lg" id="email" placeholder="ایمیل خود را وارد کنید">
                    </div>
                    <label for="mobile" class="col-form-label">تلفن تماس <span class="text-danger">*</span></label>
                    <div class="form-group">
                        <input type="tel" name="mobile" value="{{$contact->mobile}}" class="form-control form-control-lg" id="mobile" placeholder="تلفن تماس خود را وارد کنید">
                    </div>
                    <label for="subject" class="col-form-label">موضوع <span class="text-danger">*</span></label>
                    <div class="form-group">
                        <select name="subject" class="form-control form-control-lg" id="subject">
                            <option value="پیشنهاد">پیشنهاد</option>
                            <option value="انتقاد و شکایت" >انتقاد و شکایت</option>
                            <option value="پیگیری سفارش"  selected>پیگیری سفارش</option>
                            <option value="خدمات پس از فروش" >خدمات پس از فروش</option>
                            <option value="حسابداری و امور مالی" >حسابداری و امور مالی</option>
                            <option value="همکاری در فروش" >همکاری در فروش</option>
                            <option value="مدیریت" >مدیریت</option>
                            <option value="سایر موضوعات" >سایر موضوعات</option>
                        </select>
                    </div>
                    <label for="order" class="col-form-label">شناسه سفارش </label>
                    <div class="form-group">
                        <input type="text" value="{{$contact->order_code}}" name="order" class="form-control form-control-lg" id="order" placeholder="شناسه سفارش خود را وارد کنید">
                    </div>
                    <label for="description" class="col-form-label">متن پیام <span class="text-danger">*</span></label>
                    <div class="form-group">
                        <textarea name="description" class="form-control form-control-lg" id="description" placeholder="متن پیام خود را اینجا وارد کنید...">{{$contact->description}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-info btn-lg btn-block">ثبت اطلاعات</button>
                </form>
            </div>
        </div>
    </div>



@endsection
