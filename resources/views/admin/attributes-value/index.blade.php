@extends('admin.layouts.master')

@section('title')
    مقادیر ویژگی ها
@endsection

@section('content')
<div class="container-fluid">
    <div class="alert alert-secondary d-block p-1 m-0 shadow">
        <div class="pr-1">
            <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">محصولات</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{url('/administrator/attributes-value')}}">مقدار ویژگی ها</a></span>
        </div>
    </div>
    <br>
            <div class="border-bottom pb-3">
                <h3 class="text-center">مقادیر ویژگی</h3>
                <div class="text-left mb-2">
                    <a class="btn btn-info btn-lg" href="{{route('attributes-value.create')}}">
                        <i class="fa fa-plus"></i> جدید
                    </a>
                </div>
            </div>
            @include('admin.partials.index-success')
                    <table class="table table-active table-striped table-hover shadow">
                        <thead>
                        <tr>
                            <th>شناسه</th>
                            <th>عنوان</th>
                            <th>ویژگی</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($attributesValue as $attribute)
                            <tr>
                                <td>{{$attribute->id}}</td>
                                <td>{{$attribute->title}}</td>
                                <td>{{$attribute->attributeGroup->title}}</td>
                                <td class="custom-control-inline">
                                    <a class="btn btn-warning text-white mx-2" href="{{route('attributes-value.edit', $attribute->id)}}">ویرایش</a>
                                    <div class="display-inline-block">
                                        <form method="post" action="/administrator/attributes-value/{{$attribute->id}}">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-danger">حذف</button>
                                        </form>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

            <div class="row">
                 <div class="col-md-4 col-4 m-auto">
                     <span class="text-center m-auto">{{$attributesValue->links()}}</span>
                 </div>
            </div>
</div>
@endsection
