@extends('admin.layouts.master')

@section('title')
    ایجاد مقدار ویژگی جدید
@endsection

@section('content')
<div class="container-fluid">
    <div class="alert alert-secondary d-block p-1 m-0 shadow">
        <div class="pr-1">
            <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">محصولات</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{url('/administrator/attributes-value')}}">مقدار ویژگی ها</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{route('attributes-value.create')}}">ایجاد ویژگی جدید</a></span>
        </div>
    </div>
    <br>
            <div class="border-bottom pb-3">
                <h3 class="text-center">ایجاد مقدار ویژگی جدید</h3>
            </div>

                <div class="row">
                    <div class="col-md-8 m-auto pt-2">

                        @include('admin.partials.form-errors')
                        <br>

                        <form method="post" action="/administrator/attributes-value">
                            @csrf
                            <div class="form-group">
                                <label for="attribute_group_id">انتخاب ویژگی<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <select name="attributeGroup_id" id="attribute_group_id" class="form-control">
                                    <option value="">انتخاب کنید</option>
                                    @foreach($attributesGroup as $attribute)
                                        <option value="{{$attribute->id}}">{{$attribute->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title">عنوان<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input type="text" id="title" name="title" class="form-control" placeholder="مقدار ویژگی را وارد کنید...">
                            </div>
                            <button type="submit" class="btn btn-success btn-block">ذخیره</button>
                        </form>
                    </div>
                </div>
</div>
@endsection
