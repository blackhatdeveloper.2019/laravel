@extends('admin.layouts.master')

@section('title')
    ویرایش مقدار ویژگی ها
@endsection

@section('content')
<div class="container-fluid">
    <div class="alert alert-secondary d-block p-1 m-0 shadow">
        <div class="pr-1">
            <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">محصولات</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{url('/administrator/attributes-value')}}">مقدار ویژگی ها</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{route('attributes-value.edit',$attributeValue->id)}}">ویرایش ویژگی جدید</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{route('attributes-value.edit',$attributeValue->id)}}">{{$attributeValue->title}}</a></span>
        </div>
    </div>
    <br>
            <div class="border-bottom pb-3">
                <h3 class="text-center">ویرایش مقدار ویژگی {{$attributeValue->title}}</h3>
            </div>

                <div class="row">
                    <div class="col-md-8 m-auto pt-2">
                        @include('admin.partials.form-errors')
                        <br>
                        <form method="post" action="/administrator/attributes-value/{{$attributeValue->id}}">
                            @csrf
                            <input type="hidden" name="_method" value="PATCH">
                            <div class="form-group">
                                <label for="name">عنوان<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input type="text" name="title" class="form-control" value="{{$attributeValue->title}}" placeholder="عنوان مقدار ویژگی را وارد کنید...">
                            </div>
                            <div class="form-group">
                                <label for="attribute_group">نوع ویژگی<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <select name="attributeGroup_id" id="" class="form-control">
                                    @foreach($attributesGroup as $attribute)
                                        <option value="{{$attribute->id}}" @if($attribute->id == $attributeValue->attributeGroup->id) selected @endif>{{$attribute->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">بروز رسانی</button>
                        </form>
                    </div>
                </div>
</div>
@endsection
