@extends('admin.layouts.master')

@section('title')
    ایجاد فروشنده
@endsection

@section('content')

    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/vendors')}}">فروشندگان</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('vendors.create')}}"> ایجاد فروشنده جدید</a></span>
            </div>
        </div>
        <div class="h4 text-center m-4">
            <span class="text-center">ایجاد فروشنده جدید </span>
        </div>
        <div class="col-9 m-auto ">
            @include('admin.partials.form-errors')

            <form action="{{route('vendors.store')}}" method="POST">
                @csrf

                <div class="form-group d-block">
                    <label for="name">نام <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" class="form-control form-control-lg" id="name" name="name" placeholder="نام را وارد کنید...">
                </div>

                <div class="form-group d-block">
                    <label for="family"> نام خانوادگی<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" class="form-control form-control-lg" id="family" name="family" placeholder="نام خانوادگی وارد کنید...">
                </div>
                <div class="form-group d-block">
                    <label for="national_code">کد ملی<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="number" class="form-control form-control-lg" id="national_code" name="national_code" placeholder="کد ملی وارد کنید...">
                </div>
                <div class="form-group d-block">
                    <label for="cart_number">شماره کارت<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="number" class="form-control form-control-lg" id="cart_number" name="cart_number" placeholder="شماره کارت وارد کنید...">
                </div>

                <div class="form-group d-block">
                    <label for="status">وضعیت فروشنده</label>
                    <select name="status" class="form-control form-control-lg" id="status">
                        <option value="0" selected>غیرفعال</option>
                        <option value="1">فعال</option>
                    </select>
                </div>

                <div class="form-group d-block">
                    <label for="user_id">نام کاربری</label>
                    <select name="user_id" class="form-control form-control-lg" id="user_id">
                        @foreach($users as $use)
                        <option value="{{$use->id}}">{{$use->mobile}}</option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-info btn-block">ذخیره اطلاعات</button>
            </form>
        </div>
    </div>
@endsection
