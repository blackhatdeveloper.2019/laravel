@extends('admin.layouts.master')

@section('title')
    ویرایش اطلاعات فروشنده
@endsection


@section('content')

    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/vendors')}}">فروشندگان</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('vendors.edit',$vendor->id)}}">ویرایش اطلاعات فروشنده</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('vendors.edit',$vendor->id)}}">{{$vendor->family}}</a></span>
            </div>
        </div>
        <div class="h4 text-center mb-4">
            <span class="text-center">ایجاد فروشنده جدید </span>
        </div>
        <div class="col-9 m-auto ">
            @include('admin.partials.form-errors')

            <form action="{{route('vendors.update',$vendor->id)}}" method="POST">
                @csrf
                {{method_field('PATCH')}}
                <div class="form-group d-block">
                    <label for="name">نام <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" value="{{$vendor->name}}" class="form-control form-control-lg" id="name" name="name" placeholder="نام را وارد کنید...">
                </div>

                <div class="form-group d-block">
                    <label for="family"> نام خانوادگی<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" value="{{$vendor->family}}" class="form-control form-control-lg" id="family" name="family" placeholder="نام خانوادگی وارد کنید...">
                </div>
                <div class="form-group d-block">
                    <label for="national_code">کد ملی<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="number" value="{{$vendor->national_code}}" class="form-control form-control-lg" id="national_code" name="national_code" placeholder="کد ملی وارد کنید...">
                </div>
                <div class="form-group d-block">
                    <label for="cart_number">شماره کارت<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="number" value="{{$vendor->cart_number}}" class="form-control form-control-lg" id="cart_number" name="cart_number" placeholder="شماره کارت وارد کنید...">
                </div>

                <div class="form-group d-block">
                    <label for="status">وضعیت فروشنده</label>
                    <select name="status" class="form-control form-control-lg" id="status">
                        <option value="0" @if($vendor->status==0) selected @endif>غیرفعال</option>
                        <option value="1" @if($vendor->status==1) selected @endif>فعال</option>
                    </select>
                </div>

                <div class="form-group d-block">
                    <label for="user_id">نام کاربری</label>
                    <select name="user_id" class="form-control form-control-lg" id="user_id">
                        @foreach($users as $use)
                            <option value="{{$use->id}}" @if($use->id==$vendor->user_id) selected @endif>{{$use->mobile}}</option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-info btn-block">ذخیره اطلاعات</button>
            </form>
        </div>
    </div>
@endsection

