@extends('admin.layouts.master')

@section('title')
    پروفایل
@endsection

@section('style')

    <link rel="stylesheet" href="{{asset('backend/css/dropzone.min.css')}}">

@endsection

@section('content')


    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/profiles')}}">پروفایل</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('profiles.edit',$users->id)}}">ویرایش پروفایل</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('profiles.edit',$users->id)}}">{{$users->name}}</a></span>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="h4 text-center align-items-center">
                    <span class="text-center">ویرایش پروفایل {{$users->name}}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 m-auto pt-2">

                @include('admin.partials.form-errors')
                <table class="table table-hover table-bordered table-striped shadow">
                <form  action="{{route('profiles.update',$users->id)}}" method="POST">
                    @csrf
                    {{method_field('PATCH')}}
                    <thead class="thead-light">
                    <tr>
                        <th>عنوان</th>
                        <th>مشخصات</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>

                        <td>
                            <label for="name">نام<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                        </td>
                        <td>
                            <input type="text" value="{{$users->name}}" name="name" id="name" class="form-control form-control-lg" placeholder="نام خود را وارد کنید">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="mobile">شماره تماس<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                        </td>
                        <td>
                            <input type="tel" id="mobile" name="mobile" value="{{$users->mobile}}" class="form-control form-control-lg" placeholder="شماره موبایل خود را وارد کنید">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="password">کلمه عبور</label>
                        </td>
                        <td>
                            <input type="password" id="password" name="password"  class="form-control form-control-lg" placeholder="جهت تغییر کلمه عبور جدید کلمه عبور را وارد کنید..." autocomplete="new-password">
                        </td>
                    </tr>
                    <tr>
                        <td>وضعیت</td>
                        <td>
                            @if($users->status==1)
                                <span class="badge badge-success font-size-16">فعال</span>
                            @elseif($users->status==0)
                                <span class="badge badge-danger font-size-16">غیر فعال</span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>تصویر پروفایل<span class="text-danger font-size-22 font-weight-bold">*</span></td>
                        <td>
                            @if(!empty($user->path))
                                <img src="{{$user->path}}" alt="" width="100" height="100">
                            @else
                                <img src="{{url('backend/photos/avatars.png')}}" alt="" width="100" height="100">
                            @endif
                                <div class="form-group custom-control-inline">
                                    <input type="hidden" name="photo_id" id="photo_id" value="{{$users->photo_id}}">
                                    <div id="photo" class="dropzone">
                                    </div>
                                </div>
                        </td>
                    </tr>
                    <tr>
                        <td>تاریخ ایجاد</td>
                        <td>{{\Hekmatinasser\Verta\Verta::instance($users->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}</td>
                    </tr>
                    <tr>
                        <td>عملیات</td>
                        <td>
                            <button type="submit" class="btn btn-lg btn-primary text-white btn-block">
                                <span>بروزرسانی</span>
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </form>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('backend/js/dropzone.min.js')}}"></script>

    <script>
        Dropzone.autoDiscover=false;
        var drop = new Dropzone('#photo', {
            addRemoveLinks: true,
            maxFiles: 1,
            url: "{{ route('photos.upload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                document.getElementById('photo_id').value = response.photo_id
            }
        });

    </script>
@endsection

