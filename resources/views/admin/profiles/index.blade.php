@extends('admin.layouts.master')

@section('title')
    پروفایل
@endsection

@section('content')


    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/profiles')}}">پروفایل</a></span>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="h4 text-center align-items-center">
                    <span class="text-center">پروفایل</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 m-auto pt-2">

                @include('admin.partials.index-success')
                <table class="table table-hover table-bordered table-striped shadow">
                    <thead class="thead-light">
                    <tr>
                        <th>عنوان</th>
                        <th>مشخصات</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>نام</td>
                            <td>{{$users->name}}</td>
                        </tr>
                        <tr>
                            <td>شماره تماس</td>
                            <td>{{$users->mobile}}</td>
                        </tr>
                        <tr>
                            <td>وضعیت</td>
                            <td>
                                @if($users->status==1)
                                    <span class="badge badge-success">فعال</span>
                                @elseif($users->status==0)
                                    <span class="badge badge-danger">غیر فعال</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>تصویر پروفایل</td>
                            <td>
                                @if(!empty($user->path))
                                    <img src="{{$user->path}}" alt="" width="100" height="100">
                                @else
                                    <img src="{{url('backend/photos/avatars.png')}}" alt="" width="100" height="100">
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>تاریخ ایجاد</td>
                            <td>{{\Hekmatinasser\Verta\Verta::instance($users->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}</td>
                        </tr>
                        <tr>
                            <td>عملیات</td>
                            <td>
                                <a href="{{route('profiles.edit',$users->id)}}" class="btn btn-warning text-white ml-2 btn-block">
                                    <span>ویرایش</span>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
