<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{url('/images/icons_fjO_icon.ico')}}">
    <!-- yield title add -->
    <title>@yield('title')</title>

    <!-- link bootstrap css style -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet">
    <!-- link Yield style -->
    @yield('style')

    <link rel="stylesheet" href="{{asset('frontend/css/fonts.css')}}">

    <!-- link backend css style -->
    <link rel="stylesheet" href="{{asset('backend/css/backend.css')}}">


</head>
<body class="bg-light">
<!-----------------------Navbar Start-------------------------->
<nav class="navbar sticky-top navbar-expand-md navbar-light bg-white">
    <div class="d-flex align-items-center">
        <a href="{{url('/')}}" class="navbar-brand display-2">پنل مدیریت <span class="text-info font-weight-bold h4">آلاکالا</span></a>
        <i  class="fas fa-angle-double-right h4 text-secondary mr-3 cursor-pointer"  data-toggle="collapse" data-target="#sidebar"></i>
    </div>

    <button class="navbar-toggler" data-toggle="collapse" data-target="#main-menu">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse mr-3 d-md-flex justify-content-md-between" id="main-menu">
        <nav class="navbar-nav">
            <ul class="list-unstyled custom-control-inline">
            <li class="nav-item"><a href="{{url('administrator/dashboard')}}" class="nav-link">
                    <i class="fas fa-tachometer-alt text-success"></i>
                    داشبورد
                </a></li>
            <li class="nav-item"><a href="#" class="nav-link">
                    <i class="fas fa-users text-info"></i>
                    مشتری ها
                </a></li>
            <li class="nav-item"><a href="#" class="nav-link">
                    <i class="fas fa-cog text-danger"></i>
                    تنظیمات
                </a>
            </li>
            </ul>
        </nav>
        <nav class="navbar-nav align-items-center d-none d-md-flex">
           <!--
            <li class="nav-item dropdown"><a href="#" class="nav-link" data-toggle="dropdown">
                    <span class="badge badge-pill badge-danger">5</span>
                    <i class="far fa-bell"></i>
                    <div class="dropdown-menu text-right">
                        <div class="dropdown-header text-center bg-light">شما 5 اعلان دارید</div>
                        <a href="#" class="dropdown-item text-secondary">یک سفارش جدید ایجاد شد</a>
                        <a href="#" class="dropdown-item text-secondary">یک کاربر جدید وارد شد</a>
                    </div>
                </a>
            </li>
            <li class="nav-item dropdown"><a href="#" class="nav-link" data-toggle="dropdown">
                    <span class="badge badge-pill badge-warning">8</span>
                    <i class="fas fa-tasks"></i>
                    <div class="dropdown-menu text-right">
                        <div class="dropdown-header text-center bg-light">شما 8 دستور در حال اجرا دارید</div>
                        <a href="#" class="dropdown-item text-secondary">یک سفارش جدید ایجاد شد</a>
                        <a href="#" class="dropdown-item text-secondary">یک کاربر جدید وارد شد</a>
                    </div>
                </a>
            </li>
            <li class="nav-item dropdown"><a href="#" class="nav-link" data-toggle="dropdown">
                    <span class="badge badge-pill badge-info">12</span>
                    <i class="far fa-comment"></i>
                    <div class="dropdown-menu text-right">
                        <div class="dropdown-header text-center bg-light">شما 12 پیغام جدید دارید</div>
                        <a href="#" class="dropdown-item text-secondary">یک سفارش جدید ایجاد شد</a>
                        <a href="#" class="dropdown-item text-secondary">یک کاربر جدید وارد شد</a>
                    </div>
                </a>
            </li>
            -->
            <li class="nav-item dropdown"><a href="#" class="nav-link" data-toggle="dropdown">
                    @if(!empty($user->path))
                        <img src="{{$user->path}}" class="rounded-circle" width="35" alt="">
                    @else
                        <img src="{{asset('images/profile_icon.png')}}" width="35" alt="">
                    @endif
                    <div class="dropdown-menu text-right">
                        <a href="{{route('profiles.index')}}" class="dropdown-item text-secondary"><span><i class="fas fa-user-alt pl-2"></i></span>پروفایل</a>
                        <a href="{{route('logout')}}" class="dropdown-item text-secondary"><span><i class="fas fa-sign-out-alt pl-2"></i></span> خروج</a>
                    </div>
                </a>
            </li>
        </nav>
    </div>
</nav>
<!-----------------------Navbar END-------------------------->

    <!------------------------- Container Start ---------------------->
    <div class="container-fluid">
        <div class="row">
            <!------------------------------- SideBar Start -------------------------------------->
            <div id="sidebar" class="col-5 col-sm-4 col-md-2 bg-dark collapse show h-100 position-fixed">
                <ul class="nav d-flex flex-column pr-0">
                    <li class="nav-item my-1">
                        <a href="#submenu1" class="nav-link collapsed" data-toggle="collapse" >
                            <i class="fas ml-2 fa-boxes"></i>
                            محصولات
                            <i class="fas fa-chevron-left float-left"></i>
                        </a>
                        <div class="collapse bg-secondary rounded" id="submenu1">
                            <ul class="nav pr-0  flex-column">
                                <li class="nav-item"><a href="{{route('products.index')}}" class="nav-link">محصولات</a></li>
                                <li class="nav-item"><a href="{{route('categories.index')}}" class="nav-link">دسته بندی ها</a></li>
                                <li class="nav-item"><a href="{{route('attributes-group.index')}}" class="nav-link">ویژگی ها</a></li>
                                <li class="nav-item"><a href="{{route('attributes-value.index')}}" class="nav-link">مقدار ویژگی ها</a></li>
                                <li class="nav-item"><a href="{{route('brands.index')}}" class="nav-link">برند ها </a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item my-1">
                        <a href="#submenu10" class="nav-link collapsed" data-toggle="collapse">
                            <i class="fas ml-2 fa-store"></i>
                            فروشگاهها
                            <i class="fas fa-chevron-left float-left"></i>
                        </a>
                        <div class="collapse bg-secondary rounded" id="submenu10">
                            <ul class="nav pr-0 flex-column">
                                <li class="nav-item"><a href="{{url('/administrator/shops')}}" class="nav-link">فروشگاهها</a></li>
                                <li class="nav-item"><a href="{{url('/administrator/vendors')}}" class="nav-link">فروشنده ها</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item my-1">
                        <a href="#submenu2" class="nav-link collapsed" data-toggle="collapse">
                            <i class="fas ml-2 fa-users"></i>
                            کاربر
                            <i class="fas fa-chevron-left float-left"></i>
                        </a>
                        <div class="collapse bg-secondary rounded" id="submenu2">
                            <ul class="nav pr-0 flex-column">
                                <li class="nav-item"><a href="{{route('users.index')}}" class="nav-link">کاربرها</a></li>
                                <li class="nav-item"><a href="{{route('roles-user.index')}}" class="nav-link">نقش کاربر</a></li>
                                <li class="nav-item"><a href="{{route('roles.index')}}" class="nav-link">نقش ها</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item my-1">
                        <a href="#submenu7" class="nav-link collapsed" data-toggle="collapse">
                            <i class="fas ml-2 fa-money-bill-alt"></i>
                            سفارشات
                            <i class="fas fa-chevron-left float-left"></i>
                        </a>
                        <div class="collapse bg-secondary rounded" id="submenu7">
                            <ul class="nav pr-0 flex-column">
                                <li class="nav-item"><a href="{{route('order.index')}}" class="nav-link"> سفارشات</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item my-1">
                        <a href="#submenu9" class="nav-link collapsed" data-toggle="collapse">
                            <i class="fas ml-2 fa-cash-register"></i>
                            بانک
                            <i class="fas fa-chevron-left float-left"></i>
                        </a>
                        <div class="collapse bg-secondary rounded" id="submenu9">
                            <ul class="nav pr-0">
                                <li class="nav-item"><a href="{{route('banks.index')}}" class="nav-link">لیست بانک ها</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item my-1">
                        <a href="#submenu8" class="nav-link collapsed" data-toggle="collapse">
                            <i class="fas ml-2 fa-shipping-fast"></i>
                                ارسالی ها
                            <i class="fas fa-chevron-left float-left"></i>
                        </a>
                        <div class="collapse bg-secondary rounded" id="submenu8">
                            <ul class="nav pr-0">
                                <li class="nav-item"><a href="#" class="nav-link"> پست های ارسالی </a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item my-1">
                        <a href="#submenu13" class="nav-link collapsed" data-toggle="collapse">
                            <i class="fas ml-2 fa-map-marked-alt"></i>
                            ناحیه ها
                            <i class="fas fa-chevron-left float-left"></i>
                        </a>
                        <div class="collapse bg-secondary rounded" id="submenu13">
                            <ul class="nav pr-0">
                                <li class="nav-item"><a href="{{route('province.index')}}" class="nav-link">استان ها</a></li>
                                <li class="nav-item"><a href="{{route('city.index')}}" class="nav-link">شهرهستان ها</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item my-1">
                        <a href="#submenu11" class="nav-link collapsed" data-toggle="collapse">
                            <i class="fas ml-2 fa-comment-dots"></i>
                             پنل پیام کوتاه
                            <i class="fas fa-chevron-left float-left"></i>
                        </a>
                        <div class="collapse bg-secondary rounded" id="submenu11">
                            <ul class="nav pr-0">
                                <li class="nav-item"><a href="{{route('sms-panel.index')}}" class="nav-link"> تنظیمات پنل</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item my-1">
                        <a href="{{route('coupons.index')}}" class="nav-link" >
                            <i class="fas ml-2 fa-key"></i>
                            کدهای تخفیف
                        </a>
                    </li>
                    <li class="nav-item my-1">
                        <a href="{{route('news.index')}}" class="nav-link" >
                            <i class="fas ml-2 fa-newspaper"></i>
                            خبرنامه
                        </a>
                    </li>
                    <li class="nav-item my-1">
                        <a href="{{route('posts.index')}}" class="nav-link" >
                            <i class="fas ml-2 fa-comment-alt"></i>
                            نظرات کاربران
                        </a>
                    </li>
                    <li class="nav-item my-1">
                        <a href="#" class="nav-link" >
                            <i class="fas ml-2 fa-cogs"></i>
                            تنظیمات
                        </a>
                    </li>
                </ul>
            </div>
            <!------------------------------- SideBar End --------------------------------------------->

            <!-------------------------------- Main Content Start ----------------------------------->
            <div id="main-content" class="col-12 col-md-10 p-4 mr-auto">

                <!-- yield Content add -->
                @yield('content')

            </div>
            <!-------------------------------- Main Content End --------------------------->

        </div>
    </div>
    <!------------------------- Container End ------------------------>


         <!-- src jquery js -->
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>

        <!-- src popper js -->
        <script src="{{asset('backend/js/popper.min.js')}}"></script>

        <!-- src bootstrap js -->
        <script src="{{asset('js/app.js')}}"></script>


        <!-- src Chart js -->
        <script src="{{asset('frontend/js/Chart.min.js')}}"></script>

        <!-- src Sample js -->
        <script src="{{asset('frontend/js/sample.js')}}"></script>

        <!--- Yield Js script --->
        @yield('script')

</body>
</html>


