@if($errors->any())

    <div class="alert alert-warning alert-dismissible fade show border-danger" role="alert">
        <span><i class="fa fa-exclamation-triangle text-danger"></i></span>
        @foreach($errors->all() as $error)
           <ul>
               <li><strong>{{$error}}</strong></li>
           </ul>

        @endforeach

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

@endif
