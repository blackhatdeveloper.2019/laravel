@foreach($categories as $sub_category)
    <tr>
        <td>{{$sub_category->id}}</td>
        <td>{{str_repeat('--', $level)}} {{$sub_category->name}}</td>
        <td class="custom-control-inline">
            <a class="btn btn-warning text-white mx-1" href="{{route('categories.edit', $sub_category->id)}}">ویرایش</a>
                <form method="post" action="/administrator/categories/{{$sub_category->id}}">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger mx-1">حذف</button>
                </form>
            <a class="btn btn-primary mx-1" href="{{route('categories.indexSetting', $sub_category->id)}}">تنظیمات</a>
        </td>
    </tr>
    @if(count($sub_category->childrenRecursive) > 0)
        @include('admin.partials.category_list', ['categories' => $sub_category->childrenRecursive, 'level' => $level+1])
    @endif
@endforeach
