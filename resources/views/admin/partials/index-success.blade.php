@if(Session::has('delete_method'))
    <div class="alert alert-warning alert-dismissible fade show border-warning" role="alert">
        <strong>{{session('delete_method')}}</strong><br>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade show border-success" role="alert">
        <strong>{{session('success')}}</strong><br>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
