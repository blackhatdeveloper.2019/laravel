@extends('admin.layouts.master')

@section('title')
    ایجاد کد تخفیف
@endsection

@section('content')
    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/coupons')}}">کد های تخفیف</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/coupons/create')}}">ایجاد کد تخفیف</a></span>
            </div>
        </div>
        <br>
                <div class="row">
                    <div class="col-md-8 m-auto">
                        @include('admin.partials.form-errors')
                        <form id="myForm" method="post" action="/administrator/coupons">
                            @csrf
                            <div class="form-group">
                                <label for="title">عنوان کد تخفیف</label>
                                <input type="text" id="title" name="title" class="form-control" placeholder="عنوان کد تخفیف را وارد کنید...">
                            </div>

                            <div class="form-group">
                                <label for="code">کد</label>
                                <input type="text" id="code" name="code" class="form-control" placeholder=" کد را وارد کنید...">
                            </div>

                            <div class="form-group">
                                <label for="price">قیمت</label>
                                <input type="number" id="price" name="price" class="form-control" placeholder=" قیمت را وارد کنید...">
                            </div>

                            <div class="form-group">
                                <label for="status">وضعیت</label>
                                <div class="form-control form-control-lg">
                                    <input id="status" type="radio" name="status" value="0" checked> <span class="margin-l-10">منتشر نشده</span>
                                    <input id="status" type="radio" name="status" value="1"> <span>منتشر شده</span>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success btn-block">ذخیره</button>
                        </form>
                    </div>
                </div>
            </div>

@endsection
