@extends('admin.layouts.master')

@section('title')
   کدهای تخفیف
@endsection

@section('content')

    <div class="container-fluid">

        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/coupons')}}">کد تخفیف ها</a></span>
            </div>
        </div>
        <br>

        <div class="h4 text-center mb-4">
            <span class="text-center">کد تخفیف ها</span>
            <span class="float-left">
                <a href="{{route('coupons.create')}}" class="btn btn-lg btn-info text-white">
                    <i class="fas fa-plus fa-lg pl-2"></i>
                    <span>جدید</span>
                </a>
            </span>
        </div>

        @include('admin.partials.index-success')
        <div class="row">
                    <table class="table table-active table-striped table-hover shadow text-center">
                        <thead class="thead-light">
                        <tr>
                            <th>شناسه</th>
                            <th>عنوان</th>
                            <th>کد</th>
                            <th>قیمت</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($coupons as $coupon)
                            <tr>
                                <td>{{$coupon->id}}</td>
                                <td>{{$coupon->title}}</td>
                                <td>{{$coupon->code}}</td>
                                <td >{{$coupon->price}}</td>
                                @if($coupon->status == 0)
                                    <td><span class="badge badge-danger rounded">غیر فعال</span></td>
                                @else
                                    <td><span class="badge badge-success rounded">فعال</span></td>
                                @endif
                                <td class="custom-control-inline">
                                    <a class="btn btn-warning text-white ml-2" href="{{route('coupons.edit', $coupon->id)}}">ویرایش</a>
                                    <div class="display-inline-block">
                                        <form method="post" action="/administrator/coupons/{{$coupon->id}}">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-danger">حذف</button>
                                        </form>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            <div class="m-auto">
                <span class="align-items-center">{{$coupons->links()}}</span>
            </div>
    </div>



@endsection
