@extends('admin.layouts.master')

@section('title')
    ویرایش کد تخفیف
@endsection

@section('content')
    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/coupons')}}">کد های تخفیف</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/coupons/edit',$coupon->id)}}">ویرایش کد تخفیف</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/coupons/edit',$coupon->id)}}">{{$coupon->title}}</a></span>
            </div>
        </div>
        <br>
                <div class="row">
                    <div class="col-md-8 m-auto">
                        <form method="post" action="/administrator/coupons/{{$coupon->id}}">
                            @csrf
                            <input type="hidden" name="_method" value="PATCH">

                            <div class="form-group">
                                <label for="title">نام</label>
                                <input type="text" id="title" name="title" class="form-control" value="{{$coupon->title}}" placeholder="عنوان کد تخفیف را وارد کنید...">
                            </div>

                            <div class="form-group">
                                <label for="code">توضیحات</label>
                                <input type="text" id="code" name="code" class="form-control" value="{{$coupon->code}}" placeholder="کد را وارد کنید...">
                            </div>

                            <div class="form-group">
                                <label for="price">قیمت</label>
                                <input type="number" id="price" name="price" class="form-control" value="{{$coupon->price}}" placeholder=" قیمت را وارد کنید...">
                            </div>

                            <div class="form-group">
                                <label for="status">وضعیت</label>
                                <div class="form-control form-control-lg">
                                    <input type="radio" id="status" name="status" value="0" @if($coupon->status == 0) checked @endif> <span class="margin-l-10">منتشر نشده</span>
                                    <input type="radio" id="status" name="status" value="1" @if($coupon->status == 1) checked @endif> <span>منتشر شده</span>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-info btn-block">بروزرسانی</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
