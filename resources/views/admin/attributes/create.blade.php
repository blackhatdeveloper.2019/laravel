@extends('admin.layouts.master')

@section('title')
    ایجاد ویژگی های جدید
@endsection

@section('content')
    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">محصولات</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/attributes-group')}}">ویژگی ها</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('attributes-group.create')}}">ایجاد ویژگی جدید</a></span>
            </div>
        </div>
        <br>
            <div class="border-bottom pb-3">
                <h3 class="text-center">ایجاد ویژگی جدید</h3>
            </div>
          <div class="row">
                    <div class="col-md-8 m-auto pt-2">
                        @include('admin.partials.form-errors')
                        <br>
                        <form method="post" action="/administrator/attributes-group">
                            @csrf
                            <div class="form-group">
                                <label for="title">عنوان<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input type="text" name="title" id="title" class="form-control" placeholder="عنوان گروه بندی ویژگی را وارد کنید...">
                            </div>
                            <div class="form-group">
                                <label for="meta_title">نوع<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <select name="type" id="meta_title" class="form-control">
                                    <option value="select">لیست تکی</option>
                                    <option value="multiple">لیست چندتایی</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">توضیحات</label>
                                <textarea class="form-control" name="description" id="description" placeholder="درباره عنوان ویژگی بنویسید..."></textarea>
                            </div>
                            <button type="submit" class="btn btn-info btn-block">ذخیره</button>
                        </form>
                    </div>
                </div>
    </div>

@endsection
