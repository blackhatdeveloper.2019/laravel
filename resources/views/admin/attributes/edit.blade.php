@extends('admin.layouts.master')

@section('title')
ویرایش ویژگی ها
@endsection

@section('content')

<div class="container-fluid">
    <div class="alert alert-secondary d-block p-1 m-0 shadow">
        <div class="pr-1">
            <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">محصولات</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{url('/administrator/attributes-group')}}">ویژگی ها</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{route('attributes-group.edit',$attributeGroup->id)}}">ویرایش ویژگی </a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{route('attributes-group.edit',$attributeGroup->id)}}">{{$attributeGroup->title}} </a></span>
        </div>
    </div>
    <br>
            <div class="border-bottom pb-3">
                <h3 class="text-center">ویرایش ویژگی {{$attributeGroup->title}}</h3>
            </div>

                <div class="row">
                    <div class="col-md-8 m-auto py-2">
                        @include('admin.partials.form-errors')
                        <br>
                        <form method="post" action="/administrator/attributes-group/{{$attributeGroup->id}}">
                            @csrf
                            <input type="hidden" name="_method" value="PATCH">
                            <div class="form-group">
                                <label for="title">عنوان<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input type="text" name="title" id="title" class="form-control" value="{{$attributeGroup->title}}" placeholder="عنوان گروه بندی ویژگی را وارد کنید...">
                            </div>
                            <div class="form-group">
                                <label for="meta_title">نوع<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <select name="type" id="meta_title" class="form-control">
                                    <option value="select" @if($attributeGroup->type == 'select') selected @endif>لیست تکی</option>
                                    <option value="multiple" @if($attributeGroup->type == 'multiple') selected @endif>لیست چندتایی</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">توضیحات</label>
                                <textarea class="form-control" name="description" id="description" placeholder="درباره عنوان ویژگی بنویسید...">{{$attributeGroup->description}}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">بروز رسانی</button>
                        </form>
                    </div>
                </div>
</div>
@endsection
