@extends('admin.layouts.master')

@section('title')
    ویرایش نقش
@endsection

@section('content')

    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/users')}}">کاربران</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/roles')}}">نقش ها</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('roles.edit',$role->id)}}">ویرایش نقش </a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('roles.edit',$role->id)}}">{{$role->name}}</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center"> ویرایش نقش {{$role->name}} </span>
        </div>
        <div class="col-8 m-auto ">

            @include('admin.partials.form-errors')


            <form action="{{route('roles.update',$role->id)}}" method="POST">
                @csrf
                {{method_field('PATCH')}}
                <div class="form-group d-block">
                    <label for="name">نام نقش <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" value="{{$role->name}}" class="form-control form-control-lg" id="name" name="name" placeholder="نام نقش را وارد کنید...">
                </div>
                <button type="submit" class="btn btn-info btn-block">ذخیره اطلاعات</button>
            </form>
        </div>
    </div>
@endsection
