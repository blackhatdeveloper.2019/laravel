@extends('admin.layouts.master')

@section('title')
ایجاد محصول جدید
@endsection

@section('style')

    <link rel="stylesheet" href="{{asset('backend/css/dropzone.min.css')}}">

@endsection

@section('content')

    <div class="container-fluid" id="app">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">محصولات</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('products.create')}}">ایجاد محصول</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center">ایجاد محصول جدید </span>
        </div>
        <div class="col-9 m-auto ">
            @include('admin.partials.form-errors')

            <form id="myForm" method="post" action="/administrator/products">
               @csrf
               <div class="form-group">
                                <label for="title">نام<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input type="text" id="title" name="title" class="form-control" placeholder="نام محصول را وارد کنید...">
                            </div>
               <div class="form-group">
                                <label for="slug">نام مستعار<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input type="text" id="slug" name="slug" class="form-control" placeholder="نام مستعار محصول را وارد کنید...">
                            </div>
                <div class="form-group">
                    <label for="qty">تعداد محصول<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="number" id="qty" name="qty" class="form-control form-control" placeholder="لطفا تعداد محصولات را وارد کنید...">
                </div>
               <attribute-component :brands="{{ $brands }}"></attribute-component>
               <div class="form-group">
                                <label for="radio">وضعیت نشر<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <div>
                                    <input id="radio" type="radio"  name="status" value="0" checked><span class="margin-l-10">منتشر نشده</span>
                                    <input id="radio" type="radio"  name="status" value="1"> <span>منتشر شده</span>
                                </div>
                            </div>
               <div class="form-group">
                                <label for="price">قیمت<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input id="price" type="number" name="price" class="form-control" placeholder="قیمت محصول را وارد کنید...">
                            </div>
               <div class="form-group">
                                <label for="price_ef">قیمت ویژه<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input id="price_ef" type="number" name="discount_price" class="form-control" placeholder="قیمت ویژه محصول را وارد کنید...">
                            </div>
               <div class="form-group">
                                <label for="textareaDescription">توضیحات</label>
                                <textarea id="textareaDescription" type="text" name="description" class=" form-control" placeholder="توضیحات محصول را وارد کنید..."></textarea>
                            </div>
               <div class="form-group">
                                <label for="photo">گالری تصاویر<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input type="hidden" name="photo_id[]" id="product-photo">
                                <div id="photo" class="dropzone"></div>
                            </div>
               <div class="form-group">
                                <label for="meta_title">عنوان سئو<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input id="meta_title" type="text" name="meta_title" class="form-control" placeholder="عنوان سئو را وارد کنید...">
                            </div>
               <div class="form-group">
                                <label for="meta_desc"> توضیحات سئو<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <textarea type="text" id="meta_desc" name="meta_desc" class="form-control" placeholder="توضیحات سئو را وارد کنید..."></textarea>
                            </div>
               <div class="form-group">
                  <label for="meta_keywords">کلمات کلیدی سئو<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                  <input type="text" id="meta_keywords" name="meta_keywords" class="form-control" placeholder="کلمات کلیدی سئو را وارد کنید...">
                </div>
               <button type="submit" onclick="productGallery()" class="btn btn-info btn-block">ذخیره</button>
            </form>

        </div>
    </div>

@endsection


@section('script')
    <script type="text/javascript" src="{{asset('backend/plugins/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/dropzone.min.js')}}"></script>

    <script>
        Dropzone.autoDiscover = false;
        var photosGallery = []
        var drop = new Dropzone('#photo', {
          addRemoveLinks: true,
          url: "{{ route('photos.upload') }}",
          sending: function(file, xhr, formData){
            formData.append("_token","{{csrf_token()}}")
          },
          success: function(file, response){
            photosGallery.push(response.photo_id)
          }
        });
        productGallery = function(){
          document.getElementById('product-photo').value = photosGallery
        }

        CKEDITOR.replace('textareaDescription',{
          customConfig: 'config.js',
          toolbar: 'simple',
          language: 'fa',
          removePlugins: 'cloudservices, easyimage'
        })

    </script>

@endsection
