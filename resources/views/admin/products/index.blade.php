@extends('admin.layouts.master')

@section('title')
محصولات
@endsection

@section('content')


    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">محصولات</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center">محصولات</span>
            <span class="float-left">
                <a href="{{url('administrator/products/create')}}" class="btn btn-lg btn-info text-white">
                    <i class="fas fa-plus fa-lg pl-2"></i>
                    <span>جدید</span>
                </a>
            </span>
        </div>

        @include('admin.partials.index-success')
        <div class="row">

                <table class="table table-active table-striped table-hover shadow">
                    <thead>
                    <tr>
                        <th>شناسه</th>
                        <th>کد محصول</th>
                        <th>عنوان</th>
                        <th>تعداد</th>
                        <th>عکس محصول</th>
                        <th>وضعیت</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td>{{$product->sku}}</td>
                            <td>{{$product->title}}</td>
                            <td>{{$product->qty}}</td>
                            <td>
                                @foreach($product->photos as $key=>$photo)
                                    @if($key==0)
                                        <img src="{{$photo->path}}" width="100" height="100" alt="">
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                @if($product->status==1)
                                    <div class="badge badge-success rounded">منتشر شده</div>
                                    @else
                                    <div class="badge badge-danger rounded">منتشر نشده</div>
                                @endif
                            </td>
                            <td class="custom-control-inline">
                                <a class="btn btn-warning text-white" href="{{route('products.edit', $product->id)}}">ویرایش</a>
                                <div class="display-inline-block">
                                    <form method="post" action="/administrator/products/{{$product->id}}">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-danger mr-2 pr-2">حذف</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            <div class="m-auto">
                <span class="align-items-center">{{$products->links()}}</span>
            </div>
        </div>
    </div>

@endsection
