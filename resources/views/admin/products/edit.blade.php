@extends('admin.layouts.master')

@section('style')

    <link rel="stylesheet" href="{{asset('backend/css/dropzone.min.css')}}">

@endsection

@section('content')
    <section class="content" id="app">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">محصولات</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('products.edit',$product->id)}}">ویرایش محصول</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('products.edit',$product->id)}}">{{$product->title}}</a></span>
            </div>
        </div>
        <br>
            <div class="box-header with-border">
                <h3 class="text-center">ویرایش محصول {{$product->title}}</h3>
            </div>
                <div class="row">
                    <div class="col-md-10 m-auto">
                        <form method="post" action="/administrator/products/{{$product->id}}">
                            @csrf
                            {{method_field('PATCH')}}

                            <div class="form-group">
                                <label for="title">نام<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input type="text" name="title" class="form-control" value="{{$product->title}}" placeholder="نام محصول را وارد کنید...">
                            </div>
                            <div class="form-group">
                                <label for="slug">نام مستعار<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input type="text" name="slug" class="form-control" value="{{$product->slug}}"  placeholder="نام مستعار محصول را وارد کنید...">
                            </div>
                            <div class="form-group">
                                <label for="qty">تعداد محصول<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input type="number" value="{{$product->qty}}" id="qty" name="qty" class="form-control"  placeholder="تعداد محصول را وارد کنید...">
                            </div>
                            <attribute-component :brands="{{ $brands }}" :product="{{$product}}"></attribute-component>
                            <div class="form-group">
                                <label>وضعیت نشر<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <div>
                                    <input type="radio" name="status" value="0" @if($product->status == 0) checked @endif> <span class="margin-l-10">منتشر نشده</span>
                                    <input type="radio" name="status" value="1" @if($product->status == 1) checked @endif> <span>منتشر شده</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>قیمت<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input type="number" name="price"  value="{{$product->price}}" class="form-control" placeholder="قیمت محصول را وارد کنید...">
                            </div>
                            <div class="form-group">
                                <label>قیمت ویژه<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input type="number" name="discount_price" value="{{$product->discount_price}}" class="form-control" placeholder="قیمت ویژه محصول را وارد کنید...">
                            </div>
                            <div class="form-group">
                                <label>توضیحات</label>
                                <textarea id="textareaDescription" type="text" name="description" class="ckeditor form-control" placeholder="توضیحات محصول را وارد کنید...">{{$product->description}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="photo">گالری تصاویر</label>
                                <input type="hidden" name="photo_id[]" id="product-photo">
                                <div id="photo" class="dropzone"></div>
                                <div class="row shadow-lg">
                                    @foreach($product->photos as $photo)
                                        <div class="col-sm-3" id="updated_photo_{{$photo->id}}">
                                            <img class="img-responsive" width="120" height="120" src="{{$photo->path}}">
                                            <button type="button" class="btn btn-danger" onclick="removeImages({{$photo->id}})">حذف</button>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label>عنوان سئو<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input type="text" name="meta_title"  value="{{$product->meta_title}}" class="form-control" placeholder="عنوان سئو را وارد کنید...">
                            </div>
                            <div class="form-group">
                                <label>توضیحات سئو<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <textarea type="text" name="meta_desc" class="form-control" value="{{$product->meta_desc}}" placeholder="توضیحات سئو را وارد کنید...">{{$product->meta_desc}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>کلمات کلیدی سئو<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                                <input type="text" name="meta_keywords" value="{{$product->meta_keywords}}" class="form-control" placeholder="کلمات کلیدی سئو را وارد کنید...">
                            </div>
                            <button type="submit" onclick="productGallery()" class="btn btn-primary btn-lg btn-block">بروزرسانی</button>
                        </form>
                    </div>
                </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('backend/plugins/ckeditor/ckeditor.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/dropzone.min.js')}}"></script>
    <script>
      Dropzone.autoDiscover = false;
      var photosGallery = []
      var photos = [].concat({{$product->photos->pluck('id')}})
      var drop = new Dropzone('#photo', {
        addRemoveLinks: true,
        url: "{{ route('photos.upload') }}",
        sending: function(file, xhr, formData){
          formData.append("_token","{{csrf_token()}}")
        },
        success: function(file, response){
          photosGallery.push(response.photo_id)
        }
      });
      productGallery = function(){
        document.getElementById('product-photo').value = photosGallery.concat(photos)
      }

      CKEDITOR.replace('textareaDescription',{
        customConfig: 'config.js',
        toolbar: 'simple',
        language: 'fa',
        removePlugins: 'cloudservices, easyimage'
      })
      removeImages = function(id){
        var index = photos.indexOf(id)
        photos.splice(index, 1);
        document.getElementById('updated_photo_' + id).remove();
      }
    </script>

@endsection
