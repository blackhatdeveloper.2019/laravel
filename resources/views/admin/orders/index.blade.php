@extends('admin.layouts.master')

@section('title')
    سفارشات
@endsection

@section('content')
    <div class="container-fluid">

        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/orders')}}">سفارشات</a></span>
            </div>
        </div>
        <br>

        <div class="h4 text-center mb-4">
            <span class="text-center">سفارشات</span>
        </div>

        @include('admin.partials.index-success')
            <div class="row">
                    <table class="table table-active table-striped table-hover shadow text-center">
                        <thead class="thead-light">
                        <tr>
                            <th>شناسه</th>
                            <th>مقدار</th>
                            <th>وضعیت</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td><a href="{{route('orders.lists', ['id'=>$order->id])}}">{{$order->id}}</a></td>
                                <td >{{$order->amount}}</td>
                                @if($order->status == 0)
                                    <td><span class="badge badge-danger">پرداخت نشده</span></td>
                                @else
                                    <td><span class="badge badge-success">پرداخت شده</span></td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <!-- /.table-responsive -->
                <div class="m-auto">
                    <span class="align-items-center">{{$orders->links()}}</span>
                </div>
            </div>
@endsection
