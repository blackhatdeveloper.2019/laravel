@extends('admin.layouts.master')

@section('title')
    سفارشات
@endsection

@section('content')
    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
                        <div class="pr-1">
                            <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                            <span><a class="span-link text-muted" href="{{url('/administrator/orders')}}">سفارشات</a></span>
                        </div>
                    </div>
        <br>
        <div class="row">
            <table class="table table-active table-striped table-hover shadow">
                <thead class="thead-light">
                        <tr>
                            <th>تصویر محصول</th>
                            <th>نام محصول</th>
                            <th>کد محصول</th>
                            <th>تعداد</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($order->products as $product)
                            <tr>
                                <td><img src="{{$product->photos[0]->path}}" class="img-fluid" width="120" height="110"> </td>
                                <td>{{$product->title}}</td>
                                <td>{{$product->sku}}</td>
                                <td>{{$product->pivot->qty}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card customer-data p-2">
                    <p class="mt-5">
                        <strongt>نام خریدار: </strongt> {{$order->user->name}}
                    </p>
                    <p>
                        <strongt>آدرس خریدار: </strongt> {{$order->user->addresses[0]->province->name . ' ' . $order->user->addresses[0]->city->name . ' ' . $order->user->addresses[0]->address}}
                        <strongt>کد پستی خریدار: </strongt> {{$order->user->addresses[0]->post_code}}
                    </p>
                    <p>
                        <strongt>شماره موبایل خریدار: </strongt> {{$order->user->mobile}}
                    </p>
                </div>
                <!-- /.table-responsive -->
            </div>

@endsection
