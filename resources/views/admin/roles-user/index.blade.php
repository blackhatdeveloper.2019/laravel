@extends('admin.layouts.master')

@section('title')
    کاربر ها
@endsection

@section('content')


<div class="container-fluid">
    <div class="alert alert-secondary d-block p-1 m-0 shadow">
        <div class="pr-1">
            <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{url('/administrator/users')}}">کاربران</a></span><span class="font-size-25 text-muted">/</span>
            <span><a class="span-link text-muted" href="{{url('/administrator/roles-user')}}">نقش کاربران</a></span>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="h4 text-center align-items-center">
                <span class="text-center">نقش کاربران</span>
                <span class="float-left">
                    <a href="{{route('roles-user.create')}}" class="btn btn-lg btn-info text-white">
                        <i class="fas fa-plus fa-lg pl-2"></i>
                        <span>جدید</span>
                    </a>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 m-auto pt-2">

        @include('admin.partials.index-success')
        <table class="table table-hover table-bordered table-striped shadow">
            <thead class="thead-light">
            <tr>
                <th>نام کاربر</th>
                <th>ایمیل</th>
                <th>نقش ها</th>
                <th>وضعیت نقش مدیریتی</th>
                <th>تاریخ ایجاد</th>
                <th>عملیات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users_role as $users)
                <tr>
                    <td>{{$users->name}}</td>
                    <td>{{$users->mobile}}</td>
                    <td>
                        <ul class="list-group-horizontal">
                        @foreach($users->roles as $role)
                                <li>{{$role->name}}</li>
                        @endforeach
                        </ul>
                    </td>
                    <td>
                            @if($users->status)
                                <span class="badge badge-success">فعال</span>
                                @else
                                <span class="badge badge-danger">غیر فعال</span>
                            @endif
                    </td>
                    <td>{{\Hekmatinasser\Verta\Verta::instance($users->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}</td>
                    <td>

                        <a href="{{route('roles-user.edit',$users->id)}}" class="btn btn-warning text-white d-inline ml-2">
                            <span>ویرایش</span>
                        </a>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    </div>
    <div class="row">
        <div class="m-auto">
                <span class="align-items-center">
                    {{$users_role->links()}}
                </span>
        </div>
    </div>
</div>
@endsection
