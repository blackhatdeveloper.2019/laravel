@extends('admin.layouts.master')

@section('title')
   ایجاد نقش کاربران
@endsection

@section('content')

    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/users')}}">کاربران</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/roles-user')}}">نقش کاربران</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('roles-user.create')}}">ایجاد نقش کاربر</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center">ایجاد نقش جدید کاربران </span>
        </div>
        <div class="col-8 m-auto ">

            @include('admin.partials.form-errors')


            <form action="{{route('roles-user.store')}}" method="POST">
                @csrf
                <div class="form-group d-block">
                    <label for="user"> نام کاربر <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" class="form-control form-control-lg" id="user" name="user" placeholder="نام کاربری را وارد کنید...">
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                </div>

                <div class="form-group d-block">
                    <label for="role"> نام نقش <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <select name="role_id" id="role" class="custom-select" multiple>
                        <option value="">انتخاب کنید</option>
                        @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-info btn-block">ذخیره اطلاعات</button>
            </form>
        </div>
    </div>
@endsection
