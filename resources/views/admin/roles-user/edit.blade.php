@extends('admin.layouts.master')

@section('title')
    ایجاد نقش کاربری
@endsection

@section('content')

    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/users')}}">کاربران</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/roles-user')}}">نقش کاربران</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('roles-user.edit',$users->name)}}">ویرایش نقش کاربر</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('roles-user.edit',$users->name)}}">{{$users->name}}</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center">ایجاد نقش کاربر {{$users->name}} </span>
        </div>
        <div class="col-8 m-auto ">

            @include('admin.partials.form-errors')


            <form action="{{route('roles-user.update',$users->id)}}" method="POST">
                @csrf
                {{method_field('PATCH')}}
                <div class="form-group d-block">
                    <label for="user"> نام کاربر  <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" value="{{$users->name}}" class="form-control form-control-lg" id="user" name="user" >
                    <input type="hidden" name="user_id" value="{{$users->id}}">
                </div>

                <div class="form-group d-block">
                    <label for="role_id"> نام نقش <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <select name="role_id[]" id="role_id" class="custom-select" multiple>
                        @foreach($roles as $role)
                            <option @foreach($users->roles as $user_role) @if($user_role->name==$role->name) selected @endif @endforeach value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="status">وضعیت نقش مدیریتی</label>
                    <select name="status" id="status" class="form-control form-control-lg">
                        <option value="1" @if($users->status==1) selected @endif>فعال</option>
                        <option value="0" @if($users->status==0) selected @endif>غیر فعال</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-info btn-block">ذخیره اطلاعات</button>
            </form>
        </div>
    </div>
@endsection
