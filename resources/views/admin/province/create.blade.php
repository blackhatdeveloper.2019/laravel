@extends('admin.layouts.master')

@section('title')
    اضافه کردن استان جدید
@endsection

@section('content')
    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/province')}}">ناحیه</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/province')}}">استان ها</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/province/create')}}">ضافه کردن استان جدید</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center">ایجاد استان جدید </span>
        </div>
                <div class="row">

                    <div class="col-md-8 m-auto">
                        @include('admin.partials.form-errors')
                        <form id="myForm" method="post" action="/administrator/province">
                            @csrf
                            <div class="form-group">
                                <label for="name">نام استان <span class="text-danger">*</span></label>
                                <input type="text" id="name" name="name" class="form-control" placeholder="نام استان را وارد کنید...">
                            </div>

                            <button type="submit" class="btn btn-info btn-block">ذخیره</button>
                        </form>
                    </div>
                </div>
            </div>

@endsection
