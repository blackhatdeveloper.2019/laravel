@extends('admin.layouts.master')

@section('title')
    ویرایش درگاه بانک
@endsection

@section('style')

    <link rel="stylesheet" href="{{asset('backend/css/dropzone.min.css')}}">

@endsection

@section('content')

    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/banks')}}"> درگاه بانک</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('banks.edit',$bank->id)}}">ویرایش درگاه بانک </a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('banks.edit',$bank->id)}}">{{$bank->bank_name}}</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center">ویرایش  درگاه بانک {{$bank->bank_name}} </span>
            <h1 class="text-center"><img src="{{$bank->photo->path}}" width="140" height="120" alt=""></h1>
        </div>
        <div class="col-9 m-auto ">

            @include('admin.partials.form-errors')


            <form action="{{route('banks.update',$bank->id)}}" method="POST">
                @csrf
                {{method_field('PATCH')}}

                <div class="form-group d-block">
                    <label for="bank_name">نام درگاه بانک <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" class="form-control form-control-lg" value="{{$bank->bank_name}}" id="bank_name" name="bank_name" placeholder="نام بانک را وارد کنید...">
                </div>

                <div class="form-group d-block">
                    <label for="bank_url">آدرس درگاه بانک <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" class="form-control form-control-lg" value="{{$bank->bank_url}}" id="bank_url" name="bank_url" placeholder="آدرس اتصال بانک را وارد کنید...">
                </div>

                <div class="form-group d-block">
                    <label for="bank_code">کد درگاه بانک</label>
                    <input class="form-control form-control-lg" value="{{$bank->bank_code}}" id="bank_code" name="bank_code" placeholder="کد بانک وارد کنید...">
                </div>

                <div class="form-group d-block">
                    <label for="status">وضعیت درگاه بانک<span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <select type="text" class="form-control form-control-lg" id="status" name="status" >
                        <option value="0" @if($bank->status==0) selected @endif>غیر فعال</option>
                        <option value="1" @if($bank->status==1) selected @endif>فعال</option>
                    </select>
                </div>

                <div class="form-group">
                    <input type="hidden" name="photo_id" id="photo_id" value="{{$bank->photo_id}}">
                    <div id="photo" class="dropzone"></div>
                </div>

                <button type="submit" class="btn btn-success btn-block">ذخیره </button>
            </form>
        </div>
    </div>
@endsection

@section('script')

    <script type="text/javascript" src="{{asset('backend/js/dropzone.min.js')}}"></script>

    <script>
        Dropzone.autoDiscover=false;
        var photosGallery = []
        var drop = new Dropzone('#photo',{
            addRemoveLinks:true,
            maxFiles :1,
            url:"{{route('photos.upload')}}",
            sending:function(file,xhr,formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success:function(file,response){
                document.getElementById('photo_id').value = response.photo_id

            }
        });


    </script>
@endsection
