@extends('admin.layouts.master')

@section('title')
    درگاه بانک
@endsection

@section('content')


    <div class="container-fluid">

        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/products')}}">بانک</a></span>
            </div>
        </div>
        <br>

        <div class="h4 text-center mb-4">
            <span class="text-center">درگاه بانک ها</span>
            <span class="float-left">
                <a href="{{route('banks.create')}}" class="btn btn-lg btn-info text-white">
                    <i class="fas fa-plus fa-lg pl-2"></i>
                    <span>جدید</span>
                </a>
            </span>
        </div>

        @include('admin.partials.index-success')
        <div class="row">
               <table class="table table-active table-striped table-hover shadow text-center">
                <thead>
                <tr>
                    <th>نام درگاه بانک</th>
                    <th>آدرس درگاه بانک</th>
                    <th>کد درگاه بانک</th>
                    <th>عکس درگاه بانک</th>
                    <th>وضعیت درگاه بانک</th>
                    <th>تاریخ ایجاد</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($banks as $bank)
                    <tr>
                        <td>{{$bank->bank_name}}</td>
                        <td>{{$bank->bank_url}}</td>
                        <td>{{$bank->bank_code}}</td>
                        <td>
                            <img src="{{$bank->photo->path}}" width="50" height="50" alt="">
                        </td>
                        <td>
                            @if($bank->status==1)
                                <div class="badge badge-success rounded">فعال</div>
                                @else
                                <div class="badge badge-danger rounded">غیر فعال</div>
                            @endif
                        </td>
                        <td>{{\Hekmatinasser\Verta\Verta::instance($bank->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}</td>
                        <td class="custom-control-inline">
                            <a href="{{route('banks.edit',$bank->id)}}" class="btn btn-warning text-white d-inline ml-2">
                                <span>ویرایش</span>
                            </a>
                            <form action="{{route('banks.destroy',$bank->id)}}" method="POST" class="d-inline">
                                @csrf
                                {{method_field('DELETE')}}
                                <button type="submit" class="btn btn-danger">
                                    <span>حذف</span>
                                </button>
                            </form>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
           </div>
               <div class="col-md-4 col-4 m-auto">
                    <span class="align-items-center">{{$banks->links()}}</span>
                </div>
        </div>


@endsection

