@extends('admin.layouts.master')

@section('title')
    ویرایش اطلاعات فروشگاه
@endsection

@section('style')

    <link rel="stylesheet" href="{{asset('backend/css/dropzone.min.css')}}">

@endsection

@section('content')

    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/shops')}}">فروشگاه ها</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{route('shops.edit',$shop->id)}}">ویرایش فروشگاه</a></span>
                <span><a class="span-link text-muted" href="{{route('shops.edit',$shop->id)}}">{{$shop->shop_name}}</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center">ویرایش فروشگاه {{$shop->shop_name}} </span><br><br>
        </div>
        <div class="col-9 m-auto ">

            @include('admin.partials.form-errors')


            <form action="{{route('shops.update',$shop->id)}}" method="POST">
                @csrf
                {{method_field('PATCH')}}
                <div class="form-group d-block">
                    <label for="shop_name">نام فروشگاه <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" value="{{$shop->shop_name}}" class="form-control form-control-lg" id="shop_name" name="shop_name" placeholder="نام فروشگاه را وارد کنید...">
                </div>
                <div class="form-group d-block">
                    <label for="address">نام فروشگاه <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="text" value="{{$shop->address}}" class="form-control form-control-lg" id="address" name="address" placeholder="آدرس فروشگاه را وارد کنید...">
                </div>
                <div class="form-group d-block">
                    <label for="phone">شماره تلفن فروشگاه <span class="text-danger font-size-22 font-weight-bold">*</span></label>
                    <input type="tel" value="{{$shop->phone}}" class="form-control form-control-lg" id="phone" name="phone" placeholder="شماره تلفن فروشگاه را وارد کنید...">
                </div>
                <div class="form-group">
                    <input type="hidden" name="photo_id[]" id="shop-photo">
                    <div id="photo" class="dropzone"></div>
                       <div class="row custom-control-inline">
                        @foreach($shop->photos as $photo)
                            <div class="col-md-3" id="updated_photo_{{$photo->id}}">
                                <img class="img-responsive" width="120" height="120" src="{{$photo->path}}">
                                <button type="button" class="btn btn-danger" onclick="removeImages({{$photo->id}})">حذف</button>
                            </div>
                        @endforeach
                       </div>
                </div>
                <button type="submit" onclick="shopGallery()" class="btn btn-info btn-block">ذخیره اطلاعات</button>
            </form>
        </div>
    </div>
@endsection

@section('script')

    <script type="text/javascript" src="{{asset('backend/js/dropzone.min.js')}}"></script>

    <script>
        Dropzone.autoDiscover = false;
        var photosGallery = []
        var photos = [].concat({{$shop->photos->pluck('id')}})
        var drop = new Dropzone('#photo', {
            addRemoveLinks: true,
            url: "{{ route('photos.upload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                photosGallery.push(response.photo_id)
            }
        });
        shopGallery = function(){
            document.getElementById('shop-photo').value = photosGallery.concat(photos)
        }


        removeImages = function(id){
            var index = photos.indexOf(id)
            photos.splice(index, 1);
            document.getElementById('updated_photo_' + id).remove();
        }
    </script>
@endsection
