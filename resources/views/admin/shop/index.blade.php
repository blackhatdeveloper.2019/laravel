@extends('admin.layouts.master')

@section('title')
    فروشگاهها
@endsection

@section('content')


    <div class="container-fluid">
        <div class="alert alert-secondary d-block p-1 m-0 shadow">
            <div class="pr-1">
                <span><a class="span-link text-muted" href="{{url('/administrator/dashboard')}}">داشبورد</a></span><span class="font-size-25 text-muted">/</span>
                <span><a class="span-link text-muted" href="{{url('/administrator/shops')}}">فروشگاه ها</a></span>
            </div>
        </div>
        <br>
        <div class="h4 text-center mb-4">
            <span class="text-center">فروشگاهها</span>
            <span class="float-left">
                <a href="{{route('shops.create')}}" class="btn btn-lg btn-info text-white">
                    <i class="fas fa-plus fa-lg pl-2"></i>
                    <span>جدید</span>
                </a>
            </span>
        </div>

        <div class="row">
            <div class="col-md-12">
            @include('admin.partials.index-success')
            <table class="table table-hover table-striped shadow text-center">
                <thead class="thead-light">
                <tr>
                    <th>نام فروشگاه</th>
                    <th>نام فروشنده</th>
                    <th>شماره تلفن</th>
                    <th>آدرس فروشگاه</th>
                    <th>عکس فروشگاه</th>
                    <th>تاریخ ایجاد</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>

                @foreach($shops as $shop)
                    <tr>
                        <td>{{$shop->shop_name}}</td>
                        <td>{{$shop->vendor->family}}</td>
                        <td>{{$shop->address}}</td>
                        <td>{{$shop->phone}}</td>
                        <td>
                            @foreach($shop->photos as $keys=>$photo)
                                @if($keys==0)
                                    <img src="{{$photo->path}}" width="140" height="120" alt="">
                                @endif
                                @endforeach
                        </td>
                        <td>{{\Hekmatinasser\Verta\Verta::instance($shop->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}</td>
                        <td>
                            <a href="{{route('shops.edit',$shop->id)}}" class="btn btn-warning text-white d-inline ml-2">
                                <span>ویرایش</span>
                            </a>
                            <form action="{{route('shops.destroy',$shop->id)}}" method="POST" class="d-inline">
                                @csrf
                                {{method_field('DELETE')}}
                                <button type="submit" class="btn btn-danger">
                                    <span>حذف</span>
                                </button>
                            </form>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="m-auto">
                <span class="align-items-center">{{$shops->links()}}</span>
            </div>
            </div>
        </div>
        </div>

@endsection

