@extends('layouts.app')

@section('title')
    آدرسهای من
@endsection

@include('layouts.navigation')

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div id="column-right" class="col-sm-8 col-md-4 col-12">
                <div class="card m-4 bg-light">
                    <h3 class="subtitle p-4 m-auto">حساب کاربری</h3>
                    <div class="list-group">
                        <ul class="list-item">
                            <li><a href="{{url('/profile')}}">اطلاعات حساب</a></li>
                            <li><a href="{{url('/addresses')}}">لیست آدرس ها</a></li>
                            <li><a href="#">لیست علاقه مندی</a></li>
                            <li><a href="{{route('profile.orders')}}">تاریخچه سفارشات</a></li>
                            <li><a href="#">تراکنش ها</a></li>
                            <li><a href="#">خبرنامه</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--Middle Part Start-->
            <div id="content" class="col-sm-4 col-md-8 col-12 w-100 my-4">
                    @include('admin.partials.index-success')
                    <div class="card  bg-light">
                        <h4 class="text-center my-2 py-2">آدرس ها</h4>
                        <span class="text-left m-3"><a href="{{url('/addresses/create')}}" class="btn btn-info btn-lg" >ایجاد آدرس جدید</a></span>
                        <div class="card-body">
                            <table class="table table-striped table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <td>آدرس</td>
                                    <td>استان</td>
                                    <td>شهرستان</td>
                                    <td>کد پستی</td>
                                    <td>حذف</td>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($addresses as $address)
                                <tr>
                                    <td>{{$address->address}}</td>
                                    <td>{{$address->province->name}}</td>
                                    <td>{{$address->city->name}}</td>
                                    <td>{{$address->post_code}}</td>
                                    <td>
                                        <form method="post" action="/addresses/{{$address->id}}">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-danger mr-2 pr-2">حذف</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>

        </div>
    </div>
    @include('layouts.footer')
@endsection
