@extends('layouts.app')

@section('title')
    آدرسهای من
@endsection

@include('layouts.navigation')

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div id="column-right" class="col-sm-8 col-md-4 col-12">
                <div class="card m-4 bg-light">
                    <h3 class="subtitle p-4 m-auto">حساب کاربری</h3>
                    <div class="list-group">
                        <ul class="list-item">
                            <li><a href="{{route('addresses.index')}}">لیست آدرس ها</a></li>
                            <li><a href="#">لیست علاقه مندی</a></li>
                            <li><a href="{{route('profile.orders')}}">تاریخچه سفارشات</a></li>
                            <li><a href="#">تراکنش ها</a></li>
                            <li><a href="#">خبرنامه</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--Middle Part Start-->
            <div id="content" class="col-sm-4 col-md-8 col-12 w-100 my-4">
                @include('admin.partials.form-errors')
                <div class="card  bg-light">
                    <h4 class="text-center my-2 py-2">آدرس ها</h4>

                    <div class="card-body">
                        <form action="{{route('addresses.store')}}" method="post" dir="rtl">
                            @csrf

                            <div class="form-group">
                                <label for="address">آدرس <span class="text-danger">*</span></label>
                                <textarea name="address" id="address" class="form-control form-control-lg" placeholder="آدرس خود را وارد کنید..."></textarea>
                            </div>

                            <div class="form-group">
                                <label for="company">نام شرکت </label>
                                <input type="text" name="company" id="company" class="form-control form-control-lg" placeholder="اسم شرکت خود را وارد کنید...">
                            </div>

                            <div class="form-group">
                                <label for="receptor_name">نام تحویل گیرنده <span class="text-danger">*</span></label>
                                <input type="text" name="receptor_name" id="receptor_name" class="form-control form-control-lg" placeholder="نام تحویل گیرنده را وارد کنید...">
                            </div>
                            <div class="form-group">
                                <label for="receptor_mobile">شماره تماس تحویل گیرنده <span class="text-danger">*</span></label>
                                <input type="text" name="receptor_mobile" id="receptor_mobile" class="form-control form-control-lg" placeholder="شماره تماس تحویل گیرنده را وارد کنید...">
                            </div>
                            <div class="form-group">
                                <label for="post_code">شماره تماس تحویل گیرنده <span class="text-danger">*</span></label>
                                <input type="text" name="post_code" id="post_code" class="form-control form-control-lg" placeholder="کد پستی خود را وارد کنید...">
                            </div>
                            <div class="form-group">
                                <label for="province">استان <span class="text-danger">*</span></label>
                                <select name="province" id="province" class="form-control form-control-lg" >
                                    <option value="">انتخاب کنید</option>
                                    @foreach($province as $state)
                                        <option value="{{$state->id}}">{{$state->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="city">شهرستان <span class="text-danger">*</span></label>
                                <select name="city" id="city" class="form-control form-control-lg" >
                                    <option value="">انتخاب کنید</option>
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}">{{$city->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn bg-yellow btn-lg border-dark btn-block">ذخیره اطلاعات</button>

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @include('layouts.footer')
@endsection
