
@extends('layouts.app')

@section('title')
    سبد خرید
@endsection

@include('layouts.navigation')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-9 col-12">
                        @if(Session::has('error'))
                            <div class="alert alert-warning border-warning alert-dismissible fade show my-2" role="alert">
                                <div>{{session('error')}}</div>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        @if(Session::has('warning'))
                            <div class="alert alert-warning border-warning alert-dismissible fade show my-2" role="alert">
                                <div>{{session('warning')}}</div>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success border-success alert-dismissible fade show my-2" role="alert">
                                <div>{{session('success')}}</div>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="card bg-light border p-2 my-2">
                            @if(Session::has('cart'))
                                    <h1 class="title">سبد خرید</h1>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped">
                                            <thead class="thead-light">
                                            <tr>
                                                <td width="12%">تصویر</td>
                                                <td>نام محصول</td>
                                                <td>کد محصول</td>
                                                <td>تعداد</td>
                                                <td>قیمت واحد</td>
                                                <td>کل</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($cart->items as $product)
                                                <tr>
                                                    <td width="9%"><a href="#"><img src="{{$product['item']->photos[0]->path}}" class="img-thumbnail" /></a></td>
                                                    <td><a href="#">{{$product['item']->title}}</a><br />
                                                    <td>{{$product['item']->sku}}</td>
                                                    <td>
                                                        <div class="input-group btn-block quantity text-center">
                                                            <a class="btn btn-primary" data-toggle="tooltip" title="اضافه"  href="{{route('cart.add', ['id' => $product['item']->id])}}"><i class="fa fa-plus"></i></a>
                                                            <button type="button" data-toggle="tooltip" title="کم" class="btn btn-danger" onclick="event.preventDefault();
                                                                document.getElementById('remove-cart-item_{{$product['item']->id}}').submit();"><i class="fa fa-minus"></i></button>
                                                            <form id="remove-cart-item_{{$product['item']->id}}" action="{{ route('cart.remove', ['id' => $product['item']->id]) }}" method="post" style="display: none;">
                                                                @csrf
                                                            </form>
                                                        </div>
                                                        <p class="text-center" style="margin-top: 10px;">تعداد:‌ <span>{{$product['qty']}}</span></p>

                                                    </td>
                                                    <td class="text-right">{{$product['item']->discount_price ? $product['item']->discount_price : $product['item']->price }} تومان</td>
                                                    <td class="text-right">{{$product['price']}} تومان</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                    <form action="{{ route('coupon.add') }}" method="post" dir="ltr">
                                        @csrf
                                        <label for="code">کد تخفیف خود را در اینجا وارد کنید</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control border-dark" name="code" id="code" placeholder="کد تخفیف خود را در اینجا وارد کنید" class="form-control" />
                                            <span class="input-group-append">
                                                    <button type="submit" class="input-group-text btn bg-yellow border-dark" >اعمال کوپن</button>
                                            </span>
                                        </div>
                                    </form>
                            @else
                                <div class="display-1 text-center">
                                    <span><i class="fa fa-shopping-cart"></i></span>
                                    <h5 class="text-center py-3">
                                        سبد خرید شما خالی است
                                    </h5>
                                    <a href="{{url('/home')}}" class="btn bg-yellow border-dark w-25">صفحه اصلی</a>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3 col-12">
                        @if(!Auth::check())
                        <div class="card bg-light border p-4 my-2">
                            <h6 class="text-center py-2">
                                برای ثبت نام در سایت کلیک کنید
                            </h6>
                            <a href="{{url('/register')}}" class="btn bg-yellow btn-block border-dark">ثبت نام</a>
                            <h6 class="text-center py-2">
                                برای ورود به سایت کلیک کنید
                            </h6>
                            <a href="{{url('/login')}}" class="btn bg-yellow btn-block border-dark">ورود</a>
                        </div>
                            @else
                                <div class="cart bg-light border p-4 my-2 ">
                                    @if(Session::has('cart'))
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped">
                                            <tbody class="thead-light">
                                        <tr>
                                            <td class="text-right"><strong>جمع کل</strong></td>
                                            <td class="text-right">{{Session::get('cart')->totalPurePrice}} تومان</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right"><strong>تخفیف</strong></td>
                                            <td class="text-right">{{Session::get('cart')->totalDiscountPrice}} تومان</td>
                                        </tr>
                                        @if(Auth::check() && Session::get('cart')->coupon)
                                            <tr>
                                                <td class="text-right"><strong>{{Session::get('cart')->coupon['coupon']->title}}</strong></td>
                                                <td class="text-right">{{Session::get('cart')->couponDiscount}} تومان</td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td class="text-right"><strong>قابل پرداخت</strong></td>
                                            <td class="text-right">{{Session::get('cart')->totalPrice}} تومان</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <a href="{{route('order.verify')}}" class="btn bg-yellow border-dark btn-block">نهایی کردن سفارش</a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    </div>
                                                    @else
                                        <div class="table-responsive">
                                            <table class="table table-hover table-striped">
                                                <tbody class="thead-light">
                                                <tr>
                                                    <td class="text-right"><strong>جمع کل</strong></td>
                                                    <td class="text-right">0 تومان</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right"><strong>تخفیف</strong></td>
                                                    <td class="text-right">0 تومان</td>
                                                </tr>

                                                <tr>
                                                    <td class="text-right"><strong>قابل پرداخت</strong></td>
                                                    <td class="text-right">0 تومان</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <button class="btn bg-yellow border-dark btn-block">نهایی کردن سفارش</button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footer')

@endsection

