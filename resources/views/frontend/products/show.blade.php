
@extends('layouts.maser')

@section('title')
    محصولات
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('css/smoothproducts.css')}}">

@endsection

@section('script-top')
    <!-- src jquery js -->
    <script src="{{asset('backend/js/jquery.min.js')}}"></script>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>

    <script>
        $(document).ready(function()
        {


            if($('.bbb_viewed_slider').length)
            {
                var viewedSlider = $('.bbb_viewed_slider');

                viewedSlider.owlCarousel(
                    {
                        loop:false,
                        margin:30,
                        autoplay:false,
                        autoplayTimeout:6000,
                        nav:false,
                        dots:true,
                        responsive:
                            {
                                0:{items:1},
                                475:{items:2},
                                668:{items:3},
                                1200:{items:4},
                                1199:{items:6}
                            }
                    });

                if($('.bbb_viewed_prev').length)
                {
                    var prev = $('.bbb_viewed_prev');
                    prev.on('click', function()
                    {
                        viewedSlider.trigger('prev.owl.carousel');
                    });
                }

                if($('.bbb_viewed_next').length)
                {
                    var next = $('.bbb_viewed_next');
                    next.on('click', function()
                    {
                        viewedSlider.trigger('next.owl.carousel');
                    });
                }
            }


        });

        $(document).ready(function(){
            $('.dropdown-toggle').dropdown()
        });
    </script>
@endsection

@include('layouts.navigation')

@section('content')
    <div class="container-fluid">
        <div class="card bg-light text-muted m-2 p-2">
            <div class="custom-control-inline">
            <span><a href="{{url('/')}}" class="text-muted text-decoration-none">فروشگاه اینترنتی آلاکالا</a></span>
                @foreach($categories as $category)
                    <span class="px-1 font-size-18">/</span><span><a href="{{url('/categories/'.$category->id)}}" class="text-muted text-decoration-none">{{$category->name}}</a></span>
                @endforeach
            </div>
        </div>
        @include('admin.partials.form-errors')
        @include('admin.partials.index-success')
        <div class="card">
            <div class="row card-body">
            <div class="col-md-4 col-12">
                    <div class="row">
                        <div class="col-md-12 col-12 vw-100">
                                <div class="sp-loading">
                                    @foreach($product->photos as $key=>$photo)
                                    @if($key==0)
                                        <img src="{{$photo->path}}"  width="50;" height="50%;" alt="">
                                    @endif
                                    @endforeach
                                    <br>
                                </div>
                                <div class="sp-wrap">
                                    @foreach($product->photos as $photo)
                                    <a href="{{$photo->path}}"><img src="{{$photo->path}}" alt=""></a>
                                    @endforeach
                                </div>
                        </div>
                    </div>

            </div>
            <div class="col-md-4 col-12">
                <div class="card border-0 border-left-0">
                    <span class="m-1 p-1">{{$product->title}}</span><br>
                    <hr class="pt-0 mt-0">
                    <h6 class="m-1 p-1">نام برند :<span class="p-2">{{$product->brand->title}}</span></h6>
                    <h6>دسته بندی ها:
                    <span>
                        @foreach($product->categories as $key=>$category)
                            @if($key>0)
                                 <span class="p-1">{{$category->name}}</span> <span> , </span>
                            @else
                                <span class="p-1">{{$category->name}}</span>
                            @endif
                        @endforeach
                    </span>
                    </h6>
                    <span class="p-2 m-1">ویژگی های محصول</span>
                    <ul>
                        @foreach($product->attributeValues as $attribute)
                            <li><span>{{$attribute->attributeGroup->title}} :</span> {{$attribute->title}}</li>
                        @endforeach
                    </ul>

                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="card bg-light text-right p-2 ">
                    <h5><span><i class="fas fa-store text-muted p-2 font-size-30"></i></span> فروشنده :</h5>
                    <hr>
                    <h5><span><i class="fa fa-shield-alt text-muted p-2 font-size-30"></i></span>گارانتی :</h5>
                    <hr>
                    <h5><span><i class="fa fa-truck text-muted p-2 font-size-30"></i></span>آماده ارسال </h5>
                    <hr>
                    <span><span><i class="fas fa-money-bill-alt text-muted p-2 font-size-30"></i></span>  قیمت مصرف کننده :</span>
                    <h5 class="text-left pl-2"><del>{{$product->presentPrice()}}</del><span class="pr-2">تومان</span> </h5>
                    <h2 class="text-danger text-left pl-2">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></h2>
                    <a href="{{route('cart.add', ['id' => $product->id])}}" id="button-cart" class="btn bg-yellow border-dark btn-lg btn-block m-auto text-dark">افزودن به سبد خرید</a>
                </div>
            </div>
        </div>
        </div>
        <br>
        <div class="card">
        <div class="row card-body">
             <div class="col-md-12">
                 <ul class="nav d-flex nav-pills" id="pills-tab" role="tablist">
                                <li class="nav-item flex-grow-1 text-center">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-about"
                                       role="tab" aria-controls="pills-home" aria-selected="true">مشخصات فنی</a>
                                </li>
                                <li class="nav-item flex-grow-1 text-center">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                                       role="tab" aria-controls="pills-profile" aria-selected="false">توضیحات درباره کالا</a>
                                </li>
                                <li class="nav-item flex-grow-1 text-center">
                                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact"
                                       role="tab" aria-controls="pills-contact" aria-selected="false">نظر درباره کالا</a>
                                </li>

                            </ul>
                 <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-about" role="tabpanel"
                                     aria-labelledby="pills-home-tab">
                                    <hr>
                                    <h4 class="p-3 m-2 text-center">مشخصات کلی کالا</h4>
                                    <ul class="list-unstyled">
                                        @foreach($product->attributeValues as $attribute)
                                            <li class="bg-light text-muted p-2 m-2 rounded"><span class="pr-2 mr-2">{{$attribute->attributeGroup->title}} :</span> {{$attribute->title}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                                     aria-labelledby="pills-profile-tab">
                                    <hr>
                                    <h4 class="p-3 m-2 text-center">توضیحات کالا</h4>
                                    <div class="p-4 m-4 text-justify text-muted bg-light rounded">
                                        <div class="display-4 text-right"><i class="fa fa-quote-right text-muted"></i></div>
                                        {!! $product->description   !!}
                                        <div class="display-4 text-left"><i class="fa fa-quote-left text-muted"></i></div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-contact" role="tabpanel"
                                     aria-labelledby="pills-contact-tab">
                                    <hr>
                                    <h4 class="p-3 m-2 text-center">نظر کاربران درباره کالا</h4>
                                    <div class="row">
                                        <div class="col-12 col-md-5">

                                            <form action="{{route('post.store')}}" method="POST">
                                                @csrf
                                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                                <div class="form-group">
                                                    <label for="title">عنوان نظر</label>
                                                    <input type="text" name="title" id="title" class="form-control form-control-lg" placeholder="عنوان نظر خود را وارد کنید...">
                                                </div>
                                                <div class="form-group">
                                                    <label for="strong_point">نقاط قوت کالا</label>
                                                    <input type="text" name="strong_point" id="strong_point" class="form-control form-control-lg" placeholder="نقاط قوت محصول را وارد کنید...">
                                                </div>
                                                <div class="form-group">
                                                    <label for="weak_point">نقاط ضعف کالا</label>
                                                    <input type="text" name="weak_point" id="weak_point" class="form-control form-control-lg" placeholder="نقاط ضعف محصول را وارد کنید...">
                                                </div>
                                                <div class="form-group">
                                                    <label for="description">متن نظر <span class="text-danger">*</span></label>
                                                    <textarea  name="description" id="description" class="form-control form-control-lg" placeholder="متن نظر خود را وارد کنید..."></textarea>
                                                </div>
                                                <button class="btn bg-yellow btn-lg btn-block border-dark">ثبت نظر خود</button>
                                            </form>
                                        </div>
                                        <div class="col-12 col-md-7">
                                            <h5 class=" p-2 m-2">دیگران را با نوشتن نظرات خود، برای انتخاب این محصول راهنمایی کنید.</h5>
                                            <p class="text-justify text-muted p-2 m-2 txt-justify font-size-16">
                                                لطفا پیش از ارسال نظر، خلاصه قوانین زیر را مطالعه کنید:

                                                فارسی بنویسید و از کیبورد فارسی استفاده کنید. بهتر است از فضای خالی (Space) بیش‌از‌حدِ معمول، شکلک یا ایموجی استفاده نکنید و از کشیدن حروف یا کلمات با صفحه‌کلید بپرهیزید.

                                                نظرات خود را براساس تجربه و استفاده‌ی عملی و با دقت به نکات فنی ارسال کنید؛ بدون تعصب به محصول خاص، مزایا و معایب را بازگو کنید و بهتر است از ارسال نظرات چندکلمه‌‌ای خودداری کنید.

                                                بهتر است در نظرات خود از تمرکز روی عناصر متغیر مثل قیمت، پرهیز کنید.

                                                به کاربران و سایر اشخاص احترام بگذارید. پیام‌هایی که شامل محتوای توهین‌آمیز و کلمات نامناسب باشند، حذف می‌شوند.

                                                از ارسال لینک‌های سایت‌های دیگر و ارایه‌ی اطلاعات شخصی خودتان مثل شماره تماس، ایمیل و آی‌دی شبکه‌های اجتماعی پرهیز کنید.

                                                با توجه به ساختار بخش نظرات، از پرسیدن سوال یا درخواست راهنمایی در این بخش خودداری کرده و سوالات خود را در بخش «پرسش و پاسخ» مطرح کنید.

                                                هرگونه نقد و نظر در خصوص سایت آلا‌کالا، خدمات و درخواست کالا را با ایمیل info@alakala.com یا با شماره‌ی 44241700 - 044 در میان بگذارید و از نوشتن آن‌ها در بخش نظرات خودداری کنید.
                                            </p>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        @foreach($posts as $post)
                                        <div class="col-12 col-md-12 bg-light rounded p-4 my-4">
                                            <span class="mr-4 pr-5 font-size-22"><span>عنوان نظر :</span> @if(isset($post->title)) {{$post->title}} @else  <span class="text-muted">بدون عنوان</span>  @endif</span>
                                            <span class="mr-4 pr-5 font-size-22 float-left">
                                                <span >کاربر :</span>
                                                @foreach($post->users as $user)
                                                   <span class="text-muted">{{$user->name}}</span>
                                                @endforeach
                                            </span>
                                            <div class="display-4 text-right"><i class="fa fa-quote-right text-muted"></i></div>
                                            <p class="pr-5">{{$post->description}}</p>
                                            <div class="display-4 text-left"><i class="fa fa-quote-left text-muted"></i></div>
                                            <span class="p-2 float-left">{{\Hekmatinasser\Verta\Verta::instance($post->created_at)}}</span>
                                        </div>
                                            @endforeach
                                    </div>
                                </div>

                            </div>
             </div>
        </div>
        </div>

    </div>
    @include('layouts.section-product')
    @include('layouts.footer')

@endsection

@section('script')
    <script type="text/javascript" src="{{asset('/js/jquery-2.1.3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/smoothproducts.min.js')}}"></script>

    <script type="text/javascript">
        /* wait for images to load */
        $(window).load(function() {
            $('.sp-wrap').smoothproducts();
        });

    </script>

@endsection
