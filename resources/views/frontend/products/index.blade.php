@extends('layouts.maser')

@section('title')
 محصولات
@endsection

@section('script-top')
    <!-- src jquery js -->
    <script src="{{asset('backend/js/jquery.min.js')}}"></script>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>

    <script>
        $(document).ready(function()
        {


            if($('.bbb_viewed_slider').length)
            {
                var viewedSlider = $('.bbb_viewed_slider');

                viewedSlider.owlCarousel(
                    {
                        loop:false,
                        margin:30,
                        autoplay:false,
                        autoplayTimeout:6000,
                        nav:false,
                        dots:true,
                        responsive:
                            {
                                0:{items:1},
                                475:{items:2},
                                668:{items:3},
                                1200:{items:4},
                                1199:{items:6}
                            }
                    });

                if($('.bbb_viewed_prev').length)
                {
                    var prev = $('.bbb_viewed_prev');
                    prev.on('click', function()
                    {
                        viewedSlider.trigger('prev.owl.carousel');
                    });
                }

                if($('.bbb_viewed_next').length)
                {
                    var next = $('.bbb_viewed_next');
                    next.on('click', function()
                    {
                        viewedSlider.trigger('next.owl.carousel');
                    });
                }
            }

        });
    </script>
@endsection

@include('layouts.navigation')

@section('content')

    <br>
   @include('layouts.section-product')
    <br>
   @include('layouts.footer')

@endsection

@section('script')
    <script type="text/javascript" src="{{asset('/js/jquery-2.1.3.min.js')}}"></script>

@endsection
