
@extends('layouts.app')

@section('title')
   جستجوی محصول
@endsection

@include('layouts.navigation')

@section('content')


    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-5 col-md-3 my-2">
               <div class="card p-2 m-2">
                   <span class="text-right pr-2">فیلتر های اعمال شده</span>
                   <hr class="bg-light">
               </div>
                <div class="card p-2 m-2">
                    <span class="text-right pr-2">دسته بندی</span>
                    <hr class="bg-light">
                    <ul class="list-unstyled text-decoration-none">
                        @foreach($categories as $category)
                        <li><a class="text-dark" href="{{url('/categories/'.$category->id)}}">{{$category->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="card p-2 m-2">
                    <span class="text-right pr-2">برند</span>
                    <hr class="bg-light">
                    <ul class="list-unstyled text-decoration-none pr-0 mr-0">
                        @foreach($products as $key=>$product)
                        <li>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="brand-{!!$key!!}" id="brand-{!!$key!!}" class="custom-control-input ml-2" >
                                <label for="brand-{!!$key!!}" class="custom-control-label pr-3"></label>
                                <span class="mr-3 pr-3">{{$product->brand->title}}</span>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="card p-2 m-2">
                    <span class="text-right pr-2">فروشنده</span>
                    <hr class="bg-light">
                           <div class="custom-control custom-checkbox">
                               <input type="checkbox" name="vendor" id="vendor" class="custom-control-input ml-2" >
                               <label for="vendor" class="custom-control-label pr-3"></label>
                               <span class="mr-3 pr-3">فروشنده برگزیده</span>
                           </div>
                </div>
                <div class="card p-2 m-2">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1">
                        <label class="custom-control-label" for="customSwitch1"></label>
                        <span class="mr-3 pr-4">فقط کالاهای موجود</span>
                    </div>
                </div>
                <div class="card p-2 m-2">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch2">
                        <label class="custom-control-label" for="customSwitch2"></label>
                        <span class="mr-3 pr-4">فقط کالاهای آماده ارسال</span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-7 col-md-9 my-2">
                <h6 class="p-2 m-2">
                    <span class="rounded">لیست ها</span>
                </h6>
                <div class="container-fluid">
                    <div class="row">
                    <div class="col-md-12 col-12 shadow">
                        <div class="row">
                            <div class="col-md-12 col-12 pt-1 mt-1">
                                <a href="#" class="rounded bg-info text-white">جدیدترین ها</a>
                            </div>
                        </div>
                        <hr>
                        <div class="row m-2">
                            <div class="col-md-12 col-sm-12 col-12 ">

                                <div class="row">
                                    @foreach($products as $key_d=>$product)
                                        @if($key_d<3 )
                                            <div class="col-md-4 col-12">
                                                <div class="card card-pk">
                                                    <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{$product->title}}</h5>
                                                        @if($product->qty!=0)
                                                            <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                                            <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                                            <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                                        @elseif($product->qty==0)
                                                            <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                                <div class="row">
                                    @foreach($products as $key_d=>$product)
                                        @if($key_d>=3 && $key_d < 6 )
                                            <div class="col-md-4 col-12">
                                                <div class="card card-pk">
                                                    <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{$product->title}}</h5>
                                                        @if($product->qty!=0)
                                                            <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                                            <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                                            <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                                        @elseif($product->qty==0)
                                                            <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                                <div class="row">
                                    @foreach($products as $key_d=>$product)
                                        @if($key_d>=6 && $key_d < 9 )
                                            <div class="col-md-4 col-12">
                                                <div class="card card-pk">
                                                    <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{$product->title}}</h5>
                                                        @if($product->qty!=0)
                                                            <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                                            <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                                            <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                                        @elseif($product->qty==0)
                                                            <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                                <div class="row">
                                    @foreach($products as $key_d=>$product)
                                        @if($key_d>=9 && $key_d < 12 )
                                            <div class="col-md-4 col-12">
                                                <div class="card card-pk">
                                                    <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{$product->title}}</h5>
                                                        @if($product->qty!=0)
                                                            <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                                            <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                                            <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                                        @elseif($product->qty==0)
                                                            <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                                <div class="row">
                                    @foreach($products as $key_d=>$product)
                                        @if($key_d>=12 && $key_d < 15 )
                                            <div class="col-md-4 col-12">
                                                <div class="card card-pk">
                                                    <img class="card-img-top" src="{{$product->photos[0]->path}}"  alt="">
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{$product->title}}</h5>
                                                        @if($product->qty!=0)
                                                            <span class="text-left pl-2 h5"><del>{{$product->presentPrice()}}</del><span class="pr-2 font-size-14">تومان</span></span>
                                                            <span class="text-danger text-left pl-2 h3">{{$product->discountPresent()}} <span class="pr-2 font-size-14">تومان</span></span>
                                                            <a href="{{url('/products/'.$product->slug)}}" class="btn bg-yellow border-dark btn-block">جزئیات محصول</a>
                                                        @elseif($product->qty==0)
                                                            <h5 class="p-4 m-4 text-muted">موجود نیست</h5>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footer')
@endsection
