@extends('layouts.app')

@section('title')
    ارتباط با ما
@endsection

@include('layouts.navigation')

@section('content')

    <div class="container-fluid">
        <div class="row m-4">
            <div class="col-md-12 col-12 bg-light shadow">
                <div class="my-4">
                    <div class="py-2 my-2">
                        <span class="m-4 pt-4 pb-2 h4">تماس با آلاکالا</span>
                        <span class="float-left pl-2 ml-2"><img src="{{url('images/Support.png')}}" class="img-fluid" height="40%" width="40%" alt=""></span>
                    </div>

                    <p class="align-justify font-size-18 text-muted p-5">مرکز ارتباط با مشتریان آلاکالا در تمام ایام هفته و به صورت 24 ساعته، پاسخگوی سؤالات و تماسهای مشتریان است. آلاکالا در راستای تکریم مشتریان، امکان پاسخگویی به سؤالات و ارایه مشاوره را از طریق مرکز ارتباط با مشتریان به شماره های ذیل در تمام ایام هفته و به صورت شبانه‌روزی فراهم کرده‌است. کارشناسان آلاکالا در بخش‌‌های پیگیری سفارش، خدمات پس از فروش، حسابداری، امور مالی، پیشنهاد ، انتقاد، مشاوره و اطلاع‌رسانی آماده پاسخگویی به مشتریان هستند. گفتنی است آلاکالا به منظور پیشبرد اهداف مشتری‌مداری و در راستای یکپارچه‌سازی فرآیند ثبت و پیگیری سئوالات، نظرات، مغایرت‌ها و... اولویت پیگیری را به درخواست های ثبت شده بر روی سایت قرار داده است. از این‌رو مشتریان می‌توانند علاوه بر برقراری تماس تلفنی با آلاکالا، بهتر است جهت ثبت درخواست برای پیگیری یا سوال درباره سفارش و ارسال پیام بهتر است از فرم زیر استفاده کنید.

پیش از ارسال ایمیل یا تماس تلفنی با آلاکالا، بخش پرسش‏های متداول را ملاحظه فرمایید و در صورتی که پاسخ خود را نیافتید، با ما تماس بگیرید.</p>
                    <div class="row">
                        <div class="col-md-12">
                            @include('admin.partials.index-success')
                            @include('admin.partials.form-errors')
                        </div>
                    </div>
                   <div class="row m-4">
                       <div class="col-md-8 col-12 col-sm-12">

                           <h5 class="mb-4 pb-4">برای پیگیری یا سوال درباره سفارش و ارسال پیام بهتر است از فرم زیر استفاده کنید</h5>
                           <form action="{{route('contact.save')}}" method="POST">
                               @csrf
                               <label for="name" class="col-form-label">نام و نام خانوادگی <span class="text-danger">*</span></label>
                               <div class="form-group w-100">
                                   <input type="text" name="name" class="form-control form-control-lg" id="name" placeholder="نام و نام خانوادگی خود را وارد کنید">
                               </div>
                               <label for="email" class="col-form-label">ایمیل <span class="text-danger">*</span></label>
                               <div class="form-group w-100">
                                   <input type="email" name="email" class="form-control form-control-lg" id="email" placeholder="ایمیل خود را وارد کنید">
                               </div>
                               <label for="mobile" class="col-form-label">تلفن تماس <span class="text-danger">*</span></label>
                               <div class="form-group w-100">
                                   <input type="tel" name="mobile" class="form-control form-control-lg" id="mobile" placeholder="تلفن تماس خود را وارد کنید">
                               </div>
                               <label for="subject" class="col-form-label">موضوع <span class="text-danger">*</span></label>
                               <div class="form-group w-100">
                                   <select name="subject" class="form-control form-control-lg" id="subject">
                                       <option value="" selected>انتخاب کنید</option>
                                       <option value="پیشنهاد" >پیشنهاد</option>
                                       <option value="انتقاد و شکایت" >انتقاد و شکایت</option>
                                       <option value="پیگیری سفارش" >پیگیری سفارش</option>
                                       <option value="خدمات پس از فروش" >خدمات پس از فروش</option>
                                       <option value="سابداری و امور مالی" >حسابداری و امور مالی</option>
                                       <option value="همکاری در فروش" >همکاری در فروش</option>
                                       <option value="مدیریت" >مدیریت</option>
                                       <option value="سایر موضوعات" >سایر موضوعات</option>
                                   </select>
                               </div>
                               <label for="order" class="col-form-label">شناسه سفارش </label>
                               <div class="form-group w-100">
                                   <input type="text" name="order" class="form-control form-control-lg" id="order" placeholder="شناسه سفارش خود را وارد کنید">
                               </div>
                               <label for="description" class="col-form-label">متن پیام <span class="text-danger">*</span></label>
                               <div class="form-group w-100">
                                   <textarea name="description" class="form-control form-control-lg" id="description" placeholder="متن پیام خود را اینجا وارد کنید..."></textarea>
                               </div>
                               <button type="submit" class="btn bg-yellow border-dark btn-lg ">ثبت اطلاعات</button>
                           </form>
                           <hr>
                           <div class="alert alert-warning border-warning p-4">
                               <h6>روزهای که در سال جاری به دلیل تعطیلی ، آلاکالا هیچ گونه پاسخگویی، سرویس دهی و خدمات ندارد، به شرح زیر است:</h6>
                               <ul>
                                   <li>13 فروردین</li>
                                   <li>14 خرداد</li>
                                   <li>18 شهریور(تاسوعا)</li>
                                   <li>19 شهریور(عاشورا)</li>
                                   <li>27 مهر (اربعین)</li>
                                   <li>275 آبان (رحلت حضرت رسول اکرم(ص))</li>
                                   <li>22 بهمن</li>
                               </ul>
                           </div>
                           <div class="alert alert-success border-success p-3">
                               <span><h5> تلفن تماس و فکس: 44241700 - 044 ، 44241701 - 044<span class="h6 pr-2">(پاسخگویی 24 ساعته، 7 روز هفته)</span></h5></span>
                           </div>
                           <br>
                           <h3 class="my-3 py-2">دفتر مرکزی:</h3>
                           <h6>استان آذربایجان غربی - پیرانشهر - خیابان شهید بهشتی - نبش کوچه رزگران - مجتمع موبایل الماس - طبقه 2 - واحد 33</h6>
                           <h3 class="my-3 py-2">مرکز امور مشتریان:</h3>
                           <h6>لطفا کالا را برای بازگرداندن و ارسال آن به خدمات پس از فروش آلاکالا از طریق کد پستی 5781855746 ارسال نمایید</h6>
                       </div>
                   </div>
                    <div class="row m-4">
                      <div class="col-md-12 col-12">
                          <div class="alert alert-danger border-danger p-4 text-center d-block">
                              لطفا قبل از هر اقدامی به کارشناسان پشتیبانی خدمات پس از فروش تماس گرفته و از ارسال کالا بدون هماهنگی با خدمات پس از فروش آلاکالا جداً، خودداری کنید.
                          </div>
                          <span><h5 class="text-muted py-4">ایمیل سازمانی آلاکالا: info@alakala.com</h5></span>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footer')
@endsection
