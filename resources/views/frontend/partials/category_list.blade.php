@foreach($categories as $sub_category)
    <div class="col-md-6 col-lg-6 col-sm-6">
        <a href="{{url('/categories/'.$sub_category->id)}}"><h4 class="sub-menu-head"> {{$sub_category->name}}</h4></a>
        <ul class="sub-menu-lists">
          @if(count($sub_category->childrenRecursive) > 0)
             @foreach($sub_category->childrenRecursive as $category)
             <li>
                 <a href="{{url('/categories/'.$category->id)}}" class="text-justify">
                     {{$category->name}}
                 </a>
             </li>
             @endforeach
          @endif
       </ul>
    </div>


@endforeach

