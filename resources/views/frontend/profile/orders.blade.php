@extends('layouts.app')

@section('title')
    پروفایل
@endsection

@include('layouts.navigation')

@section('content')
    <div class="container">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <div>{{session('success')}}</div>
            </div>
        @endif

        <div class="row">
            <div id="column-right" class="col-sm-8 col-md-4 col-12">
                <div class="card m-4 bg-light">
                    <h3 class="subtitle p-4 m-auto">حساب کاربری</h3>
                    <div class="list-group">
                        <ul class="list-item">
                            <li><a href="{{url('/profile')}}">اطلاعات حساب</a></li>
                            <li><a href="{{url('/addresses')}}">لیست آدرس ها</a></li>
                            <li><a href="#">لیست علاقه مندی</a></li>
                            <li><a href="{{route('profile.orders')}}">تاریخچه سفارشات</a></li>
                            <li><a href="#">تراکنش ها</a></li>
                            <li><a href="#">خبرنامه</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--Middle Part Start-->
            <div id="content" class="col-sm-4 col-md-8 col-12">

                    <div class="table-responsive m-4">
                        <table class="table table-hover table-striped">
                            <thead class="thead-light">
                            <tr>
                                <th>شناسه</th>
                                <th>مقدار</th>
                                <th>وضعیت</th>
                                <th>تاریخ تراکنش</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td><a href="{{route('profile.orders.lists', ['id'=>$order->id])}}">{{$order->id}}</a></td>
                                    <td>{{$order->amount}}</td>
                                    @if($order->status == 0)
                                        <td><span class="badge badge-danger">پرداخت نشده</span></td>
                                    @else
                                        <td><span class="badge badge-success">پرداخت شده</span></td>
                                    @endif
                                    <td>{{\Hekmatinasser\Verta\Verta::instance($order->created_at)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

            </div>
            <!--Middle Part End -->
        </div>
    </div>

    @include('layouts.footer')
@endsection
