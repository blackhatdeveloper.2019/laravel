
@extends('layouts.app')

@section('title')
    ویرایش اطلاعات پروفایل
@endsection
@section('style')

    <link rel="stylesheet" href="{{asset('backend/css/dropzone.min.css')}}">

@endsection
@include('layouts.navigation')

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div id="column-right" class="col-sm-8 col-md-4 col-12">
                <div class="card m-4 bg-light">
                    <h3 class="subtitle p-4 m-auto">حساب کاربری</h3>
                    <div class="list-group">
                        <ul class="list-item">
                            <li><a href="{{url('/profile')}}">اطلاعات حساب</a></li>
                            <li><a href="{{url('/addresses')}}">لیست آدرس ها</a></li>
                            <li><a href="#">لیست علاقه مندی</a></li>
                            <li><a href="{{route('profile.orders')}}">تاریخچه سفارشات</a></li>
                            <li><a href="#">تراکنش ها</a></li>
                            <li><a href="#">خبرنامه</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--Middle Part Start-->
            <div id="content" class="col-sm-4 col-md-8 col-12">
                <div class="m-4">
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <div>{{session('success')}}</div>
                            <button type="button" class="close" aria-label="close" data-dismiss="alert">&times</button>
                        </div>
                    @endif
                    @include('admin.partials.form-errors')
                    <div class="card  bg-light">
                        <div class="table table-responsive">
                            <form action="/profile-update/{{Auth::user()->id}}" method="POST" >
                                @csrf
                                {{method_field('PUT')}}
                            <table class="table table-striped table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <th>عنوان</th>
                                    <th>مشخصات</th>
                                </tr>
                                </thead>
                                <tr>
                                    <td><label for="name">نام و نام خانوادگی <span class="text-danger">*</span></label></td>
                                    <td><input type="text" class="form-control form-control-lg" name="name" id="name" value="{{$user->name}}" placeholder="نام و نام خانوادگی خود را وارد کنید"></td>
                                </tr>
                                <tr>
                                    <td><label for="mobile">شماره موبایل <span class="text-danger">*</span></label></td>
                                    <td><input type="tel" class="form-control form-control-lg" name="mobile" id="mobile" value="{{$user->mobile}}" placeholder="شماره موبایل خود را وارد کنید"></td>
                                </tr>
                                <tr>
                                    <td><label for="password">کلمه عبور <span class="text-danger">*</span></label></td>
                                    <td><input type="password" class="form-control form-control-lg" name="password" id="password"  placeholder=" کلمه عبور جدید را وارد کنید"></td>
                                </tr>
                                <tr>
                                    <td>تصویر پروفایل</td>
                                    <td>
                                        @if( !empty($user->photo_id))
                                            <img src="{{$photo->path}}" class="img-fluid rounded-circle"  width="150" height="150" alt="">
                                        @else
                                            <img src="{{url('/images/profile_icon.png')}}" class="img-fluid rounded-circle"  width="150" height="150" alt="">
                                        @endif

                                            <div class="form-group custom-control-inline">
                                                <input type="hidden" name="photo_id" id="photo_id" value="{{$user->photo_id}}">
                                                <div id="photo" class="dropzone">
                                                </div>
                                            </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>تاریخ ایجاد</td>
                                    <td>{{\Hekmatinasser\Verta\Verta::instance($user->created_at)}}</td>
                                </tr>
                                <tr>
                                    <td>عملیات</td>
                                    <td><button type="submit" class="btn bg-info btn-lg text-white border-dark">بروزرسانی</button></td>
                                </tr>
                            </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--Middle Part End -->
        </div>
    </div>
    @include('layouts.footer')
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('backend/js/dropzone.min.js')}}"></script>

    <script>
        Dropzone.autoDiscover=false;
        var drop = new Dropzone('#photo', {
            addRemoveLinks: true,
            maxFiles: 1,
            url: "{{ route('photos.upload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                document.getElementById('photo_id').value = response.photo_id
            }
        });

    </script>
@endsection
