@extends('layouts.app')

@section('title')
    پروفایل
@endsection

@include('layouts.navigation')

@section('content')
    <div class="container">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <div>{{session('success')}}</div>
            </div>
        @endif

        <div class="row">
            <div id="column-right" class="col-sm-8 col-md-4 col-12">
                <div class="card m-4 bg-light">
                    <h3 class="subtitle p-4 m-auto">حساب کاربری</h3>
                    <div class="list-group">
                        <ul class="list-item">
                            <li><a href="{{url('/profile')}}">اطلاعات حساب</a></li>
                            <li><a href="{{url('/addresses')}}">لیست آدرس ها</a></li>
                            <li><a href="#">لیست علاقه مندی</a></li>
                            <li><a href="{{route('profile.orders')}}">تاریخچه سفارشات</a></li>
                            <li><a href="#">تراکنش ها</a></li>
                            <li><a href="#">خبرنامه</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--Middle Part Start-->
            <div id="content" class="col-sm-4 col-md-8 col-12">

                <div class="table-responsive m-4">
                    <table class="table table-hover table-striped">
                        <thead class="thead-light">
                        <tr>
                            <th>تصویر محصول</th>
                            <th>نام محصول</th>
                            <th>کد محصول</th>
                            <th>تعداد</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($order->products as $product)
                            <tr>
                                <td><img width="125" height="100" src="{{$product->photos[0]->path}}"> </td>
                                <td>{{$product->title}}</td>
                                <td>{{$product->sku}}</td>
                                <td>{{$product->pivot->qty}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="customer-data">
                    <p class="mt-4">
                        <strongt>نام خریدار: </strongt> {{$order->user->name}}
                    </p>
                    <p>

                    </p>
                    <p>
                        <strongt>شماره موبایل خریدار: </strongt> {{$order->user->mobile}}
                    </p>
                </div>
            </div>
            <!--Middle Part End -->
        </div>
    </div>
@endsection
