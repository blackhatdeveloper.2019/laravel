@extends('layouts.app')

@section('title')
    پروفایل
@endsection

@include('layouts.navigation')

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div id="column-right" class="col-sm-8 col-md-4 col-12">
                <div class="card m-4 bg-light">
                    <h3 class="subtitle p-4 m-auto">حساب کاربری</h3>
                    <div class="list-group">
                        <ul class="list-item">
                            <li><a href="{{url('/profile')}}">اطلاعات حساب</a></li>
                            <li><a href="{{url('/addresses')}}">لیست آدرس ها</a></li>
                            <li><a href="#">لیست علاقه مندی</a></li>
                            <li><a href="{{route('profile.orders')}}">تاریخچه سفارشات</a></li>
                            <li><a href="#">تراکنش ها</a></li>
                            <li><a href="#">خبرنامه</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--Middle Part Start-->
            <div id="content" class="col-sm-4 col-md-8 col-12">
                <div class="m-4">
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <div>{{session('success')}}</div>
                            <button type="button" class="close" aria-label="close" data-dismiss="alert">&times</button>
                        </div>
                    @endif

                    <div class="card  bg-light">
                        <div class="table table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <th>عنوان</th>
                                    <th>مشخصات</th>
                                </tr>
                                </thead>
                                <tr>
                                    <td>نام و نام خانوادگی</td>
                                    <td>{{$user->name}}</td>
                                </tr>
                                <tr>
                                    <td>شماره موبایل</td>
                                    <td>{{$user->mobile}}</td>
                                </tr>
                                <tr>
                                    <td>تصویر پروفایل</td>
                                    <td>
                                        @if( isset($user->photo_id))
                                            <img src="{{$photo->path}}" class="img-fluid rounded-circle"  width="150" height="150" alt="">
                                            @else
                                            <img src="{{url('/images/profile_icon.png')}}" width="100" height="100" alt="">
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>تاریخ ایجاد</td>
                                    <td>{{\Hekmatinasser\Verta\Verta::instance($user->created_at)}}</td>
                                </tr>
                                <tr>
                                    <td>عملیات</td>
                                    <td><a href="{{route('profile.edit',$user->id)}}" class="btn bg-yellow border-dark">ویرایش</a></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--Middle Part End -->
        </div>
    </div>
    @include('layouts.footer')
@endsection
