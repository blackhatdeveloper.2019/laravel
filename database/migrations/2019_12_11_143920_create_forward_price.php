<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForwardPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forward_price', function (Blueprint $table) {
            $table->unsignedInteger('forward_id');
            $table->foreign('forward_id')->references('id')->on('forwards');

            $table->unsignedInteger('price_id');
            $table->foreign('price_id')->references('id')->on('prices');
            $table->increments('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forward_price');
    }
}
