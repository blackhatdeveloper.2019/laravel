<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmsPanelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_panel', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url_client');
            $table->string('user_name');
            $table->string('user_password');
            $table->string('panel_number');
            $table->string('pattern_code');
            $table->tinyInteger('status')->default(0);
            $table->integer('code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_panel');
    }
}
